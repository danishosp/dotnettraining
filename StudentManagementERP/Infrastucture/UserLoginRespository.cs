﻿using System.Data;
using System.Data.SqlClient;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Core;
using Core.Common;
using Core.UsersModels;
using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Infrastucture
{
    public class UserLoginRespository : IUserLoginRespository
    {
        private readonly IConfiguration _configuration;
        private static string _connectionString = string.Empty;
        private static string _secretKey = string.Empty;

        public UserLoginRespository(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = _configuration["ConnectionStrings:StudentManagementDB"] ?? string.Empty;
            _secretKey = _configuration["JwtSecretKey"] ?? string.Empty;
        }

        public static IDbConnection Connection
        {
            get
            {
                return new SqlConnection(_connectionString);
            }
        }


        public async Task<ResponseBase> ResetPassword(DtoResetPassword dtoResetPassword)
        {
            ResponseBase responseBase = new();
            using IDbConnection db = Connection;
            var response = await db.QueryMultipleAsync("[dbo].[uspResetPassword]", new
            {
                dtoResetPassword.Email,
            },commandType: CommandType.StoredProcedure);

            var res = response.Read<ResponseBase>().First();
            var userDetails = response.Read<DtoUserLoginDetails>().First();

            if(userDetails != null)
            {
                bool isVerified = EncryptAndDecrypt.VerifyPasswordHash(dtoResetPassword.Password!, userDetails.PasswordHash!, userDetails.PasswordSalt!);
                if(isVerified)
                {
                    EncryptAndDecrypt.CreatePasswordHash(dtoResetPassword.NewPassword!, out byte[] passwordHash, out byte[] passwordSalt);
                    var respone = await db.QueryFirstOrDefaultAsync<ResponseBase>("[dbo].[uspUserTempPasswordUpdate]", new
                    {
                        dtoResetPassword.Email,
                        passwordHash,
                        passwordSalt

                    }, commandType: CommandType.StoredProcedure);
                   
                    if(! respone.Status)
                    {
                        return respone;
                    }
                    responseBase.Status = true;
                    responseBase.Message = "Password Change Successfully";
                }
                else
                {
                    responseBase.Status = false;
                    responseBase.Message = "Your Default Password is incorrect";
                }
            }
            else
            {
                return res;
            }
           
            return responseBase;
        }

        public async Task<Response<string>> UserLogin(DtoUserLogin dtoUserLogin)
        {
            Response<string> objectResponse = new();
            using IDbConnection db = Connection;

            var response = await db.QueryMultipleAsync("[dbo].[uspUsersLogin]", new
            {
                dtoUserLogin.Email,
            }, commandType: CommandType.StoredProcedure);

            var res = response.Read< Response<string>> ().First();
            var userdetails = response.Read<DtoUserLoginDetails>().First();

            if (userdetails.UserId != null)
            {
                bool isVerified = EncryptAndDecrypt.VerifyPasswordHash(dtoUserLogin.Password!, userdetails.PasswordHash!, userdetails.PasswordSalt!);

                if (isVerified)
                {
                    var result = await GenerateToken(userdetails);
                    objectResponse.Data = result.Data;
                    objectResponse.Status = true;
                    objectResponse.Message = "User Login Successfully";
                }
                else
                {
                    objectResponse.Status = false;
                    objectResponse.Message = "Wrong Credentials";
                }
                
            }

            else
            {
                return res;
            }

            return objectResponse;
        }

        public async Task<Response<string>> GenerateToken(DtoUserLoginDetails dtoUserLoginDetails)
        {
            return await Task.Run(() =>
            {
                Response<string> tokenResponse = new();
                var tokenHandler = new JwtSecurityTokenHandler();

                var key = Encoding.ASCII.GetBytes(_secretKey);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                    new Claim(ClaimTypes.Name, dtoUserLoginDetails.UserId ?? string.Empty),
                    new Claim(ClaimTypes.Role,"User"),
                    new Claim(ClaimTypes.Email, dtoUserLoginDetails.Email ?? string.Empty)
                    }),
                    Expires = DateTime.UtcNow.AddDays(1),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };

                var token = tokenHandler.CreateToken(tokenDescriptor);
                tokenResponse.Data = tokenHandler.WriteToken(token);

                return tokenResponse;
            });

        }
    }
}


