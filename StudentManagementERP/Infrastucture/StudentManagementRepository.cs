﻿using System.Data;
using System.Data.SqlClient;
using System.Text;
using Core;
using Core.Common;
using Core.UsersModels;
using Dapper;
using Microsoft.Extensions.Configuration;


namespace Infrastucture
{
    public class StudentManagementRepository : IStudentManagementRepository
    {
        private readonly IConfiguration _configuration;
        private static string _connectionString = string.Empty;
        private static string _randomChar = string.Empty;

        public StudentManagementRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = _configuration["ConnectionStrings:StudentManagementDB"] ?? string.Empty;
            _randomChar = _configuration["GetRandomChar"] ?? string.Empty;
        }

        public static IDbConnection Connection
        {
            get
            {
                return new SqlConnection(_connectionString);
            }
        }

        

        private static string GetRandomPassword(int length)
        {
            StringBuilder stringBuilder = new();
            Random rnd = new();

            for (int i = 0; i < length; i++)
            {
                int index = rnd.Next(_randomChar.Length);
                stringBuilder.Append(_randomChar[index]);
            }

            return stringBuilder.ToString();
        }

        public async Task<ResponseBase> InsertStudentUser(DtoUserInsert dtoUserInsert)
        {
            string password = GetRandomPassword(8);

            EncryptAndDecrypt.CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);

            using IDbConnection db = Connection;
            var respone = await db.QueryFirstOrDefaultAsync<ResponseBase>("[dbo].[uspUsersInsert]", new
            {  
                dtoUserInsert.FirstName,
                dtoUserInsert.LastName,
                dtoUserInsert.Address,
                dtoUserInsert.MobileNo,
                dtoUserInsert.Email,
                passwordHash, 
                passwordSalt

            }, commandType: CommandType.StoredProcedure);

            if (respone.Status)
            {
               string message = $"Hello {dtoUserInsert.FirstName} {dtoUserInsert.LastName} \n Your Temporary password is : '{password}'";
               await EmailService.EmailSender(dtoUserInsert.Email, message);
            }

            return respone;
        }

        public async Task<ResponseBase> UpdateStudentUser(DtoUserUpdate dtoUserUpdate)
        {
            using IDbConnection db = Connection;
            return await db.QueryFirstOrDefaultAsync<ResponseBase>("[dbo].[uspUsersUpdate]", dtoUserUpdate, commandType: CommandType.StoredProcedure);
        }

        public async Task<ResponseBase> DeleteStudentUser(DtoUserDelete dtoUserDelete)
        {
            using IDbConnection db = Connection;
            var respone = await db.QueryFirstOrDefaultAsync<ResponseBase>("[dbo].[uspUsersDelete]", dtoUserDelete, commandType: CommandType.StoredProcedure);
            return respone;
        }

        public async Task<Response<UserDetails>> GetStudentUser(DtoUserGetById dtoUserGetById)
        {
            Response<UserDetails> response;
            using IDbConnection db = Connection;
            var result = await db.QueryMultipleAsync("[dbo].[uspUserGetById]", dtoUserGetById, commandType: CommandType.StoredProcedure);
            response = result.Read<Response<UserDetails>>().FirstOrDefault()!;
            response.Data = result.Read<UserDetails>().FirstOrDefault();
            return response;
        }

        public async Task<ResponseList<UserDetails>> ListStudentUsers(DtoUsersListAll dtoUsersListAll)
        {
            ResponseList<UserDetails> response;
            using IDbConnection db = Connection;
            var result = await db.QueryMultipleAsync("[dbo].[uspUsersAllList]", dtoUsersListAll, commandType: CommandType.StoredProcedure);

            response = result.Read<ResponseList<UserDetails>>().FirstOrDefault()!;
            response.Data = result.Read<UserDetails>().ToList();
            response.TotalRecords = response.Data.Count;

            return response;
        }

        public async Task<ResponseBase> ChangeStudentUserStatus(DtoUserActivedAndDeactivated dtoUserActivedAndDeactivated)
        {
            using IDbConnection Db = Connection;
            return await Db.QueryFirstOrDefaultAsync<ResponseBase>("[dbo].[uspUsersStatusChanges]", dtoUserActivedAndDeactivated, commandType: CommandType.StoredProcedure);
            
        }

    }

}
