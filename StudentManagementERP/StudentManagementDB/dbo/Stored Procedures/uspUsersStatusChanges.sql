﻿CREATE PROC uspUsersStatusChanges
(
	@UserId	   VARCHAR(50), --CHANGE DATATYPE TO UNIQUEIDENTIFIER
	@IsActive  BIT,
	@UpdatedBy VARCHAR(50) --CHANGE DATATYPE TO UNIQUEIDENTIFIER

)
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY

 

    IF @UserId IS NULL
    BEGIN
        SELECT 0 [Status], 'User Id is required' [Message]
        RETURN
    END    

 

    IF NOT EXISTS (SELECT 1 FROM [dbo].[tblUsers] WHERE [UserId] = @UserId AND IsDelete = 0)
    BEGIN
        SELECT 0 [Status], 'User not found' [Message]
        RETURN
    END
	
	IF EXISTS (SELECT 1 FROM [dbo].[tblUsers] WHERE [UserId] = @UserId AND IsDelete = 0)
    BEGIN
   		UPDATE [dbo].[tblUsers]
   		SET IsActive = @IsActive,
			UpdatedOn = GETUTCDATE(),
			UpdatedBy = @UpdatedBy

   		WHERE [UserId] = @UserId
	END
   
    SELECT 1 [Status], CONCAT('User', SPACE(1), 'successfully', SPACE(1),(CASE WHEN @IsActive = 1 THEN 'activated.' ELSE 'deactivated.' END))  [Message]

    END TRY
    BEGIN CATCH
        DECLARE @Msg NVARCHAR(500) = ERROR_MESSAGE();
        DECLARE @ErrorSeverity INT = Error_severity();       
        DECLARE @ErrorState INT = Error_state();    
        RAISERROR(@Msg, @ErrorSeverity, @ErrorState);

        SELECT 0 [Status], @Msg [Message]
    END CATCH

END