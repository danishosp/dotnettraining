﻿CREATE PROC uspUsersUpdate
(
	@UserId		VARCHAR(50), 
	@FirstName	VARCHAR(50), 
	@LastName	VARCHAR(50),
	@MobileNo	VARCHAR(50), 
	@Address	VARCHAR(MAX), 
	@UpdatedBy	VARCHAR(50)
)
AS
BEGIN
	SET NOCOUNT ON;

	SET @FirstName = TRIM(@FirstName);
	SET @LastName  = TRIM(@LastName);
	SET @MobileNo  = TRIM(@MobileNo);
	SET @Address   = TRIM(@Address);

	IF EXISTS( SELECT 1 FROM tblUsers WITH (NOLOCK) WHERE UserId = @UserId AND IsDelete = 0)
		BEGIN
			UPDATE tblUsers
			SET FirstName = @FirstName,
				LastName  = @LastName,
				MobileNo  = @MobileNo,
				[Address] = @Address,
				UpdatedOn = GETUTCDATE(),
				UpdatedBy = @UpdatedBy

			WHERE UserId  = @UserId

			SELECT 1 [Status], 'Data is been successfully updated' [Message]
        END

    ELSE 
    	BEGIN
        	SELECT 0 [Status], 'UserId does not exists please check and try again.' [Message]
        END
END