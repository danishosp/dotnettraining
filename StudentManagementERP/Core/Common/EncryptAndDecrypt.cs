﻿using System.Text;

namespace Core.Common
{
    public class EncryptAndDecrypt
    {
        public static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            var mySalt = BCrypt.Net.BCrypt.GenerateSalt();
            passwordSalt = Encoding.ASCII.GetBytes(mySalt);
            passwordHash = Encoding.ASCII.GetBytes(BCrypt.Net.BCrypt.HashPassword(password, mySalt));
        }

        public static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            try
            {
                string storedSaltStr = Encoding.ASCII.GetString(storedSalt);
                var newPassword = BCrypt.Net.BCrypt.HashPassword(password, storedSaltStr);

                string oldPassword = Encoding.Default.GetString(storedHash);
                return newPassword == oldPassword;
            }
            catch
            {
                throw;
            }
        }

    }
}
