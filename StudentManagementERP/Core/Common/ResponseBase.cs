﻿namespace Core.Common
{
    public class ResponseBase
    {
        public bool Status { get; set; }
        public string? Message { get; set; }

    }

    public class Response<T> : ResponseBase where T : class
    {
        public T? Data { get; set; } 
    }

    public class ResponseList<T> : ResponseBase where T : class
    {
        public List<T>? Data { get; set;}
        public long TotalRecords { get; set; }
    }
    
}
