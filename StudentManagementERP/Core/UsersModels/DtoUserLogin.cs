﻿using System.ComponentModel.DataAnnotations;

namespace Core.UsersModels
{
    public class DtoResetPassword
    {
        [Required, DataType(DataType.EmailAddress)]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Not an valid Email Address.")]
        public string? Email { get; set; }
        public string? Password { get; set; }

        [Required, DataType(DataType.Password)]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$",
         ErrorMessage = "Password must be 8 Characters long and must contain at least one upper case character, one lowercase character, one number and one special character.")]
        public string? NewPassword { get; set; }

        [Required, Compare("NewPassword", ErrorMessage = "Both Password and Confirm Password Must be Same")]
        public string? ConfirmPassword { get; set; }
    }

    public class DtoUserLogin
    {
        [Required, DataType(DataType.EmailAddress)]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Not an valid Email Address.")]
        public string? Email { get; set; }

        [Required, DataType(DataType.Password)]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$",
         ErrorMessage = "Password must be 8 Characters long and must contain at least one upper case character, one lowercase character, one number and one special character.")]
        public string? Password { get; set; }
    }

    public class DtoUserLoginDetails
    {
        public string? UserId { get; set; }
        public string? Email { get; set; }
        public byte[]? PasswordHash { get; set; }
        public byte[]? PasswordSalt { get; set; }
    }

}
