﻿
using Core.Common;
using Core.UsersModels;

namespace Core
{
    public interface IUserLoginRespository
    {
        Task<Response<string>> UserLogin(DtoUserLogin dtoUserLogin);
        Task<Response<string>> GenerateToken(DtoUserLoginDetails dtoUserLoginDetails);
        Task<ResponseBase> ResetPassword(DtoResetPassword dtoResetPassword);
    }
}
