﻿using Core.Common;
using Core.UsersModels;

namespace Core
{
    public interface IStudentManagementRepository
    {
        Task<ResponseBase> InsertStudentUser(DtoUserInsert dtoUsersInsert);
        Task<ResponseBase> UpdateStudentUser(DtoUserUpdate dtoUsersUpdate);
        Task<ResponseBase> DeleteStudentUser(DtoUserDelete dtoUserDelete);
        Task<Response<UserDetails>> GetStudentUser(DtoUserGetById dtoUserGetById);
        Task<ResponseList<UserDetails>> ListStudentUsers(DtoUsersListAll dtoUserListAll);
        Task<ResponseBase> ChangeStudentUserStatus(DtoUserActivedAndDeactivated dtoUserActivedAndDeactivated);
        
    }
}
