﻿using Core;
using Core.UsersModels;
using Microsoft.AspNetCore.Mvc;
using StudentManagementERP.CommonException;

namespace StudentManagementERP.Controllers
{
    [Route("api/users"), ServiceFilter(typeof(ModelValidatorAttribute))]

    public class StudentManagementController : ControllerBase
    {
        private readonly IStudentManagementRepository _studentManagementRepository;
        

        public StudentManagementController(IStudentManagementRepository studentManagementRepository)
        {
            _studentManagementRepository = studentManagementRepository;
        }

        [HttpPost]
        public async Task<IActionResult> InsertStudentUser([FromBody]DtoUserInsert dtoUserInsert)
        {
            try
            {
                var response = await _studentManagementRepository.InsertStudentUser(dtoUserInsert);
                return Ok(response);
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        [HttpPut]
        public async Task<IActionResult> UpdateStudentUser([FromBody] DtoUserUpdate dtoUserUpdate)
        {
            try
            {
                var response = await _studentManagementRepository.UpdateStudentUser(dtoUserUpdate);
                return Ok(response);
            }
            catch (Exception)
            {

                throw;
            }
              
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteStudentUser([FromBody] DtoUserDelete dtoUserDelete)
        {
            try
            {
                var response = await _studentManagementRepository.DeleteStudentUser(dtoUserDelete);
                return Ok(response);
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        [HttpGet, Route("{userId}")]
        public async Task<IActionResult> GetStudentUser(string userId)
        {
            try
            {   
                var response = await _studentManagementRepository.GetStudentUser(new DtoUserGetById { UserId = userId });
                return Ok(response);
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        [HttpGet]
        public async Task<IActionResult> ListStudentUser(DtoUsersListAll dtoUsersListAll)
        {
            try
            {
                var response = await _studentManagementRepository.ListStudentUsers(dtoUsersListAll);
                return Ok(response);
            }
            catch (Exception)
            {

                throw;
            }
            
            
        }

        [HttpPatch]
        public async Task<IActionResult> ChangeStudentUserStatus([FromBody]DtoUserActivedAndDeactivated dtoUserActivedAndDeactivated)
        {
            try
            {
                var response = await _studentManagementRepository.ChangeStudentUserStatus(dtoUserActivedAndDeactivated);
                return Ok(response);
            }
            catch (Exception)
            {

                throw;
            }
             
        }

    }
}
