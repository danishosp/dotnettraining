﻿using Core.UsersModels;
using Microsoft.AspNetCore.Mvc;
using StudentManagementERP.CommonException;
using Core;

namespace StudentManagementERP.Controllers
{
    [Route("api/[controller]"), ServiceFilter(typeof(ModelValidatorAttribute))]

    public class AuthController : ControllerBase
    {
        private readonly IUserLoginRespository _userLoginRespository;
        public AuthController(IUserLoginRespository userLoginRespository)
        {
            _userLoginRespository = userLoginRespository;
        }

        [HttpPost, Route("Login")]
        public async Task<IActionResult> UserLogin([FromBody] DtoUserLogin dtoUserLogin)
        {
            try
            {
                var response = await _userLoginRespository.UserLogin(dtoUserLogin);
                return Ok(response);
                
            }
            catch (Exception)
            {
                throw;
            }

        }

        [HttpPost ,Route("ResetPassword")]
        public async Task<IActionResult> ResetPassword([FromBody] DtoResetPassword dtoResetPassword)
        {
            try
            {
                var response = await _userLoginRespository.ResetPassword(dtoResetPassword);
                return Ok(response);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
