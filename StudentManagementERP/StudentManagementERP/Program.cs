using AuthLibrary;
using Core;
using CrudOperations;
using Infrastucture;
using StudentManagementERP.CommonException;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddScoped<ModelValidatorAttribute>();
builder.Services.AddScoped<IStudentManagementRepository, StudentManagementRepository>();
builder.Services.AddScoped<IUserLoginRespository, UserLoginRespository>();
//builder.Services.AddScoped<ICrudOperationService, CrudOperationDataAccess>();
//builder.Services.AddScoped<IAuthLib, DataAccess>();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
