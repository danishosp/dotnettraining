﻿using Core.Interface;
using Core.Models;
using Microsoft.AspNetCore.Mvc;

namespace RCM_CRUD_Operation.Controllers
{
    [Route("api/UserModuleMapping")]
    //njdsguiaguisiuddhsahjao
    public class UserModuleMappingController : ControllerBase
    {
        private readonly IUserModuleMappingRepository _userModuleMapping;

        public UserModuleMappingController(IUserModuleMappingRepository userModuleMapping)
        {
            _userModuleMapping = userModuleMapping;
        }
        
        [HttpPost]
        public async Task<ActionResult> InsertData([FromBody] DtoUserModuleMapping dtoUserModuleMapping)
        {
            await _userModuleMapping.UserModuleMapping(dtoUserModuleMapping);
            return Ok("Data Entered Successfully");
        }

    }
}
