﻿

namespace Core.Models
{
    public class DtoUserModuleMapping
    {
        public Guid UserId { get; set; }
        public List<PermissionTypeList>? ListPermission { get; set; }

        public class PermissionTypeList
        {
            public string? ModuleId { get; set; }
            public bool IsEdit { get; set; }
            public bool IsDelete { get; set; }
            public bool IsView { get; set; }
            public bool IsAdd { get; set; }
        }

    }
    

}
