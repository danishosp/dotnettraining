﻿using Core.Models;

namespace Core.Interface
{
    public interface IUserModuleMappingRepository
    {
        Task UserModuleMapping(DtoUserModuleMapping dtoUserModuleMapping);
    }
}
