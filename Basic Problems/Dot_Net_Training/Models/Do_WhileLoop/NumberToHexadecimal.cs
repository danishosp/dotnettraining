﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.Do_WhileLoop
{
    public class NumberToHexadecimal
    {
        [Required]
        public int Number { get; set; }
    }
}
