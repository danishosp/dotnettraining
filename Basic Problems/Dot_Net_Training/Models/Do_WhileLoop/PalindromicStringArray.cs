﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.Do_WhileLoop
{
    public class PalindromicStringArray
    {
        [Required]
        public string[]? Words { get; set; }
    }
}
