﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.Do_WhileLoop
{
    public class IncreasingDecreasingString
    {
        [Required]
        public string? InputString { get; set; }
    }
}
