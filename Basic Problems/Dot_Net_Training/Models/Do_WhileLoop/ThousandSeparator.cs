﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.Do_WhileLoop
{
    public class ThousandSeparator
    {
        [Required]
        public int Number { get; set; }
    }
}
