﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.ArithmeticOperator
{
    public class SalaryCalculation
    {
        [Required]
        public string? Name { get; set; }
        [Required]
        public float Salary { get; set; }
    }
}
