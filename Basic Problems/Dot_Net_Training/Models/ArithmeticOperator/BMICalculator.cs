﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.ArithmeticOperator
{
    public class BMICalculator
    {
        [Required]
        public double Weight { get; set; }

        [Required]
        public double Height { get; set; }
    }
}
