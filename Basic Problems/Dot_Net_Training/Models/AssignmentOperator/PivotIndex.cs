﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.AssignmentOperator
{
    public class PivotIndex
    {
        [Required ]

        public int[]? Number { get; set; }
    }
}
