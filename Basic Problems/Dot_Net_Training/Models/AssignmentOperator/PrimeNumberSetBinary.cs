﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.AssignmentOperator
{
    public class PrimeNumberSetBinary
    {
        [Required]
        public int LeftNumber { get; set; }

        [Required]
        public int RightNumber { get; set; }
    }
}
