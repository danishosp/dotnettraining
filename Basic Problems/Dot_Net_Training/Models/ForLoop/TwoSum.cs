﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.ForLoop
{
    public class TwoSum
    {
        [Required]
        public int[]? InputArray { get; set; }

        [Required]
        public int Target { get; set; }
    }
}
