﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.ForLoop
{
    public class OperationsOnValue
    {
        [Required]
        public string[]? Operations { get; set; }
    }
}
