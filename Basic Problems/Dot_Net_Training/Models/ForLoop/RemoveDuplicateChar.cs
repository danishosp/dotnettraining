﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.ForLoop
{
    public class RemoveDuplicateChar
    {
        [Required]
        public string? Char { get; set; }
    }
}
