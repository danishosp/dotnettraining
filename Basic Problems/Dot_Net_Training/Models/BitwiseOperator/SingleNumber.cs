﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.BitwiseOperator
{
    public class SingleNumber
    {
        [Required]

        public int[]? Value { get; set;}
    }
}
