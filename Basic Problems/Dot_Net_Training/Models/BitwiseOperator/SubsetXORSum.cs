﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.BitwiseOperator
{
    public class SubsetXORSum
    {
        [Required]
        public int[]? Number { get; set; }
    }
}
