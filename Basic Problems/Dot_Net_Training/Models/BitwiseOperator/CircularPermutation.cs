﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.BitwiseOperator
{
    public class CircularPermutation
    {
        [Required]
        public int Number { get; set; }

        [Required]
        public int Start { get; set; }
    }
}
