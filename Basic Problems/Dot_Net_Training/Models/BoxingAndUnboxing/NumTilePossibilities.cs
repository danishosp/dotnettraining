﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.BoxingAndUnboxing
{
    public class NumTilePossibilities
    {
        [Required]
        public string? Title { get; set; }
    }
}
