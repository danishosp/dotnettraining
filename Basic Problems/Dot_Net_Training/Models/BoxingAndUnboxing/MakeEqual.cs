﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.BoxingAndUnboxing
{
    public class MakeEqual
    {
        [Required]
        public string? InputValue { get; set; }
    }
}
