﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.BoxingAndUnboxing
{
    public class Boxing
    {
        [Required]
        public int FirstNumber { get; set; }

        [Required]
        public int SecondNumber { get; set; }

    }
}
