﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.BoxingAndUnboxing
{
    public class AppendCharacters
    {
        [Required]
        public string? Input1 { get; set; }

        [Required]
        public string? Input2 { get; set; }

        [Required]
        public char Char { get; set; }

    }
}
