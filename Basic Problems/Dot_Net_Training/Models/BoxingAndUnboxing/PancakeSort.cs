﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.BoxingAndUnboxing
{
    public class PancakeSort
    {
        [Required]
        public int[]? InputArray { get; set; }
    }
}
