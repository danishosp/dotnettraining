﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.TernaryOperator
{
    public class BinaryWatch
    {
        [Required]
        public int Number { get; set; }
    }
}
