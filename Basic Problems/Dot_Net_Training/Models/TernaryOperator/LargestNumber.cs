﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.TernaryOperator
{
    public class LargestNumber
    {
        [Required]
        public int Digit1 { get; set; }

        [Required]
        public int Digit2 { get; set;}

        [Required]
        public int Digit3 { get; set;}
    }
}
