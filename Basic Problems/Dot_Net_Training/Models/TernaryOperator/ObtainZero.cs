﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.TernaryOperator
{
    public class ObtainZero
    {
        [Required]
        public int Digit1 { get; set; }

        [Required]
        public int Digit2 { get; set;}
    }
}
