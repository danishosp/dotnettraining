﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.TernaryOperator
{
    public class BalancedStringSplit
    {
        [Required]
        public string Value { get; set; }
    }
}
