﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.TernaryOperator
{
    public class MedianOfSortedArray
    {
        [Required]
        public int[]? FirstInput { get; set; }

        [Required]
        public int[]? SecondInput { get;}
    }
}
