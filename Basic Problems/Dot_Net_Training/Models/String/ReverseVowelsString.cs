﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.String
{
    public class ReverseVowelsString
    {
        [Required]
        public string? Value { get; set; }
    }
}
