﻿namespace Dot_Net_Training.Models.String
{
    public class DestinationCity
    {
        public IList<IList<string>> ? Paths { get; set; }
    }
}
