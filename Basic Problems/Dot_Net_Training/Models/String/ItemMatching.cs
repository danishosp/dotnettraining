﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.String
{
    public class ItemMatching
    {
        [Required]
        public IList<IList<string>>? Items { get; set; }

        [Required]
        public string? RuleKey { get; set; }

        [Required]
        public string? RuleValue { get; set; }
    }
}
