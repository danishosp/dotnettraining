﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.If_Else_IfLadder
{
    public class BodyIndexCalculator
    {
        [Required]
        public double Weight { get; set; }

        [Required]
        public double Height { get; set; }
    }
}
