﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.If_Else_IfLadder
{
    public class CtcCalculation
    {
        [Required]
        public string? Name { get; set; }
        [Required]
        public float Salary { get; set; }
    }
}

