﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.If_Else_IfLadder
{
    public class Calculators
    {
        [Required]
        public double? Gold { get; set; }
        [Required]
        public double? Silver { get; set; }
        [Required]
        public double? Savings { get; set; }
    }
}
