﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.If_Else_IfLadder
{
    public class LoginForm
    {
        [Required, RegularExpression(@"\A(?i)(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?-i)[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", ErrorMessage = "Please Enter valid Email ")]
        public string? Email { get; set; }
        [Required, RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d.*)(?=.*\W.*)[a-zA-Z0-9\S]{8,}$")]
        public string? Password { get; set; }
    }
}
