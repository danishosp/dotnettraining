﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.SwitchStatement
{
    public class PartitionPivot
    {
        [Required]
        public int[]? InputArray { get; set; }

        [Required]
        public int Pivot { get; set; }
    }
}
