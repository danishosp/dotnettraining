﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.SwitchStatement
{
    public class StringCalculator
    {
        [Required]
        public string ? InputString { get; set; }
    }
}
