﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.SwitchStatement
{
    public class BaseballGame
    {
        [Required]
        public string[]? CallPoints { get; set; }
    }
}
