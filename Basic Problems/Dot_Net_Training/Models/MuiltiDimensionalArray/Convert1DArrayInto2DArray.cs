﻿namespace Dot_Net_Training.Models.MuiltiDimensionalArray
{
    public class Convert1DArrayInto2DArray
    {
        public int[]? Original { get; set; }
        public int Row { get; set; }
        public int Column { get; set; }
    }
}
