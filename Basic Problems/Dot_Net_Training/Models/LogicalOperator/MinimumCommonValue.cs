﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.LogicalOperator
{
    public class MinimumCommonValue
    {
        [Required]
        public int[]? Input1 { get; set; }

        [Required]
        public int[]? Input2 { get; set;}
    }

}
