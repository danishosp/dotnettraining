﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.LogicalOperator
{
    public class DistributeMoney
    {
        [Required]
        public int Money { get; set; }
        [Required]
        public int Children { get; set; }
    }
}
