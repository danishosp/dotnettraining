﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.LogicalOperator
{
    public class ValueAfterOperations
    {
        [Required]
        public string[]? Operations { get; set; }
    }
}
