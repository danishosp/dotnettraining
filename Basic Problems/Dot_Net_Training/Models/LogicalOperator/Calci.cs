﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.LogicalOperator
{
    public class Calci
    {
        [Required]
        public double? Gold { get; set; }
        [Required]
        public double? Silver { get; set; }
        [Required]
        public double? Savings { get; set; }
    }
}
