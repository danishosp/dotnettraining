﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.ComparisonOperator
{
    public class ProjectionArea
    {
        [Required]
        public int[][]? InputValue { get; set; }
    }
}
