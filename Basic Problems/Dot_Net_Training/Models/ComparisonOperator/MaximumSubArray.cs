﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.ComparisonOperator
{
    public class MaximumSubArray
    {
        [Required]
        public int[]? InputArray { get; set; }
    }
}
