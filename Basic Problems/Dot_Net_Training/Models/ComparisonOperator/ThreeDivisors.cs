﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.ComparisonOperator
{
    public class ThreeDivisors
    {
        [Required]
        public int Value { get; set; }
    }
}
