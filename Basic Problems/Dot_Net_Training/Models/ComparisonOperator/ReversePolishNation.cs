﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.ComparisonOperator
{
    public class ReversePolishNation
    {
        [Required]
        public string[]? InputTokens { get; set; }
    }
}
