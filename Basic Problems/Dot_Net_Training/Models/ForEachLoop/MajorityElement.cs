﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.ForEachLoop
{
    public class MajorityElement
    {
        [Required]
        public int[]? Number { get; set; }
    }
}
