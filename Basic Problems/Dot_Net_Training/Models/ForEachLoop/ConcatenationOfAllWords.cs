﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.ForEachLoop
{
    public class ConcatenationOfAllWords
    {
        [Required]
        public string? InputValue { get; set; }

        [Required]
        public string[]? Words { get; set; }
    }
}
