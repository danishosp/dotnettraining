﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.ForEachLoop
{
    public class PhoneNumberDigit
    {
        [Required]
        public string? StringInput { get; set; }
    }
}
