﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.ForEachLoop
{
    public class ReverseString
    {
        [Required]
        public string? Name { get; set; }
    }
}
