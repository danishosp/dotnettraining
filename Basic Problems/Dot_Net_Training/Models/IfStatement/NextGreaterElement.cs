﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.IfStatement
{
    public class NextGreaterElement
    {
        [Required]
        public int[]? Number1 { get; set; }

        [Required]
        public int[]? Number2  { get; set; }
    }
}
