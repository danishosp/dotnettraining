﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.IfStatement
{
    public class PokerHand
    {
        [Required]
        public int[]? Ranks { get; set; }

        [Required]

        public char[]? Suits { get; set; }
    }
}
