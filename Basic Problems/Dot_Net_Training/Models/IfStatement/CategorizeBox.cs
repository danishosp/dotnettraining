﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.IfStatement
{
    public class CategorizeBox
    {
        [Required]
        public int Length { get; set; }

        [Required]
        public int Width { get; set; }

        [Required]
        public int Height { get; set; }

        [Required]
        public int Mass { get; set; }
    }
}
