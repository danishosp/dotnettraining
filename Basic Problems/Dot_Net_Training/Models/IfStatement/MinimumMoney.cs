﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.IfStatement
{
    public class MinimumMoney
    {
        [Required]
        public int Money { get; set; }
        [Required]
        public int Children { get; set; }
    }
}
