﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.OneDimensionalArray
{
    public class InsertionSort
    {
        [Required]
        public int[]? InputArray { get; set; }
    }
}
