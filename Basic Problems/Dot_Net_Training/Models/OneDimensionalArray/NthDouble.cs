﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.OneDimensionalArray
{
    public class NthDouble
    {
        [Required]
        public int[]? ArrayInput { get; set; }
    }
}
