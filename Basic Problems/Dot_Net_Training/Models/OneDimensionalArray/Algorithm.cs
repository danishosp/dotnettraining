﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.OneDimensionalArray
{
    public class Algorithm
    {
        [Required]
        public int[]? Number { get; set; }

        [Required]
        public int KthIndex { get; set; }
    }
}
