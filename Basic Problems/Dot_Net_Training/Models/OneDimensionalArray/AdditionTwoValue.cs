﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.OneDimensionalArray
{
    public class AdditionTwoValue
    {
        [Required]
        public int[]? InputArray { get; set; }

        [Required]
        public int Target { get; set; }
    }
}

