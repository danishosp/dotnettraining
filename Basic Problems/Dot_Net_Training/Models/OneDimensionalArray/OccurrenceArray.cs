﻿namespace Dot_Net_Training.Models.OneDimensionalArray
{
    public class OccurrenceArray
    {
        public int[] ArrayInput { get; set; }
        public int Target { get; set; }
    }
}
