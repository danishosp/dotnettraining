﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.WhileLoop
{
    public class SortEvenAndOdd
    {
        [Required]
        public int[]? Number { get; set; }
    }
}
