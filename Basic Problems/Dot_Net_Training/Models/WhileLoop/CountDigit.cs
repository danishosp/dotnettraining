﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.WhileLoop
{
    public class CountDigit
    {
        [Required]
        public int Number { get; set; }
    }
}
