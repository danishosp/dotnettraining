﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.WhileLoop
{
    public class InsertPosition
    {
        [Required ]
        public int[]? Number { get; set; }
        [Required]
        public int Target { get; set; }
    }
}
