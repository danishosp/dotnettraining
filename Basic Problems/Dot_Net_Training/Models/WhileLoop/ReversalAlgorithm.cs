﻿using System.ComponentModel.DataAnnotations;

namespace Dot_Net_Training.Models.WhileLoop
{
    public class ReversalAlgorithm
    {
        [Required]
        public int[]? Number { get; set; }

        [Required]
        public int KthIndex { get; set; }
    }
}
