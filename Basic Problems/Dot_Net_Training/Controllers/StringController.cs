﻿using System.Text;
using Dot_Net_Training.Models.String;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Dot_Net_Training.Controllers
{
    [Route("api/string")]
    public class StringController : ControllerBase
    {
        [HttpPost, Route("itemMatching")]
        public IActionResult CountMatches([FromBody] ItemMatching itemMatching)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                IList<IList<string>>? items = itemMatching.Items;
                string? ruleKey = itemMatching.RuleKey;
                string? ruleValue = itemMatching.RuleValue;

                int result = 0;
                int indexer = 0;
                //Create the indexer - already initialized as 0, no need to add it implicity, just change it if necessary
                if (!ruleKey!.Equals("type"))
                {
                    if (ruleKey.Equals("color")) indexer = 1;
                    else indexer = 2;
                }

                foreach (var item in items!)
                {
                    if (item[indexer] == ruleValue) result++;
                }

                return Ok(result);



            }
            catch (Exception)
            {

                throw;
            }
        }




        [HttpPost, Route("replaceDigitWithChar")]
        public IActionResult ReplaceDigit(ReplaceDigitWithChar replaceDigitWithChar)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                string? value = replaceDigitWithChar.Value;
                StringBuilder value2 = new();
                for (int i = 0; i < value?.Length; i++)
                {
                    if (value[i] >= '0' && value[i] <= '9')
                        value2.Append(Convert.ToChar(value[i - 1] + Convert.ToInt32(value2[i] - '0')));
                    else
                        value2.Append(value[i]);
                }
                return Ok(value2);
            }
            catch (Exception)
            {

                throw;
            }
        }




        [HttpPost, Route("destinationCity")]
        public IActionResult Destcity([FromBody] DestinationCity destinationCity)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                IList<IList<string>>? paths = destinationCity.Paths;

                HashSet<String> noOutgoing = new HashSet<String>();
                for (int i = 0; i < paths![0].Count; i++)
                {
                    for (int j = 0; j < paths.Count; j++)
                    {
                        noOutgoing.Add(paths[j][i]);
                    }
                }
                return Ok(noOutgoing.ElementAt(noOutgoing.Count - 1));
            }
            catch (Exception)
            {

                throw;
            }
        }




        [HttpPost, Route("reverseWordStringIII")]
        public IActionResult ReverseWord(ReverseWordStringIII reverseWordStringIII)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                string? inputString = reverseWordStringIII.Value;

                var arr = inputString!.ToArray();
                var l = 0;

                for (var i = 1; i < inputString?.Length; i++)
                {
                    if (arr[i] == ' ')
                    {
                        ReverseString(arr, l, i - 1);
                        l = i + 1;
                    }
                }
                ReverseString(arr, l, arr.Length - 1);

                return Ok(new string(arr));
            }
            catch (Exception)
            {

                throw;
            }

            static void ReverseString(char[] stringInput, int left, int right)
            {
                while (left < right)
                {
                    (stringInput[right], stringInput[left]) = (stringInput[left], stringInput[right]);
                    left++;
                    right--;
                }
            }
        }





        [HttpPost, Route("ReverseVowelsString")]
        public IActionResult ReverseVowels(ReverseVowelsString reverseVowelsString)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                var value = reverseVowelsString.Value;
                var vowels = "aeiouAEIOU";
                var extract = value!.Where(c => vowels.Contains(c)).Reverse().ToArray();
                var result = value!.ToCharArray();

                int v = 0;

                for (int i = 0; i < value.Length; i++)
                {
                    if (vowels.Contains(value[i]))
                    {
                        result[i] = extract[v++];
                    }
                }

                return Ok(result);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }

}
