﻿using System.Diagnostics;
using Dot_Net_Training.Models.ArithmeticOperator;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static System.Net.Mime.MediaTypeNames;
using static System.Runtime.CompilerServices.RuntimeHelpers;

namespace Dot_Net_Training.Controllers
{
    [Route ("api/arithmeticOperator")]
    public class ArithmeticOperatorController : ControllerBase
    {
        [HttpPost, Route("splitString")]
        public IActionResult SplitString(SplitString splitString)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                string value = splitString.Value;
                var list = new List<int>();
                int n = value.Length;
                for (int i = 0; i < n; i++)
                {
                    if (value[i] == '1')
                        list.Add(i);
                }
                long result = 0;
                long mode = 1_000_000_007;
                if (list.Count % 3 != 0) return Ok(0);
                if (list.Count == 0)
                {
                    long x = n - 1;
                    long y = n - 2;
                    result = x * y / 2 % mode;
                }
                else
                {
                    long x = list[list.Count / 3] - list[list.Count / 3 - 1];
                    long y = list[list.Count / 3 * 2] - list[list.Count / 3 * 2 - 1];
                    result = x * y % mode;
                }
                return Ok((int)result);
            }
            catch (Exception)
            {

                throw;
            }
        }



        [HttpPost, Route("BMICalculator")]
        public IActionResult BmiCalculator(BMICalculator bMICalculator)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                double weight = bMICalculator.Weight;
                double height = bMICalculator.Height;

                double meterHeight = height / 100;
                double bmi = weight / (meterHeight * meterHeight);

                if (bmi < 18.5)
                {
                    return Ok("UnderWeight: " + bmi);
                }
                else if (bmi <= 24.9)
                {
                    return Ok("Normal: " + bmi);
                }
                else if (bmi <= 29.9)
                {
                    return Ok("OverWeight: " + bmi);
                }
                else if (bmi <= 34.9)
                {
                    return Ok("Obese/High: " + bmi);
                }
                else if (bmi <= 39.9)
                {
                    return Ok("Severely Obese/Very High: " + bmi);
                }
                else if (bmi > 40)
                {
                    return Ok("Morbidly Obese/Extremely High: " + bmi);
                }

                return Ok(bmi);
            }
            catch (Exception)
            {

                throw;
            }

        }



        [HttpPost, Route("charitycalculation")]
        public ActionResult CharityCalculation(CharityCalculation charityCalculation)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                double? inputGold = charityCalculation.Gold ?? 0.0;
                double? inputSilver = charityCalculation.Silver ?? 0.0;
                double? inputSavings = charityCalculation.Savings;

                if (inputGold >= 7.5 && inputSilver >= 52.5 && inputSavings >= 27000)
                {
                    return Ok("Gold: " + inputGold * 50000 * 2.5 / 100 + "\n" + "Silver: " + inputSilver * 5000 * 2.5 / 100 + "\n" + "Savings: " + inputSavings * 2.5 / 100);
                }

                else if (inputGold >= 7.5 && inputSilver >= 52.5 || inputSavings < 27000)
                {
                    return Ok("Gold: " + inputGold * 50000 * 2.5 / 100 + "\n" + "Silver: " + inputSilver * 5000 * 2.5 / 100 + "\n" + "savings: Not Eligible");
                }

                else if (inputGold >= 7.5 && inputSavings >= 27000 || inputSilver < 52.5)
                {
                    return Ok("Gold: " + inputGold * 50000 * 2.5 / 100 + "\n" + "Silver: Not Eligible" + "\n" + "Savings: " + inputSavings * 2.5 / 100);
                }

                else if (inputSilver >= 52.5 && inputSavings >= 27000)
                {
                    return Ok("Gold: Not Eligible" + "\n" + "Silver: " + inputSilver * 5000 * 2.5 / 100 + "\n" + "Savings: " + inputSavings * 2.5 / 100);
                }

                else if (inputGold >= 7.5)
                {
                    return Ok("Gold: " + (inputGold * 50000 * 2.5 / 100));
                }

                else if (inputSilver >= 52.5)
                {
                    return Ok("Silver: " + inputSilver * 5000 * 2.5 / 100);
                }

                else if (inputSavings >= 27000)
                {
                    return Ok("Savings: " + inputSavings * 2.5 / 100);
                }

                else
                {
                    return Ok("You are not eligible");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }



        [HttpPost, Route("salaryCalculation")]
        public ActionResult Salary(SalaryCalculation salaryCalculation)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                string? name = salaryCalculation.Name;
                float salary = salaryCalculation.Salary;
                float tax = 0;
                float netSalary = 0;

                if (salary > 50000)
                {
                    tax = salary * 10 / 100;
                    netSalary = salary - tax;
                }
                else if (salary > 30000)
                {
                    tax = salaryCalculation.Salary * 5 / 100;
                    netSalary = salary - tax;
                }
                else
                {
                    netSalary = (float)salary;
                }
                return Ok("Account holder name " + name + "\n" + "Your Gross Salary" + salary + "\n" + "Your tax deduction " + tax + "\n" + "Your net salary " + netSalary);
            }
            catch (Exception)
            {
                throw;
            }
        }



        [HttpPost, Route("clumsyFactorial")]
        public IActionResult Clumsy([FromBody] ClumsyFactorial clumsyFactorial)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                int number = clumsyFactorial.Number;
                int ans = number;
                int j = number - 1;
                bool t = true;

                Stack<int> stk = new();
                for (int i = 1; i < number; i++)
                {
                    if (i % 4 == 0 || i % 4 == 3)
                    {
                        stk.Push(ans);

                        if (t)
                        {
                            ans = j--;
                        }

                        else
                        {
                            ans = (-1 * j--);
                        }

                        t = !t;

                    }
                    else if (i % 4 == 1)
                    {
                        ans *= j--;
                    }

                    else
                    {
                        ans /= j--;
                    }

                }
                int sum = 0;
                while (stk.Count > 0)
                {
                    sum += stk.Pop();

                }
                return Ok(sum + ans);
            }

            catch (Exception)
            {
                throw;
            }
        }

    }
}
