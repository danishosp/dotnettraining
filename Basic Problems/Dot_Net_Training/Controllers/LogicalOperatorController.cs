﻿using Dot_Net_Training.Models.LogicalOperator;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Dot_Net_Training.Controllers
{
    [Route("api/logicalOperator")]
    public class LogicalOperatorController : ControllerBase
    {
        [HttpPost, Route ("distributeMoney")]
        public IActionResult GetMoney(DistributeMoney distributeMoney)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                int money = distributeMoney.Money;
                int children = distributeMoney.Children;
                money -= children;
                if (money < 0)
                    return Ok(-1);
                if (money / 7 == children && money % 7 == 0)
                    return Ok(children);
                if (money / 7 == children - 1 && money % 7 == 3)
                    return Ok(children - 2);
                return Ok(Math.Min(children - 1, money / 7));

                
            }
            catch (Exception)
            {

                throw;
            }
        }



        [HttpPost, Route("checkVowels")]
        public ActionResult Vowels(CheckVowels checkvowels)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                string? name = checkvowels.Name;
                bool result = Isvowel(name);

                return Ok(result ? "contain Vowel's" : "Does not contain vowel's");

            }
            catch (Exception)
            {

                throw;
            }

        }
        static bool Isvowel(string name)
        {
            name = name.ToLower();

            for (int i = 0; i < name.Length; i++)
            {
                if (name[i] == 'a' || name[i] == 'e' || name[i] == 'i' || name[i] == 'o' || name[i] == 'u')
                    return true;
            }
            return false;
        }



        [HttpPost, Route("calci")]
        public ActionResult Calci(Calci calci)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                double? inputGold = calci.Gold ?? 0.0;
                double? inputSilver = calci.Silver ?? 0.0;
                double? inputSavings = calci.Savings;

                if (inputGold >= 7.5 && inputSilver >= 52.5 && inputSavings >= 27000)
                {
                    return Ok("Gold: " + inputGold * 50000 * 2.5 / 100 + "\n" + "Silver: " + inputSilver * 5000 * 2.5 / 100 + "\n" + "Savings: " + inputSavings * 2.5 / 100);
                }

                else if (inputGold >= 7.5 && inputSilver >= 52.5 || inputSavings < 27000)
                {
                    return Ok("Gold: " + inputGold * 50000 * 2.5 / 100 + "\n" + "Silver: " + inputSilver * 5000 * 2.5 / 100 + "\n" + "savings: Not Eligible");
                }

                else if (inputGold >= 7.5 && inputSavings >= 27000 || inputSilver < 52.5)
                {
                    return Ok("Gold: " + inputGold * 50000 * 2.5 / 100 + "\n" + "Silver: Not Eligible" + "\n" + "Savings: " + inputSavings * 2.5 / 100);
                }

                else if (inputSilver >= 52.5 && inputSavings >= 27000)
                {
                    return Ok("Gold: Not Eligible" + "\n" + "Silver: " + inputSilver * 5000 * 2.5 / 100 + "\n" + "Savings: " + inputSavings * 2.5 / 100);
                }

                else if (inputGold >= 7.5)
                {
                    return Ok("Gold: " + (inputGold * 50000 * 2.5 / 100));
                }

                else if (inputSilver >= 52.5)
                {
                    return Ok("Silver: " + inputSilver * 5000 * 2.5 / 100);
                }

                else if (inputSavings >= 27000)
                {
                    return Ok("Savings: " + inputSavings * 2.5 / 100);
                }

                else
                {
                    return Ok("You are not eligible");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }



        [HttpPost, Route("valueAfterOperations")]
        public IActionResult ValueAfterOperations(ValueAfterOperations valueAfterOperations)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                string[]? operations = valueAfterOperations.Operations;
                var x = 0;
                for (var i = 0; i < operations?.Length; i++)
                {
                    if (operations[i].Equals("++X") || operations[i].Equals("X++"))
                        x++;
                    if (operations[i].Equals("--X") || operations[i].Equals("X--"))
                        x--;
                }
                return Ok(x);
            }
            catch (Exception)
            {

                throw;
            }

        }



        [HttpPost, Route ("minimumCommonValue")]
        public IActionResult FindCommonValue([FromBody]MinimumCommonValue minimumCommonValue)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                if (minimumCommonValue.Input1 == null || minimumCommonValue.Input2 == null)
                {
                    return Ok("Invalid Input");
                }

                int[] input1 = minimumCommonValue.Input1!;
                int[] input2 = minimumCommonValue.Input2!;

               

                if (input1[0] > input2[^1] || input1?[^1] < input2[0])
                {
                    return Ok(-1);
                }
                    
                int i = 0;


                int Length = input1!.Length > input2.Length ? input2.Length : input1.Length;


                while (i < Length)
                {
                    if (i >= 1)
                    {
                        if (input1[i - 1] != input1[i] && input2!.Contains(input1[i]))
                        {
                            return Ok(input1[i]);
                        }
                            
                        else if (input2?[i - 1] != input2?[i] && input1.Contains(input2![i]))
                        {
                            return Ok(input2[i]);
                        }
                            
                    }
                    else if (i == 0)
                    {
                        if (input2!.Contains(input1[i]))
                        {
                            return Ok(input1[i]);
                        }
                            
                        else if (input1.Contains(input2![i]))
                        {
                            return Ok(input2[i]);
                        }
                            
                    }
                    i++;
                }
                return Ok(-1);
            }
            
            catch (Exception)
            {

                throw;
            }
        }
    }
}
