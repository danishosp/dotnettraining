﻿using Dot_Net_Training.Models.Do_WhileLoop;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System;
using System.Text;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Dot_Net_Training.Controllers
{
    [Route("api/Do_WhileLoop")]

    public class Do_WhileLoopController : ControllerBase
    {
        [HttpPost, Route ("largeGroupPositions")]
        public IActionResult GetLargeGroupPositions(LargestGroupPosition largestGroupPosition)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                string stringValue = string.Empty;
                List<IList<int>> returnList = new List<IList<int>>(); 
                int idx = 0; 
                do 
                { 
                    int cur = idx; 
                    while (idx < stringValue.Length && (stringValue[idx] == stringValue[cur]))
                    { 
                        idx++;
                    } 
                    if (((idx - 1) - cur) >= 2) 
                    { 
                        returnList.Add(new List<int> { cur, idx - 1 });
                    } 
                } 
                while (idx < stringValue.Length); 
                return Ok(returnList);
            }
            catch (Exception)
            {

                throw;
            }
        }



        [HttpPost, Route ("increasingDecreasingString")]
        public IActionResult SortString(IncreasingDecreasingString increasingDecreasingString)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                string? inputString = increasingDecreasingString.InputString;
                char[] arr = inputString!.ToCharArray(); 
                Array.Sort(arr); 
                StringBuilder result = new();
                
                int i = 0; 
                int a = 1; 
                char c = '0'; 
                bool picked = false;
                do
                {
                    if (i < 0 || i >= arr.Length)
                    {
                        if (picked == true)
                        {
                            picked = false;
                        }
                        else
                        {
                            break;

                        }
                        a = -a;
                        i += a;
                        c = '0';

                    }
                    if (arr[i] != c && arr[i] != '0')
                    {
                        result.Append(arr[i]);
                        c = arr[i];
                        arr[i] = '0';
                        picked = true;
                    }
                    i += a;
                }
                while (true);
                
                return Ok(result.ToString());
                
            
            }
            catch (Exception)
            {

                throw;
            }
        }



        [HttpPost, Route ("thousandSeparator")]
        public IActionResult ThousandSeparator(ThousandSeparator thousandSeparator)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                int number = thousandSeparator.Number;

                if (number == 0)
                {
                    return Ok("0");
                }
                string result = "";
                int remainder;
                string num;
                do
                {
                    remainder = number % 1000;
                    number /= 1000;
                    num = remainder.ToString();



                    if (number > 0)
                    {
                        while (num.Length < 3) { num = "0" + num; }
                        result = "." + num + result;
                    }
                    else
                    {
                        result = num + result;
                    }
                } while (number > 0);
                return Ok(result);

            }
            catch (Exception)
            {

                throw;
            }
        }



        [HttpPost, Route ("palindromicStringInArray")]
        public IActionResult FirstPalindromic(PalindromicStringArray palindromicStringArray)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                string[]? words = palindromicStringArray.Words;
                int n = words!.Length;
                int i = 0;
                do
                {
                    bool flag = true;
                    Stack<char> temp = new();
                    int each = words[i].Length;
                    int k = 0;
                    for (int j = 0; j < each; j++)
                        temp.Push(words[i][j]);



                    while (temp.Count > 0)
                    {
                        if (temp.Pop() != words[i][k++])
                        { flag = false; break; }
                    }
                    if (flag)
                        return Ok(words[i]);
                    i++;
                } while (i < n);
                return Ok("");

            }
            catch (Exception)
            {

                throw;
            }
        }



        [HttpPost, Route("numberToHexadecimal")]
        public IActionResult ToHexa(NumberToHexadecimal numberToHexadecimal)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                int number = numberToHexadecimal.Number;
                if (number == 0)
                {
                    return Ok("0");
                }



                var letters = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
                var remainder = (uint)number;
                var digits = new List<char>();



                do
                {
                    var digit = remainder % 16;
                    remainder /= 16;
                    digits.Add(letters[digit]);
                } while (remainder > 0);



                digits.Reverse();
                return Ok(new string(digits.ToArray()));
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
