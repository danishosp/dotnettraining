﻿using Dot_Net_Training.Models.If_Else_IfLadder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Dot_Net_Training.Controllers
{
    [Route ("api/if_Else_If_Ladder")]
    public class If_Else_IfLadderController : ControllerBase
    {
        

        [HttpPost, Route("loginForm")]
        public IActionResult Login(LoginForm loginForm)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                string? email = loginForm.Email;
                string result1 = "";
                string result2 = "";
                string result3 = "";

                if (email == "dan123@gmail.com")
                {
                    string? password = loginForm.Password;
                    if (password == "dan123")
                    {
                        result1 = "Login Successfully";
                    }
                    else
                    {
                        result2 = "Invalid Email or Password..";
                    }
                }
                else
                {
                    result3 = "Invalid Email or Password..";
                }
                return Ok(result1 + " " + result2 + " " + result3);
            }
            catch (Exception)
            {

                throw;
            }
        }



        [HttpPost, Route("ctcCalculation")]
        public ActionResult CtcCalculation(CtcCalculation ctcCalculation)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                string? name = ctcCalculation.Name;
                float salary = ctcCalculation.Salary;
                float tax = 0;
                float netSalary = 0;

                if (salary > 50000)
                {
                    tax = salary * 10 / 100;
                    netSalary = salary - tax;
                }
                else if (salary > 30000)
                {
                    tax = ctcCalculation.Salary * 5 / 100;
                    netSalary = salary - tax;
                }
                else
                {
                    netSalary = (float)salary;
                }
                return Ok("Account holder name " + name + "\n" + "Your Gross Salary" + salary + "\n" + "Your tax deduction " + tax + "\n" + "Your net salary " + netSalary);
            }
            catch (Exception)
            {
                throw;
            }
        }
        


        [HttpPost, Route("calculator")]
        public ActionResult Calculator(Calculators Calculator)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                double? inputGold = Calculator.Gold ?? 0.0;
                double? inputSilver = Calculator.Silver ?? 0.0;
                double? inputSavings = Calculator.Savings;

                if (inputGold >= 7.5 && inputSilver >= 52.5 && inputSavings >= 27000)
                {
                    return Ok("Gold: " + inputGold * 50000 * 2.5 / 100 + "\n" + "Silver: " + inputSilver * 5000 * 2.5 / 100 + "\n" + "Savings: " + inputSavings * 2.5 / 100);
                }

                else if (inputGold >= 7.5 && inputSilver >= 52.5 || inputSavings < 27000)
                {
                    return Ok("Gold: " + inputGold * 50000 * 2.5 / 100 + "\n" + "Silver: " + inputSilver * 5000 * 2.5 / 100 + "\n" + "savings: Not Eligible");
                }

                else if (inputGold >= 7.5 && inputSavings >= 27000 || inputSilver < 52.5)
                {
                    return Ok("Gold: " + inputGold * 50000 * 2.5 / 100 + "\n" + "Silver: Not Eligible" + "\n" + "Savings: " + inputSavings * 2.5 / 100);
                }

                else if (inputSilver >= 52.5 && inputSavings >= 27000)
                {
                    return Ok("Gold: Not Eligible" + "\n" + "Silver: " + inputSilver * 5000 * 2.5 / 100 + "\n" + "Savings: " + inputSavings * 2.5 / 100);
                }

                else if (inputGold >= 7.5)
                {
                    return Ok("Gold: " + (inputGold * 50000 * 2.5 / 100));
                }

                else if (inputSilver >= 52.5)
                {
                    return Ok("Silver: " + inputSilver * 5000 * 2.5 / 100);
                }

                else if (inputSavings >= 27000)
                {
                    return Ok("Savings: " + inputSavings * 2.5 / 100);
                }

                else
                {
                    return Ok("You are not eligible");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }



        [HttpPost, Route("bodyIndexCalculator")]
        public IActionResult BmiCalculator(BodyIndexCalculator bodyIndexCalculator)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                double weight = bodyIndexCalculator.Weight;
                double height = bodyIndexCalculator.Height;

                double meterHeight = height / 100;
                double bmi = weight / (meterHeight * meterHeight);

                if (bmi < 18.5)
                {
                    return Ok("UnderWeight: " + bmi);
                }
                else if (bmi <= 24.9)
                {
                    return Ok("Normal: " + bmi);
                }
                else if (bmi <= 29.9)
                {
                    return Ok("OverWeight: " + bmi);
                }
                else if (bmi <= 34.9)
                {
                    return Ok("Obese/High: " + bmi);
                }
                else if (bmi <= 39.9)
                {
                    return Ok("Severely Obese/Very High: " + bmi);
                }
                else if (bmi > 40)
                {
                    return Ok("Morbidly Obese/Extremely High: " + bmi);
                }

                return Ok(bmi);
            }
            catch (Exception)
            {

                throw;
            }

        }
        


        [HttpPost , Route ("strongPasswordChecker")]
        public IActionResult Checkpassword(StrongPasswordChecker strongPasswordChecker)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                string? password = strongPasswordChecker.Value;

                string special = "!@#$%^&*()-+";
                bool[] validation = new bool[4];

                if (password?.Length < 8) return Ok(false);
                for (int i = 0; i < password!.Length - 1; i++)
                {
                    if (password[i] == password[i + 1])  // the password is not strong if there are at least 2 of same letters in adjacent positions
                        return Ok(false);
                    CheckChar(password[i], special, validation);
                }
                CheckChar(password[^1], special, validation); //Check the last character
                return Ok(validation.All(x => x)); // return true if all the items in the array are true


            }
            catch (Exception)
            {

                throw;
            }
           
        }

        static void CheckChar(char ch, string special, bool[] validation)
        {
            if (Char.IsLower(ch)) validation[0] = true; //Lowercase letter
            else if (Char.IsUpper(ch)) validation[1] = true; //Uppercase letter
            else if (Char.IsDigit(ch)) validation[2] = true; // A digit letter
            else if (special.Contains(ch)) validation[3] = true; // special letter
        }
    }
}
