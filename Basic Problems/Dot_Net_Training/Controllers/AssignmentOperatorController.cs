﻿using Dot_Net_Training.Models.AssignmentOperator;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Dot_Net_Training.Controllers
{
    [Route("api/assignmentOperator")]
    public class AssignmentOperatorController : ControllerBase
    {
        [HttpPost, Route("teemoAttacking")]
        public IActionResult FindPoisonedDuration(TeemoAttacking teemoAttacking)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                int[]? timeSeries = teemoAttacking.TimeSerise;
                int duration = teemoAttacking.Duration;

                int count = 0;
                for (int i = 1; i < timeSeries?.Length; i++)
                {
                    if (timeSeries[i] - timeSeries[i - 1] > duration)
                        count += duration;
                    else
                        count += timeSeries[i] - timeSeries[i - 1];
                }
                count += duration;
                return Ok(count);
            }
            catch (Exception)
            {

                throw;
            }
        }




        [HttpPost, Route("pivotIndex")]
        public IActionResult PivotIndex([FromBody] PivotIndex pivotIndex)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                int[]? number = pivotIndex.Number;

                int sumRight = number!.Sum();
                int sumLeft = 0;

                for (int i = 0; i < number?.Length; i++)
                {
                    sumRight -= number[i];

                    if (sumLeft == sumRight)
                    {
                        return Ok(i);
                    }

                    sumLeft += number[i];
                }

                return Ok(-1);
            }
            catch (Exception)
            {

                throw;
            }
        }




        [HttpPost, Route("stringToIntegerMapping")]
        public IActionResult FreqAlphabets(StringToIntegerMapping stringToIntegerMapping)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                string? value = stringToIntegerMapping.value;
                string result = "";
                for (int i = 0; i < value?.Length; i++)
                {
                    if (value.Length - 2 > i && value[i + 2] == '#')
                    {
                        string num = "";
                        num += value[i];
                        num += value[i + 1];
                        result += (char)(96 + int.Parse(num));
                        i += 2;
                    }
                    else
                    {
                        result += (char)(96 + (value[i] - '0'));
                    }
                }
                return Ok(result);
            }
            catch (Exception)
            {

                throw;
            }
        }



        [HttpPost, Route("differenceOfSum")]
        public IActionResult DifferenceSum([FromBody] DifferenceOfSum differenceOfSum)
        {
            try
            {
                int[]? number = differenceOfSum.Number;
                int sum = 0;
                int sum2 = 0;
                foreach (var n in number!)
                    sum += n;
                foreach (var n in number)
                {
                    int k = n;
                    while (k > 0)
                    {
                        int j = k % 10;
                        k /= 10;
                        sum2 += j;
                    }
                }
                return Ok(sum - sum2);
            }
            catch (Exception)
            {

                throw;
            }
        }




        [HttpPost, Route("primeNumberBinary")]
        public IActionResult CountPrimeBits([FromBody] PrimeNumberSetBinary primeNumberSetBinary)
        {

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                int left = primeNumberSetBinary.LeftNumber;
                int right = primeNumberSetBinary.RightNumber;

                int count = 0;
                for (int i = left; i <= right; i++)
                {
                    if (isPrime(FindBits(i)))
                    {
                        count++;
                    }
                }
                return Ok(count);
            }
            catch (Exception)
            {
                throw;
            }
        }

        static int FindBits(int inputBit)
        {
            int bitcount = 0;

            while (inputBit > 0)
            {
                if (inputBit % 2 == 1)
                {
                    bitcount++;
                }
                inputBit /= 2;
            }
            return bitcount;
        }

        static bool isPrime(int number)
        {
            if (number == 0 || number == 1)
            {
                return false;
            }

            for (int i = 2; i <= Math.Sqrt(number); i++)
            {
                if (number % i == 0)
                {
                    return false;
                }
            }
            return true;
        }

    }
}
