﻿using Dot_Net_Training.Models.BitwiseOperator;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Dot_Net_Training.Controllers
{
    [Route ("api/bitwiseOperator")]
    public class BitwiseOperatorController : ControllerBase
    {
        [HttpPost, Route("grayCode")]
        public ActionResult FindGrayCode(GrayCode grayCode)
        {
            try

            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values
                                               .SelectMany(x => x.Errors)
                                               .Select(x => x.ErrorMessage))
                    });
                }

                double number = grayCode.Number ?? 0.0;

                var result = new int[(int)Math.Pow(2, number)];

                for (var bitsPosition = 1; bitsPosition <= number; bitsPosition++)
                {
                    var bit = 1 << (bitsPosition - 1);

                    var length = 1 << bitsPosition;

                    for (int i = bit, ones = length; i < result.Length; i++)
                    {
                        result[i] = result[i] | bit;

                        if (--ones == 0)
                        {
                            i += length;
                            ones = length;
                        }
                    }
                }

                return Ok(result[0]);
            }

            catch (Exception)
            {
                throw;
            }
        }



        [HttpPost, Route ("missingNumber")]
        public IActionResult FindNumber(MissingNumber missingNumber)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values
                                               .SelectMany(x => x.Errors)
                                               .Select(x => x.ErrorMessage))
                    });
                }

                int[]? number = missingNumber.Number;
                int value = number!.Length; 

                for (int i = 0; i < number?.Length; i++)
                {
                    value ^= i ^ number[i];
                }

                return Ok(value);
            }
            catch (Exception)
            {

                throw;
            }
        }



        [HttpPost, Route("singleNumber")]
        public IActionResult GetNumber(SingleNumber singleNumber)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values
                                               .SelectMany(x => x.Errors)
                                               .Select(x => x.ErrorMessage))
                    });
                }

                int[]? value = singleNumber.Value;
                int result = value![0];

                for (int i = 1; i < value.Length; i++)
                {
                    result ^= value[i];
                }
                    

                return Ok ( result);
            }
            catch (Exception)
            {

                throw;
            }
        }



        [HttpPost, Route ("subsetXORSum")]
        public IActionResult FindsubsetXORSum(SubsetXORSum subsetXORSum)
        {
            try
            {
                int[]? number = subsetXORSum.Number;
                int result = 0;
                int powerOfNumbers = (int)Math.Pow(2, number!.Length);

                for (int i = 0; i < powerOfNumbers; i++)
                {
                    int tmp = 0;
                    for (int j = 0; j < number.Length; j++)
                    {
                        if ((i & (1 << j)) > 0)
                        {
                            tmp ^= number[j];
                        }
                    }
                    result += tmp;
                }
                return Ok(result);
            }
            catch (Exception)
            {

                throw;
            }
        }



        [HttpPost, Route("circularPermutationBitMasking")]
        public IActionResult FindCircularPermutation([FromBody] CircularPermutation circularPermutation)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                int number = circularPermutation.Number;
                int start = circularPermutation.Start;

                List<int> list = new();

                int i;

                for (i = 0; i < (1 << number); i++)
                {
                    list.Add(start ^ i ^ (i >> 1));
                }
                return Ok(list);

            }

            catch (Exception)
            {
                throw;
            }
        }
    }
}
