﻿using Dot_Net_Training.Models.BoxingAndUnboxing;
using System.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq.Expressions;

namespace Dot_Net_Training.Controllers
{
    [Route("api/boxing_And_Unboxing")]
    public class BoxingAndUnboxingController : ControllerBase
    {
        [HttpPost, Route("boxing")]
        public ActionResult Boxing(Boxing boxing)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                Stopwatch stopwatch1 = new Stopwatch();
                stopwatch1.Start();
                for (int i = 1; i < 1000000; i++)
                {
                    Boxing1();
                }
                stopwatch1.Stop();

                Stopwatch stopwatch2 = new Stopwatch();
                stopwatch2.Start();
                for (int i = 0; i < 1000000; i++)
                {
                    WithoutBoxing1();
                }
                stopwatch2.Stop();

                return Ok($"Boxing took : {stopwatch1.ElapsedMilliseconds} MS \n" + $"Withoutboxing took : {stopwatch2.ElapsedMilliseconds} Ms");
            }
            catch (Exception)
            {

                throw;
            }
        }
        //With Boxing
        public static void Boxing1()
        {
            Boxing obj = new Boxing();
            int digit = obj.FirstNumber;
            object digit1 = digit; //Boxing
        }

        //Without Boxing
        public static void WithoutBoxing1()
        {
            Boxing obj1 = new Boxing();
            int number = obj1.SecondNumber;
            int number2 = number; //No boxing and No Unboxing
        }


        [HttpPost, Route("unboxing")]
        public ActionResult Unboxing(Unboxing unboxing)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                Stopwatch stopwatch1 = new();
                stopwatch1.Start();
                for (int i = 1; i < 1000000; i++)
                {
                    Boxing2();
                }
                stopwatch1.Stop();

                Stopwatch stopwatch2 = new Stopwatch();
                stopwatch2.Start();
                for (int i = 0; i < 1000000; i++)
                {
                    WithoutBoxing2();
                }
                stopwatch2.Stop();

                return Ok($"Boxing took : {stopwatch1.ElapsedMilliseconds} MS \n" + $"Withoutboxing took : {stopwatch2.ElapsedMilliseconds} Ms");
            }
            catch (Exception)
            {

                throw;
            }
        }

        //With Unboxing
        public static void Boxing2()
        {
            Unboxing object1 = new Unboxing();
            object digit = object1.FirstNumber;
            int number = (int)digit; //Unboxing
        }

        //Without Boxing and Unboxing
        public static void WithoutBoxing2()
        {
            Unboxing object2 = new Unboxing();
            int number = object2.SecondNumber;
            int number2 = number; //No Boxing and No Unboxing
        }



        [HttpPost, Route("makeEqual")]
        public IActionResult MakeEqual(MakeEqual makeEqual)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                char[] letters = new char[26];
                string? inputValue = makeEqual.InputValue;
                object? words = inputValue;
                CountLetters(words!, letters);
                return Ok(CheckEqualDistribution(inputValue!.Length, letters));

                static void CountLetters(object inputValue, char[] letters)
                {
                    string[] words = (string[])inputValue;
                    foreach (var word in words)
                    {
                        foreach (var c in word)
                        {
                            letters[c - 'a']++;
                        }
                    }
                }
                static bool CheckEqualDistribution(int wordCount, char[] letters)
                {
                    foreach (var element in letters)
                    {
                        if (element % wordCount != 0)
                        {
                            return false;
                        }
                    }
                    return true;
                }


            }
            catch (Exception ex) 
            {
                throw;
            }
        }



        [HttpPost, Route("pancakeSort")]
        public IActionResult PancakeSort(PancakeSort pancakeSort)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                int[]? inputArray = pancakeSort.InputArray;

                void flip(object i)
                {
                    int index = (int)i;
                    int left = 0, right = index;
                    while (left < right)
                    {
                        object temp = inputArray[left];
                        inputArray[left] = inputArray[right];
                        inputArray[right] = (int)temp;
                        left++;
                        right--;
                    }
                }
                List<int> result = new();
                for (object i = inputArray!.Length - 1; (int)i >= 0; i = (int)i - 1)
                {
                    if (inputArray[(int)i] == (int)i + 1) continue;
                    for (object j = 1; (int)j < (int)i && inputArray[0] != (int)i + 1; j = (int)j + 1)
                    {
                        if (inputArray[(int)j] == (int)i + 1)
                        {
                            flip(j);
                            result.Add((int)j + 1);
                        }
                    }
                    flip(i);
                    result.Add((int)i + 1);
                }
                return Ok(result);
            }
            catch (Exception ex) 
            { 
                throw; 
            }
        }




        [HttpPost, Route("appendCharacters")]
        public IActionResult AppendCharacters(AppendCharacters appendCharacters)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                string? input1 = appendCharacters.Input1;
                string? input2 = appendCharacters.Input2;
                object stringInput = input1!;
                int startIndex = 0;
                for (int i = 0; i < input2!.Length; i++)
                {
                    int index = GetNextIndex(stringInput!, input2[i], startIndex);
                    if (index == -1)
                    {
                        return Ok(input2.Length - i);
                    }
                    else
                    {
                        startIndex = index + 1;
                    }
                }
                return Ok(0);

                static int GetNextIndex(object input1, char charter, int startIndex)
                {
                    string? stringInput = (string)input1!;

                    return stringInput!.IndexOf(charter, startIndex);
                }
            }
            catch(Exception ex)
            {
                throw;
            }
        }




        [HttpPost, Route("numTilePossibilities")]
        public IActionResult NumTilePossibilities(string tile)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                object tiles = tile;
                bool[] visit = new bool[tile.Length];
                HashSet<string> set = new();
                Solve(0, "", tiles, visit, set);
                return Ok(set.Count);
            
                static void Solve(int index, string inputString, object tile, bool[] visit, HashSet<string> set)
                {
                    string tiles = (string)tile;
                    if (inputString != "")
                        set.Add(inputString);
                    if (index >= tiles.Length)
                        return;
                    for (int i = 0; i < tiles.Length; i++)
                    {
                        if (visit[i]) continue;
                        visit[i] = true;
                        string inputString2 = inputString + tiles[i];
                        Solve(index + 1, inputString2, tiles, visit, set);
                        visit[i] = false;
                    }
                }
            }
            catch(Exception ex)
            {
               throw;
            }
            
        }

    }
}
