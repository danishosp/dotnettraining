﻿using Dot_Net_Training.Models.ForLoop;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Dot_Net_Training.Controllers
{
    [Route ("api/ForLoop")]
    public class ForLoopController : ControllerBase
    {
        #region TwoSum
        [HttpPost, Route("twoSum")]
        public ActionResult ToFindSum(TwoSum twoSum)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }

                int[]? inputArray = twoSum.InputArray;

                int[] result = new int[2];

                for (int i = 0; i < inputArray?.Length; i++)
                {
                    for (int j = i + 1; j < inputArray?.Length; j++)
                    {

                        if (inputArray[i] + inputArray[j] == twoSum.Target)
                        {
                            result[0] = i;
                            result[1] = j;

                        }
                    }
                }

                return Ok(result);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion


        #region CheckVowels
        [HttpPost, Route("checkaeiou")]
        public ActionResult Checkaeiou(Checkaeiou checkaeiou)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                string? name = checkaeiou.Name;
                bool result = Isvowel(name);

                return Ok(result ? "contain Vowel's" : "Does not contain vowel's");

            }
            catch (Exception)
            {

                throw;
            }

        }
        static bool Isvowel(string name)
        {
            name = name.ToLower();

            for (int i = 0; i < name.Length; i++)
            {
                if (name[i] == 'a' || name[i] == 'e' || name[i] == 'i' || name[i] == 'o' || name[i] == 'u')
                    return true;
            }
            return false;
        }
        #endregion


        #region FindZeroAndOnce
        [HttpPost, Route("findZeroAndOnce")]
        public ActionResult FindZeroAndOnce(FindZeroAndOnce findZeroAndOnce)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                int[]? number = findZeroAndOnce.Number;
                int[] cols = new int[3];
                for (int i = 0; i < number?.Length; i++)
                {
                    cols[number[i]]++;
                }
                int r = 0;
                #pragma warning disable
                for (; r < cols[0]; r++)
                {
                    number[r] = 0;
                }
                int zeroAndOnes = cols[0] + cols[1];
                for (; r < zeroAndOnes; r++)
                {
                    number[r] = 1;
                }
                for (; r < number.Length; r++)
                {
                    number[r] = 2;
                }

                return Ok(number);
            }
            catch (Exception)
            {

                throw;
            }
            
        }
        #endregion


        #region OperationsOnValue
        [HttpPost, Route("operationsOnValue")]
        public IActionResult FinalValueAfterOperations(OperationsOnValue operationsOnValue)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                string[]? operations = operationsOnValue.Operations;
                var x = 0;
                for (var i = 0; i < operations?.Length; i++)
                {
                    if (operations[i].Equals("++X") || operations[i].Equals("X++"))
                        x++;
                    if (operations[i].Equals("--X") || operations[i].Equals("X--"))
                        x--;
                }
                return Ok(x);
            }
            catch (Exception)
            {

                throw;
            }

        }
        #endregion


        #region RemoveDuplicateChar
        [HttpPost, Route("removeDuplicatechar")]
        public IActionResult RemoveDuplicateChar(RemoveDuplicateChar removeDuplicateChar)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                string? inputChar = removeDuplicateChar.Char;
                string resultString = "";
                for (int i = 0; i < inputChar?.Length; i++)
                {
                    if (!resultString.Contains(inputChar[i]))
                    {
                        resultString += inputChar[i];
                    }
                }
                return Ok(resultString);
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
    }
}
