﻿using Dot_Net_Training.Models.TernaryOperator;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Dot_Net_Training.Controllers
{
    [Route ("api/ternaryOperator")]
    public class TernaryOperatorController : ControllerBase
    {
        [HttpPost, Route("obtainZero")]
        public IActionResult ObtainZero([FromBody] ObtainZero obtainZero)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }



                int firstNumber = obtainZero.Digit1;
                int secondNumber = obtainZero.Digit2;



                if (firstNumber == 0 || secondNumber == 0)
                {
                    return Ok(0);
                }
                int large = firstNumber >= secondNumber ? firstNumber : secondNumber;
                int small = firstNumber < secondNumber ? firstNumber : secondNumber;
                int count = 0;
                bool greater;



                while (large - small >= 0)
                {
                    count++;
                    greater = large - small >= small;
                    firstNumber = greater ? large - small : small;
                    secondNumber = greater ? small : large - small;
                    large = firstNumber;
                    small = secondNumber;



                    if (small == 0)
                    {
                        return Ok(count);
                    }
                }
                return Ok(0);
            }



            catch (Exception)
            {
                throw;
            }
        }



        [HttpPost , Route ("largestNumber")]
        public IActionResult LargestNumber(LargestNumber largestNumber)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                int digit1 = largestNumber.Digit1;
                int digit2 = largestNumber.Digit2;
                int digit3 = largestNumber.Digit3;
                int LargestNumber = 0;

                LargestNumber = (digit1 > digit2) ? ((digit1 > digit3) ? (digit1) : (digit3)) : ((digit2 > digit3) ? (digit2) : (digit3));

                return Ok($"The Largest Number is: {LargestNumber}");


            }
            catch (Exception)
            {

                throw;
            }
        }



        [HttpPost , Route ("balancedStringSplits")]
        public IActionResult SplitsString(BalancedStringSplit balancedStringSplit )
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                string? value = balancedStringSplit.Value;
                int splits = 0, l_count = 0, r_count = 0;
                
                foreach (char c in value!)
                {
                   var result = (c == 'L') ? l_count++ : r_count++;
                   var result1 = (l_count == r_count) ? splits++ : splits;
                    
                }
                return Ok(splits);
            }
            catch (Exception)
            {

                throw;
            }
        }



        [HttpPost, Route("medianOfArray")]
        public IActionResult SortedArrayMedian([FromBody] MedianOfSortedArray medianOfSortedArray)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Message = string.Join(Environment.NewLine, ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }



                int[]? firstInput = medianOfSortedArray.FirstInput;
                int[]? secondInput = medianOfSortedArray.SecondInput;




                if (firstInput?.Length < secondInput?.Length)
                {
                    return Ok(FindMedian(secondInput, firstInput));
                }
                #pragma warning disable
                return Ok(FindMedian(firstInput, secondInput));



            }



            catch (Exception)
            {
                throw;
            }
        }
        static double FindMedian(int[] firstArray, int[] secondArray)
        {
            int length1 = firstArray.Length;
            int length2 = secondArray.Length;



            int start = 0;
            int end = length2 * 2;
            while (start <= end)
            {
                int middle2 = (start + end) / 2;
                int middle1 = length1 + length2 - middle2;



                double firstLength = (middle1 == 0) ? Int32.MinValue : firstArray[(middle1 - 1) / 2];
                double secondLength = (middle2 == 0) ? Int32.MinValue : secondArray[(middle2 - 1) / 2];



                double return1 = (middle1 == length1 * 2) ? Int32.MaxValue : firstArray[(middle1) / 2];
                double return2 = (middle2 == length2 * 2) ? Int32.MaxValue : secondArray[(middle2) / 2];



                if (firstLength > return2)
                {
                    start = middle2 + 1;
                }



                else if (secondLength > return1)
                {
                    end = middle2 - 1;
                }
                else
                {
                    return (Math.Max(firstLength, secondLength) + Math.Min(return1, return2)) / 2;
                }
            }
            return -1;
        }



        [HttpPost, Route("binaryWatch")]
        public IActionResult GetTime([FromBody] BinaryWatch binaryWatch)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Message = string.Join(Environment.NewLine, ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }



                int number = binaryWatch.Number;



                List<string> result = new List<string>();
                for (int hours = 0; hours < 12; hours++)
                {
                    for (int minutes = 0; minutes < 60; minutes++)
                    {
                        int d = 0;



                        for (int a = 0; a < 6; a++)
                        {
                            d += (((hours & (1 << a)) > 0) ? 1 : 0) + (((minutes & (1 << a)) > 0) ? 1 : 0);
                        }

                        if (d == number)
                        {
                            result.Add(hours.ToString() + ":" + minutes.ToString().PadLeft(2, '0'));
                        }

                    }
                }
                return Ok(result);
            }



            catch (Exception)
            {
                throw;
            }
        }

    }
}
