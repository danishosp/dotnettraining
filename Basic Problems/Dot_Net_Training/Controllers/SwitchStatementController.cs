﻿using System;
using System.Text;
using Dot_Net_Training.Models.SwitchStatement;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Dot_Net_Training.Controllers
{
    [Route ("api/switchStatement")]
    public class SwitchStatementController : ControllerBase
    {
        [HttpPost , Route ("base7")]
        public IActionResult ConverToBase7(Base7 base7 )
        {
            try
            {
                int number = base7.Number;
                var nums = "";
                var abs = "";

                switch (number)
                {
                    case 0:
                        {
                            return Ok(number.ToString());
                        }
                    case < 0:
                        {
                            number = Math.Abs(number);
                            abs += "-";
                            break;
                        }
                }

                while (number != 0)
                {
                    var rem = number % 7;
                    number = number / 7;
                    nums += rem;
                }
                nums = new string(nums.Reverse().ToArray());
                return Ok(abs + nums);
            }
            catch (Exception)
            {

                throw;
            }
        }



        [HttpPost , Route ("romanToInteger")]
        public IActionResult RomanToInt(RomanToInteger romanToInteger)
        {
            try
            {
                string? variables = romanToInteger.Variables;
                var chars = variables?.ToCharArray();
                var result = 0;
                var currentValue = 0;
                for (var i = 0; i < chars?.Length - 1; i++)
                {
                    currentValue = RomanNumerals(chars![i]);
                    result += (RomanNumerals(chars[i + 1]) > currentValue ? -1 : 1) * currentValue;
                }

                return Ok(result + RomanNumerals(chars![chars.Length - 1]));
            }
            catch (Exception)
            {

                throw;
            }
        }
        static int RomanNumerals(char c)
        {
            switch (c)
            {
                case 'I':
                    {
                        return 1;
                    }
                case 'V':
                    {
                        return 5;
                    }
                case 'X':
                    {
                        return 10;
                    }
                case 'L':
                    {
                        return 50;
                    }
                case 'C':
                    {
                        return 100;
                    }
                case 'D':
                    {
                        return 500;
                    }
                case 'M':
                    {
                        return 1000;
                    }
            };
            return 0;
        }



        [HttpPost, Route("stringCalculator")]
        public IActionResult FindResultString([FromBody] StringCalculator stringCalculator)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                string? inputString = stringCalculator.InputString; int result = 0;
                int i = 0;
                Stack<int> stack = new();
                int sign = 1;
                int number = 0;
                while (i < inputString?.Length)
                {
                    switch (inputString[i])
                    {
                        case '+':
                            sign = 1;
                            break;
                        case '-':
                            sign = -1;
                            break;
                        case '(':
                            stack.Push(result);
                            stack.Push(sign);
                            result = 0;
                            sign = 1;
                            break;
                        case ')':
                            int lastSign = stack.Pop();
                            result *= lastSign;
                            int lastNumber = stack.Pop();
                            result += lastNumber;
                            break;
                        case ' ':
                            break;
                        default:
                            if (char.IsDigit(inputString[i]))
                            {
                                StringBuilder stringBuilder = new();
                                stringBuilder.Append(inputString[i]);
                                while (i + 1 < inputString.Length && char.IsDigit(inputString[i + 1]))
                                {
                                    stringBuilder.Append(inputString[++i]);
                                }
                                number = int.Parse(stringBuilder.ToString());
                                number *= sign;
                                result += number;
                                sign = 1;
                            }
                            break;
                    }
                    i++;
                }
                return Ok(result);
            }
            catch (Exception)
            {
                throw;
            }
        }



        [HttpPost, Route("sortByPivot")]
        public IActionResult SortPivot([FromBody] PartitionPivot partitionPivot)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                int[]? inputArray = partitionPivot.InputArray;
                int pivot = partitionPivot.Pivot; var less = new List<int>();
                var equal = new List<int>();
                var greater = new List<int>();
                for (int i = 0; i < inputArray?.Length; i++)
                {
                    switch (inputArray[i])
                    {
                        case int x when x < pivot: less.Add(x); break;
                        case int x when x == pivot: equal.Add(x); break;
                        case int x when x > pivot: greater.Add(x); break;
                    }
                }
                less.AddRange(equal);
                less.AddRange(greater); return Ok(less.ToArray());
            }
            catch (Exception)
            {
                throw;
            }
        }



        [HttpPost, Route("baseballGame")]
        public IActionResult FindScore([FromBody] BaseballGame baseballGame)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                string[]? callPoints = baseballGame.CallPoints; 
                var countPoints = callPoints?.Length;
                var result = new List<int>();
                for (int i = 0; i < countPoints; i++)
                {
                    var run = result.Count - 1; switch (callPoints![i])
                    {
                        case "+":
                            result.Add(result[run - 1] + result[run]);
                            break;
                        case "D":
                            result.Add(result[run] * 2);
                            break;
                        case "C":
                            result.RemoveAt(run);
                            break;
                        default:
                            result.Add(int.Parse(callPoints[i]));
                            break;
                    }
                }
                return Ok(result.Sum());
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
