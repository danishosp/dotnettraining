﻿using System;
using Dot_Net_Training.Models.MuiltiDimensionalArray;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static System.Net.Mime.MediaTypeNames;

namespace Dot_Net_Training.Controllers
{
    [Route ("api/muiltiDimensionalArray")]
    public class MuiltiDimensionalArrayController : ControllerBase
    {
        [HttpPost, Route("richestCustomerWealth")]
        public IActionResult MaximumWeath([FromBody] RichestCustomerWealth richestCustomerWealth)
        {
            try
            {
                if (richestCustomerWealth == null)
                {
                    return Ok("Invalid Input");
                }

                int[][]? accounts = richestCustomerWealth.InputMatrix;

                int currentMax = int.MinValue;
                for (int i = 0; i < accounts?.Length; i++)
                {
                    if (accounts[i].Sum() > currentMax)
                    {
                        currentMax = accounts[i].Sum();
                    }
                }
                return Ok(currentMax);
            }

            catch (Exception)
            {
                throw;
            }
        }



        [HttpPost, Route("flippingImage")]
        public IActionResult InvertAndFlip([FromBody] FlippingImage flippingImage)
        {
            try
            {
                if (flippingImage.InputMatrix == null)
                {
                    return Ok("Invalid");
                }

                int[][]? image = flippingImage.InputMatrix;


                int[] temp = new int[image.Length];

                for (int i = 0; i < image.Length; i++)
                {
                    Array.Copy(image[i], temp, image[i].Length);
                    for (int j = 0; j < temp.Length; j++)
                    {
                        if (temp[j] == 1)
                            image[i][temp.Length - j - 1] = 0;
                        else
                            image[i][temp.Length - j - 1] = 1;
                    }
                }

                return Ok(image);
            }

            catch (Exception)
            {
                throw;
            }
        }



        [HttpPost, Route("matrixDiagonalSum")]
        public IActionResult DiagonalSum([FromBody]MatrixDiagonalSum matrixDiagonalSum )
        {
            try
            {
                int[][]? inputvalue = matrixDiagonalSum.InputValue;
                int sum = 0;
                for (int i = 0; i < inputvalue?.Length; i++)
                {
                    for (int j = 0; j < inputvalue.Length; j++)
                    {
                        if (i == j)
                        {
                            sum += inputvalue[i][j];
                            continue;
                        }
                        if (i + j == inputvalue.Length - 1)
                        {
                            sum += inputvalue[i][j];
                        }
                    }
                }
                return Ok(sum);
            }
            catch (Exception)
            {

                throw;
            }
        }




        [HttpPost, Route("transposeMatrix")]
        public IActionResult Transpose([FromBody]TransposeMatrix transposeMatrix)
        {
            try
            {
                int[][]? inputValue = transposeMatrix.InputValue;

                int[][] B = new int[inputValue![0].Length][];
                for (int i = 0; i < inputValue[0].Length; i++)
                {
                    B[i] = new int[inputValue.Length];
                }
                for (int i = 0; i < inputValue.Length; i++)
                {
                    for (int j = 0; j < inputValue[0].Length; j++)
                    {
                        B[j][i] = inputValue[i][j];
                    }
                }
                return Ok(B);
            }
            catch (Exception)
            {

                throw;
            }
        }




        [HttpPost , Route ("1DArrayInto2DArray")]
        public IActionResult Construct2DArrayLogic([FromBody] Convert1DArrayInto2DArray convert1DArrayInto2DArray)
        {
            int[]? userInput = convert1DArrayInto2DArray.Original;
            int row = convert1DArrayInto2DArray.Row;
            int colmn = convert1DArrayInto2DArray.Column;



            if (userInput!.Length != row * colmn)
            {
                return Ok(new int[0][]);
            }
            else
            {
                int[][] TwoDArray = new int[row][];
                for (int i = 0; i < row; i++)
                {
                    TwoDArray[i] = new int[colmn];
                    for (int j = 0; j < colmn; j++)
                    {
                        TwoDArray[i][j] = userInput[i * colmn + j];
                    }
                }
                return Ok(TwoDArray);
            }
        }
    }
}
