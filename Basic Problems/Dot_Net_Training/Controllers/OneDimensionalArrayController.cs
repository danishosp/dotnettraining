﻿using Dot_Net_Training.Models.OneDimensionalArray;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;

namespace Dot_Net_Training.Controllers
{
    [Route ("api/OneDimensionalArray")]
    public class OneDimensionalArrayController : ControllerBase
    {
        [HttpPost, Route("additionTwoValue")]
        public ActionResult ToFindSum(AdditionTwoValue additionTwoValue)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }

                int[]? inputArray = additionTwoValue.InputArray;

                int[] result = new int[2];

                for (int i = 0; i < inputArray?.Length; i++)
                {
                    for (int j = i + 1; j < inputArray?.Length; j++)
                    {

                        if (inputArray[i] + inputArray[j] == additionTwoValue.Target)
                        {
                            result[0] = i;
                            result[1] = j;

                        }
                    }
                }

                return Ok(result);
            }
            catch (Exception)
            {
                throw;
            }
        }



        [HttpPost, Route("OccurrenceArray")]
        public ActionResult Array(OccurrenceArray occurrenceArray)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                int[]? arrayInput = occurrenceArray.ArrayInput;
                int target = occurrenceArray.Target;
                int counter = 0;
                for (int i = 0; i < arrayInput.Length; i++)
                {
                    if (arrayInput[i] == target)
                    {
                        counter++;
                    }
                }
                return Ok(counter);
            }
            catch (Exception)
            {

                throw;
            }
        }




        [HttpPost, Route("nthDouble")]
        public ActionResult CheckNthArray(NthDouble nthDouble)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                int[]? arrayInput = nthDouble.ArrayInput;
                for (int i = 0; i < arrayInput?.Length; i++)
                {
                    for (int j = 0; j < arrayInput.Length; j++)
                    {
                        if (arrayInput[i] == 2 * arrayInput[j] & i != j)
                        {
                            return Ok(true);
                        }
                    }
                }
                return Ok(false);
            }
            catch (Exception)
            {

                throw;
            }
        }




        [HttpPost, Route("algorithm")]
        public IActionResult Algorithm([FromBody] Algorithm algorithm)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                int[]? array = algorithm.Number;
                int position = algorithm.KthIndex;

                Swap(array!, 0, (position - 1));
                Swap(array!, position, (array!.Length - 1));
                Swap(array, 0, (array.Length - 1));

                return Ok(array);



            }
            catch (Exception)
            {

                throw;
            }
        }
        static void Swap(int[] array, int startIndex, int endIndex)
        {
            while (startIndex < endIndex)
            {
                (array[endIndex], array[startIndex]) = (array[startIndex], array[endIndex]);
                startIndex++;
                endIndex--;
            }
        }



        [HttpPost, Route("insertionSort")]
        public IActionResult InsertionSort(InsertionSort insertionSort)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                int[]? inputArray = insertionSort.InputArray;

                for (int i = 0; i < inputArray?.Length - 1; i++)
                {
                    for (int j = i + 1; j > 0; j--)
                    {
                        if (inputArray![j] < inputArray[j - 1])
                        {
                            SwapNumbers(inputArray, j, j - 1);
                        }
                    }
                }

                return Ok(inputArray);
            }
            catch (Exception)
            {

                throw;
            }
        }

        static void SwapNumbers(int[] array, int first, int second)
        {
            (array[second], array[first]) = (array[first], array[second]);
        }

    }
    
}
