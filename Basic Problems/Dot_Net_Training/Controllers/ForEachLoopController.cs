﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Dot_Net_Training.Models.ForEachLoop;

namespace Dot_Net_Training.Controllers
{
    [Route ("api/forEachLoop")]
    public class ForEachLoopController : ControllerBase
    {
        #region Reverse_String
        [HttpPost, Route ("reverseString")]
        public IActionResult ReverseString(ReverseString reverseString)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                
                string? name = reverseString.Name;
                string result = "";

                foreach (char c in name!)
                {
                    result = c + result;
                }
                return Ok ($"Reverse String is : {result}");
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion



        #region Majority Element
        [HttpPost , Route ("majorityElement")]
        public IActionResult MajorityElement([FromBody]MajorityElement majorityElement)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                int[]? number = majorityElement.Number;
                int count = 0;
                int? candidate = null;

                foreach (int num in number!)
                {
                    if (count == 0) candidate = num;
                    count += (num == candidate) ? 1 : -1;
                }

                return Ok(candidate!.Value);
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion



        #region Minimum Time Difference
        [HttpPost, Route("minimumTimeDifference")]
        public IActionResult FindMinDifference([FromBody] MinimumTimeDifference minimumTimeDifference)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                IList<string>? points = minimumTimeDifference.Points;

                bool[] mark = new bool[24 * 60];


                foreach (String time in points!)
                {
                    String[] storeTime = time.Split(":");
                    int hours = int.Parse(storeTime[0]);
                    int minute = int.Parse(storeTime[1]);
                    if (mark[hours * 60 + minute])
                    {
                        return Ok(0);
                    }

                    mark[hours * 60 + minute] = true;
                }

                int prev = 0, min = int.MaxValue;
                int first = int.MaxValue, last = int.MinValue;
                for (int i = 0; i < 24 * 60; i++)
                {
                    if (mark[i])
                    {
                        if (first != int.MaxValue)
                        {
                            min = Math.Min(min, i - prev);
                        }
                        first = Math.Min(first, i);
                        last = Math.Max(last, i);
                        prev = i;
                    }
                }

                min = Math.Min(min, (24 * 60 - last + first));

                return Ok(min);
            }

            catch (Exception)
            {
                throw;
            }
        }
        #endregion



        #region Concatenation Of All Words
        [HttpPost, Route("concatenationOfAllWords")]
        public IActionResult ConcateAll([FromBody] ConcatenationOfAllWords concatenationOfAllWords)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                string? inputValue = concatenationOfAllWords.InputValue;
                string[]? words = concatenationOfAllWords.Words;

                var result = new List<int>();

                var wordsLenght = words?.Length;

                var sLenght = inputValue?.Length;

                if (sLenght == 0 || wordsLenght == 0)
                {
                    return Ok(result);
                }

                var wordLenght = words![0].Length;
                var concatLenght = wordsLenght * wordLenght;

                if (concatLenght > sLenght)
                {
                    return Ok(result);
                }

                var wordsMap = new Dictionary<string, int>();

                foreach (var word in words)
                {
                    if (wordsMap.ContainsKey(word))
                    {
                        wordsMap[word]++;
                    }
                    else
                    {
                        wordsMap[word] = 1;
                    }
                }
                IDictionary<string, int> used;

                int i, j, count;
                var subString = string.Empty;
                bool allUsed = false;
                for (i = 0; i <= sLenght - concatLenght; i++)
                {
                    used = new Dictionary<string, int>(wordsMap);

                    for (j = i; j <= sLenght - wordLenght; j += wordLenght)
                    {
                        subString = inputValue!.Substring(j, wordLenght);
                        if (used.TryGetValue(subString, out count))
                        {
                            if (count == 0)
                            {
                                break;
                            }

                            used[subString]--;
                        }

                        else
                        {
                            break;
                        }
                    }

                    allUsed = true;
                    foreach (var pair in used)
                    {
                        if (pair.Value > 0)
                        {
                            allUsed = false;
                            break;
                        }
                    }

                    if (allUsed) { result.Add(i); }
                }

                return Ok(result);
            }

            catch (Exception)
            {
                throw;
            }
        }
        #endregion


        #region PhoneDigits
        [HttpPost, Route("phoneDigits")]
        public IActionResult GetDigits([FromBody] PhoneNumberDigit phoneNumberDigit)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                string? stringInput = phoneNumberDigit.StringInput;

                string[] phoneChars = new string[] { " ",
                "",
                "abc",
                "def",
                "ghi",
                "jkl",
                "mno",
                "pqrs",
                "tuv",
                "wxyz"
                };

                if (stringInput.Length == 0)
                {
                    return Ok(new List<string>());
                }

                var results = new List<string>() { "" };

                foreach (var digit in stringInput)
                {
                    var keys = phoneChars[digit - '0'];

                    if (keys.Length == 0) continue;

                    var temp = new List<string>();

                    foreach (var result in results)
                    {
                        foreach (var ch in keys)
                        {
                            temp.Add(result + ch.ToString());
                        }
                    }

                    results = temp;
                }

                if (results.Count == 1 && results[0] == "")
                {
                    results.Clear();
                }

                return Ok(results);
            }

            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
