﻿using System;
using Dot_Net_Training.Models.WhileLoop;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Dot_Net_Training.Controllers
{
    [Route ("api/WhileLoop")]
    public class WhileLoopController : ControllerBase
    {
        [HttpPost, Route("reversalAlgorithm")]
        public IActionResult ReversalAlgorithm([FromBody] ReversalAlgorithm reversalAlgo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                int[]? array = reversalAlgo.Number;
                int position = reversalAlgo.KthIndex;

                SwapElement(array!, 0, (position - 1));
                SwapElement(array!, position, (array!.Length - 1));
                SwapElement(array, 0, (array.Length - 1));

                return Ok(array);


            }
            catch (Exception)
            {

                throw;
            }
        }
        static void SwapElement(int[] array, int startIndex, int endIndex)
        {
            while (startIndex < endIndex)
            {
                (array[endIndex], array[startIndex]) = (array[startIndex], array[endIndex]);
                startIndex++;
                endIndex--;
            }
        }



        [HttpPost, Route ("insertPosition")]
        public IActionResult InsertPosition(InsertPosition insertPosition)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                int[] number = insertPosition.Number;
                int target = insertPosition.Target;

                int i = 0;
                int j = number.Length - 1;
                while (i <= j)
                {
                    int mid = i + (j - i) / 2;
                    if (target < number[mid])
                        j = mid - 1;
                    else if (target > number[mid])
                        i = mid + 1;
                    else
                        return Ok(mid);
                }
                return Ok(i);
            }
            catch (Exception)
            {

                throw;
            }
        }



        [HttpPost, Route ("squreRoot")]
        public IActionResult SqureRoot(SqureRoot squreRoot)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }


                
                int number = squreRoot.InputNumber;
                long result = number;
                while (result * result > number)
                {
                    result = (result + number / result) / 2;
                }
                    

                return Ok((int)result);
            }
            catch (Exception)
            {

                throw;
            }
        }



        [HttpPost , Route ("sortEvenAndOddIndices")]
        public IActionResult SortEvenAndOdd([FromBody]SortEvenAndOdd sortEvenAndOdd)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                int[]? number = sortEvenAndOdd.Number;
                var dict = new Dictionary<int, List<int>>();
                int[] map = new int[101];
                for (int i = 0; i < number.Length; i++)
                {
                    if (dict.ContainsKey(number[i]))
                    {
                        dict[number[i]].Add(i);
                    }
                    else
                    {
                        dict[number[i]] = new List<int>() { i };
                    }
                    map[number[i]]++;
                }

                int even = 0;
                int j = 0;
                while (j < map.Length)
                {
                    if (map[j] > 0)
                    {
                        foreach (var index in dict[j])
                        {
                            if (index % 2 == 0)
                            {
                                number[even] = j;
                                even += 2;
                                map[j]--;
                            }
                        }
                    }
                    j++;
                }

                j = map.Length - 1;
                int odd = 1;
                while (j >= 0)
                {
                    if (map[j] > 0)
                    {
                        foreach (var index in dict[j])
                        {
                            if (index % 2 == 1)
                            {
                                number[odd] = j;
                                odd += 2;
                                map[j]--;
                            }
                        }
                    }
                    j--;
                }
                return Ok(number);
            }
            
            catch (Exception)
            {

                throw;
            }
        }




        [HttpPost , Route ("countDigit")]
        public IActionResult CountDigit(CountDigit countDigit)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                int number = countDigit.Number;
                int n = number, result = 0;
                while (number > 0)
                {
                    if (n % (number % 10) == 0)
                        result++;
                    number = number / 10;
                }



                return Ok(result);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
