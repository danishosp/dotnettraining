﻿using Dot_Net_Training.Models.ComparisonOperator; 
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Dot_Net_Training.Controllers
{
    [Route("api/comparisonOperator")]
    public class ComparisonOperatorController : ControllerBase
    {
        [HttpPost, Route("twoHouseWithDiffColor")]
        public IActionResult MaxDistance([FromBody] TwoHouse twoHouse)
        {
            try
            {
                int[]? inputNumber = twoHouse.InputNumber;
                int max = 0, length = inputNumber!.Length;
                for (int i = 0; i < length; i++)
                {
                    if (inputNumber[0] != inputNumber[i])
                    {
                        max = Math.Max(max, i);
                    }
                    if (inputNumber[length - 1] != inputNumber[i])
                    {
                        max = Math.Max(max, length - i - 1);
                    }
                }
                return Ok(max);
            }
            catch (Exception)
            {

                throw;
            }
        }



        [HttpPost, Route("projectionArea")]
        public IActionResult ProjectionArea([FromBody] ProjectionArea projectionArea)
        {
            try
            {
                int[][]? inputNumber = projectionArea.InputValue;
                var n = inputNumber?.Length;
                int zView = 0, xView = 0, yView = 0;

                for (var i = 0; i < n; i++)
                {
                    var yMax = 0;
                    var xMax = 0;

                    for (var j = 0; j < n; j++)
                    {
                        if (inputNumber![j][i] > yMax)
                        {
                            yMax = inputNumber[j][i];
                        }

                        if (inputNumber[i][j] > xMax)
                        {
                            xMax = inputNumber[i][j];
                        }

                        if (inputNumber[i][j] <= 0) continue;

                        zView += 1;
                    }

                    xView += xMax;
                    yView += yMax;
                }

                var area = zView + xView + yView;

                return Ok(area);
            }

            catch (Exception)
            {

                throw;
            }
        }



        [HttpPost, Route("threeDivisor")]
        public IActionResult Isprime(ThreeDivisors threeDivisors)
        {
            try
            {
                int number = threeDivisors.Value;

                if (number <= 3)
                    return Ok(false);

                int d = (int)(Math.Sqrt(number) + 0.5);

                if (number != d * d)
                    return Ok(false);

                return Ok(IsPrime(d));

            }

            catch (Exception)
            {

                throw;
            }
        }

        static bool IsPrime(int value)
        {
            if (value <= 2)
                return 2 == value;

            if (value % 2 == 0)
                return false;

            int n = (int)(Math.Sqrt(value) + 1);

            for (int d = 3; d <= n; d += 2)
                if (value % d == 0)
                    return false;

            return true;
        }



        [HttpPost, Route("MaximumSubArray")]
        public IActionResult FindMaxSubArray([FromBody] MaximumSubArray maximumSubArray)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                int[]? numberArray = maximumSubArray.InputArray;

                int max = numberArray![0];
                int sum = 0;
                for (int i = 0; i < numberArray.Length; i++)
                {
                    sum += numberArray[i];
                    if (sum > max)
                    {
                        max = sum;
                    }
                    if (sum < 0)
                    {
                        sum = 0;
                    }
                }
                return Ok(max);
            }
            catch (Exception)
            {
                throw;
            }
        }



        [HttpPost, Route("reversePolishNation")]
        public IActionResult EvaluateRPN([FromBody] ReversePolishNation reversePolishNation)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                string[]? tokens = reversePolishNation.InputTokens; 
                Stack<int> stack = new(); foreach (string token in tokens!)
                {
                    if (token is "+" or "-" or "*" or "/")
                    {
                        int right = stack.Pop();
                        int left = stack.Pop();
                        int result = token switch
                        {
                            "+" => left + right,
                            "-" => left - right,
                            "*" => left * right,
                            "/" => left / right,
                            _ => throw new ArgumentOutOfRangeException(nameof(token))
                        };
                        stack.Push(result);
                    }
                    else
                    {
                        stack.Push(int.Parse(token));
                    }
                }
                return Ok(stack.Pop());
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
