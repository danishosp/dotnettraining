﻿using System;
using System.Security.Cryptography.X509Certificates;
using Dot_Net_Training.Models.IfStatement;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Dot_Net_Training.Controllers
{
    [Route ("api/if_Statment")]
    public class IfStatementController : ControllerBase
    {
        [HttpPost, Route("finalValueAfterOperations")]
        public IActionResult FinalValueAfterOperations(FinalValueAfterOperations finalValueAfterOperations)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                string[]? operations = finalValueAfterOperations.Operations;
                var x = 0;
                for (var i = 0; i < operations?.Length; i++)
                {
                    if (operations[i].Equals("++X") || operations[i].Equals("X++"))
                        x++;
                    if (operations[i].Equals("--X") || operations[i].Equals("X--"))
                        x--;
                }
                return Ok(x);
            }
            catch (Exception)
            {

                throw;
            }

        }



        [HttpPost, Route("minimumMoney")]
        public IActionResult GetMinimumMoney(MinimumMoney minimumMoney)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                int money = minimumMoney.Money;
                int children = minimumMoney.Children;
                money -= children;
                if (money < 0)
                    return Ok(-1);
                if (money / 7 == children && money % 7 == 0)
                    return Ok(children);
                if (money / 7 == children - 1 && money % 7 == 3)
                    return Ok(children - 2);
                return Ok(Math.Min(children - 1, money / 7));


            }
            catch (Exception)
            {

                throw;
            }
        }



        [HttpPost, Route("categorizeBox")]
        public IActionResult FindBoxCategorize(CategorizeBox categorizeBox)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                int lenght = categorizeBox.Length;
                int width = categorizeBox.Width;
                int height = categorizeBox.Height;
                int mass = categorizeBox.Mass;

                long volume = lenght * width * height;
                bool heavy = mass >= 100;
                bool bulky = volume >= Math.Pow(10, 9) || lenght >= Math.Pow(10, 4) || width >= Math.Pow(10, 4) || height >= Math.Pow(10, 4);
                if (heavy && bulky)
                {
                    return Ok("Both");
                }

                if (heavy)
                {
                    return Ok("Heavy");
                }

                if (bulky)
                {
                    return Ok("Bulky");
                }

                return Ok("Neither");

            }
            catch (Exception)
            {

                throw;
            }
        }



        [HttpPost , Route ("pokerHand")]
        public IActionResult BestHand([FromBody]PokerHand pokerHand)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                int[]? ranks = pokerHand.Ranks;
                Char[]? suits = pokerHand.Suits;
                
                int[] suitMap = new int[4], rankMap = new int[14];
                int maxRankCount = 0, maxSuitCount = 0;
                for (var i = 0; i < ranks?.Length; i++)
                {
                    suitMap[suits![i] - 'a']++;
                    maxSuitCount = Math.Max(suitMap[suits[i] - 'a'], maxSuitCount);
                    rankMap[ranks[i]]++;
                    maxRankCount = Math.Max(rankMap[ranks[i]], maxRankCount);
                }

                if (maxSuitCount == suits!.Length)
                {
                    return Ok("Flush");
                }

                if (maxRankCount >= 3)
                {
                    return Ok("Three of a Kind");
                }
                if (maxRankCount == 2)
                {
                    return Ok("Pair");
                }
                return Ok("High Card");
            }
            catch (Exception)
            {

                throw;
            }
        }



        [HttpPost , Route ("nextGreaterElement")]
        public IActionResult NextGreaterElement(NextGreaterElement nextGreaterElement)
        {
            try
            {
                int[]? number1 = nextGreaterElement.Number1;
                int[]? number2 = nextGreaterElement.Number2;
                int[] answer = new int[number1!.Length];

                for (int i = 0; i < number1.Length; i++)
                {
                    bool found = false;
                    bool signed = false;
                    for (int j = 0; j < number2?.Length; j++)
                    {
                        if (found && number2[j] > number1[i])
                        {
                            answer[i] = number2[j];
                            signed = true;
                            break;
                        }
                        if (number2[j] == number1[i])
                        {
                            found = true;
                        }
                    }
                    if (signed == false) answer[i] = -1;
                }


                return Ok(answer);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
