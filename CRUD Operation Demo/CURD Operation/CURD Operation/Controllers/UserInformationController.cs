﻿using Core.Interface;
using Core.Models;
using Infrastructure.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static Core.Models.DtoUserInformationByID;
using static Core.Models.UserInformationList;

namespace CURD_Operation.Controllers
{
    [Route("api/userInformation")]
    public class UserInformationController : ControllerBase
    {
        private readonly IUserInformationRepository _userInformationRepository;

        public UserInformationController(IUserInformationRepository userInformationRepository)
        {
            _userInformationRepository = userInformationRepository;
        }


        #region Insert Data
        [HttpPost, Route("insertUserInformationDetails")]
        public async Task<ActionResult> AddData([FromBody] DtoUserInformationInsert userInformation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new
                {
                    Status = false,
                    Message = string.Join(Environment.NewLine, ModelState.Values
                                            .SelectMany(x => x.Errors)
                                            .Select(x => x.ErrorMessage))
                });
            }
            var response = await _userInformationRepository.AddAsync(userInformation);

            return Ok(response);
        }
        #endregion



        #region Update Data
        [HttpPut, Route("updateUserInformationDetails")]
        public async Task<ActionResult> UpdateData([FromBody] DtoUserInformationUpdate userInformationUpdate)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new
                {
                    Status = false,
                    Message = string.Join(Environment.NewLine, ModelState.Values
                                            .SelectMany(x => x.Errors)
                                            .Select(x => x.ErrorMessage))
                });
            }
            var response = await _userInformationRepository.UpdateAsync(userInformationUpdate);

            return Ok(response);
        }
        #endregion



        #region Delete Data
        [HttpDelete, Route("deleteUserInformationDetails")]
        public async Task<ActionResult> AddData([FromBody] DtoUserInformationDelete userInformation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new
                {
                    Status = false,
                    Message = string.Join(Environment.NewLine, ModelState.Values
                                            .SelectMany(x => x.Errors)
                                            .Select(x => x.ErrorMessage))
                });
            }
            var response = await _userInformationRepository.DeleteAsync(userInformation);

            return Ok(response);
        }
        #endregion



        #region Get All Data
        [HttpGet, Route("getAllUserInfoList")]
        public IActionResult GetAllData(UserInformationList userInformationList)
        {
            UserInformationList userInformation = new()
            {
                Start = 0,
                PageSize = 10,
                SortCol = "",
                SearchKey = ""
            };

            ResponseList result = _userInformationRepository.GetAllUserList(userInformationList);
            return Ok(result);
        }
        #endregion



        #region GetBy Id
        [HttpGet, Route("GetUserInfoByID")]
        public IActionResult GetIDData(DtoUserInformationByID dtoUserInformationByID)
        {
            ResponseListID result = _userInformationRepository.UserInformationByID(dtoUserInformationByID);

            return Ok(result);
        }
        #endregion
    }
}
