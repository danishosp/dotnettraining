﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Models
{
    public class DtoUserInformationInsert
    {
        public Guid? UserID { get; set; }

        public string? Name { get; set; }

        public int Age { get; set; }

        public string? Address { get; set; }

        [Required, RegularExpression(@"^\d{3}-\d{3}-\d{4}$")]
        public string? MobileNo { get; set; }

        public Guid CreatedBy { get; set; }
    }

    public class DtoUserInformationUpdate
    {
        public Guid? UserID { get; set; }

        public string? Name { get; set; }

        public int Age { get; set; }

        public string? Address { get; set; }

        [Required, RegularExpression(@"^\d{3}-\d{3}-\d{4}$")]
        public string? MobileNo { get; set; }

        public DateTime UpdatedON { get; set; }

        public Guid UpdatedBY { get; set; }
    }

    public class DtoUserInformationDelete
    {
        public Guid? UserID { get; set; }

        public DateTime DeletedON { get; set; }

        public Guid DeletedBY { get; set; }
    }

    public class DtoUserInformationByID
    {
        public Guid? UserID { get; set; }

        public class ResponseListID
        {
            public List<UserIdInfoList>? Data { get; set; }

            public class UserIdInfoList
            {
                public string? Name { get; set; }

                public int Age { get; set; }

                public string? Address { get; set; }

                [Required, RegularExpression(@"^\d{3}-\d{3}-\d{4}$")]
                public string? MobileNo { get; set; }
            }
        }
    }
}
