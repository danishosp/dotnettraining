﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class UserInformationList
    {
        public int Start { get; set; }

        public int PageSize { get; set; }

        public string? SortCol { get; set; }

        public string? SearchKey { get; set; }

        public class ResponseList
        {
            public string? Message { get; set; }

            public int TotalRecords { get; set; }

            public List<UserInfoList>? Data { get; set; }

            public string? Status { get; set; }

            public class UserInfoList
            {
                public Guid? UserID { get; set; }

                public string? Name { get; set; }

                public int Age { get; set; }

                public string? Address { get; set; }

                [Required, RegularExpression(@"^\d{3}-\d{3}-\d{4}$")]
                public string? MobileNo { get; set; }

                public Guid CreatedBy { get; set; }

                public DateTime CreatedON { get; set; }

                public DateTime UpdatedON { get; set; }

                public Guid UpdatedBY { get; set; }

                public DateTime DeletedON { get; set; }

                public Guid DeletedBY { get; set; }
            }
        }

    }
}

