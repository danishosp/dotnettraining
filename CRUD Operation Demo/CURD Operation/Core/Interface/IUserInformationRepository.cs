﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using static Core.Models.DtoUserInformationByID;
using static Core.Models.UserInformationList;

namespace Core.Interface
{
    public interface IUserInformationRepository
    {
        Task<Response> AddAsync(DtoUserInformationInsert userInformationInsert);

        Task<Response> UpdateAsync(DtoUserInformationUpdate userInformationUpdate);

        Task<Response> DeleteAsync(DtoUserInformationDelete userInformationDelete);

        ResponseList GetAllUserList(UserInformationList userInformationList);

        ResponseListID UserInformationByID(DtoUserInformationByID dtoUserInformationByID);
    }
}
