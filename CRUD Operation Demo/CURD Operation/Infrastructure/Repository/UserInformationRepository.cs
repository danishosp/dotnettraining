﻿using System.Data.SqlClient;
using System.Data;
using Microsoft.Extensions.Configuration;
using Dapper;
using Core.Models;
using Core.Interface;
using static Core.Models.UserInformationList;
using static Core.Models.UserInformationList.ResponseList;
using static Core.Models.DtoUserInformationByID;
using static Core.Models.DtoUserInformationByID.ResponseListID;

namespace Infrastructure.Repository
{
    public class UserInformationRepository : IUserInformationRepository
    {
        private readonly IConfiguration _configuration;

        private static string _connectionString = string.Empty;

        public UserInformationRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = _configuration["ConnectionStrings:DemoDB"]!;
        }

        public static IDbConnection Connection
        {
            get
            {
                return new SqlConnection(_connectionString);
            }
        }

        public async Task<Response> AddAsync(DtoUserInformationInsert userInformationInsert)
        {
            Response response;
            using IDbConnection db = Connection;
            response = await db.QueryFirstOrDefaultAsync<Response>("[uspInsertIntoUserInfo]", new
            {
                userInformationInsert.Name,
                userInformationInsert.Age,
                userInformationInsert.Address,
                userInformationInsert.MobileNo,
                userInformationInsert.CreatedBy

            }, commandType: CommandType.StoredProcedure).ConfigureAwait(false);

            return response;
        }

        public async Task<Response> UpdateAsync(DtoUserInformationUpdate userInformationUpdate)
        {
            Response response;
            using IDbConnection db = Connection;
            response = await db.QueryFirstOrDefaultAsync<Response>("[uspUpdateIntoUserInfo]", new
            {
                NewUserId = userInformationUpdate.UserID,
                NewName = userInformationUpdate.Name,
                NewAge = userInformationUpdate.Age,
                NewAddress = userInformationUpdate.Address,
                @NewMobileNo = userInformationUpdate.MobileNo,
                userInformationUpdate.UpdatedON,
                userInformationUpdate.UpdatedBY

            }, commandType: CommandType.StoredProcedure).ConfigureAwait(false);

            return response;
        }

        public async Task<Response> DeleteAsync(DtoUserInformationDelete userInformationDelete)
        {
            Response response;
            using IDbConnection db = Connection;
            response = await db.QueryFirstOrDefaultAsync<Response>("[uspDeletedFromUserInfo]", new
            {
                userInformationDelete.UserID,
                userInformationDelete.DeletedON,
                userInformationDelete.DeletedBY


            }, commandType: CommandType.StoredProcedure).ConfigureAwait(false);

            return response;
        }


        public ResponseList GetAllUserList(UserInformationList userInformationList)
        {
            ResponseList response = new();
            using IDbConnection db = Connection;
            var result = db.QueryMultiple("[uspGetAllUserInfo]", new
            {
                userInformationList.Start,
                userInformationList.PageSize,
                userInformationList.SortCol,
                userInformationList.SearchKey

            }, commandType: CommandType.StoredProcedure);


            response = result.Read<ResponseList>().FirstOrDefault()!;
            response.Data = result.Read<UserInfoList>().ToList();
            response.TotalRecords = result.Read<int>().FirstOrDefault();
            return response;
        }

        public ResponseListID UserInformationByID(DtoUserInformationByID dtoUserInformationByID)
        {
            ResponseListID response;
            using IDbConnection db = Connection;
            var result = db.QueryMultiple("[dbo].[uspGetUserInfoByID]", new
            {
                NewUserId = dtoUserInformationByID.UserID

            }, commandType: CommandType.StoredProcedure);


            response = result.Read<ResponseListID>().FirstOrDefault()!;
            response.Data = result.Read<UserIdInfoList>().ToList();
            
            return response;
        }
    }
}
