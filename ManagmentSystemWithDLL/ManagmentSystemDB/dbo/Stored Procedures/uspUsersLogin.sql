﻿CREATE PROC [dbo].[uspUsersLogin]
(
	@Email varchar(50)
)
AS
BEGIN
	BEGIN TRY

		SET NOCOUNT ON;

		IF NOT EXISTS(SELECT 1 FROM tblUsers WITH (NOLOCK) WHERE Email = Trim(@Email) AND IsDelete = 0)
		BEGIN
			SELECT 0 [Status], 'User does not Exist' [Message]
			RETURN;
		END

		IF EXISTS(SELECT 1 FROM tblUsers WITH (NOLOCK) WHERE Email = TRIM(@Email) AND IsDelete = 0 AND IsTempPassword = 1)
		BEGIN
			SELECT 0 [Status],'Please Reset Password first' [Message]
			RETURN;
		END

		IF EXISTS(SELECT 1 FROM tblUsers WITH (NOLOCK) WHERE Email = Trim(@Email) AND IsActive = 0)
		BEGIN
			SELECT 0 [Status],'User is Deactivated' [Message]
			RETURN;
		END

		IF EXISTS (SELECT 1 FROM [dbo].[tblUsers] WITH (NOLOCK) WHERE Email = Trim(@Email) AND IsTempPassword = 0) 
        BEGIN
            SELECT 1 [Status], 'Success' [Message]  
			SELECT UserId, Email, PasswordHash, PasswordSalt FROM tblUsers WHERE Email = @Email
			RETURN;
		END

		ELSE
        BEGIN 
            SELECT 0 [Status] 
			SELECT 'Failure' [Message]               
            RETURN 
		END
		
	END TRY

	BEGIN CATCH

		DECLARE @Msg VARCHAR(MAX) =Error_message()
        DECLARE @ErrorSeverity INT=Error_severity()
        DECLARE @ErrorState INT=Error_state()
        RAISERROR(@Msg, @ErrorSeverity, @ErrorState)
        SELECT 0 AS [Status], @Msg AS [Message]

	END CATCH

END

--select * from tblUsers

--update tblUsers set Isactive = 0 where UserId = 'D547D3B0-5625-435F-8A12-2041222445B1'