﻿CREATE PROC [dbo].[uspUserGetById]
(
	@UserId VARCHAR(50)
)
AS
BEGIN
	BEGIN TRY
		
		SET NOCOUNT ON;

		IF EXISTS(SELECT 1 FROM [dbo].[tblUsers] WITH(NOLOCK) WHERE UserId = @UserId AND IsDelete =0)
		BEGIN
			SELECT 1 AS [Status], 'UserId Data is been successfully fetched.' AS [Message]

			SELECT  [FirstName], 
					[LastName], 
					[MobileNo], 
					[Address], 
					[Email],   
					[IsActive], 
					[CreatedOn]
			FROM tblUsers
			WHERE UserId = @UserId
		END
		ELSE
		BEGIN
			SELECT 'Given UserId value does not exists please check and try again.' AS [Message] , 0 as [Status]
		END
	END TRY

	BEGIN CATCH
		DECLARE @Msg VARCHAR(MAX) =Error_message()
        DECLARE @ErrorSeverity INT=Error_severity()
        DECLARE @ErrorState INT=Error_state()
        RAISERROR(@Msg, @ErrorSeverity, @ErrorState)
        SELECT 0 AS [Status], @Msg AS [Message]
	END CATCH

END