﻿CREATE PROC uspUsersDelete
(
	@UserId	   VARCHAR(50),
	@Deletedby VARCHAR(50)
)
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM [dbo].[tblUsers] WITH(NOLOCK) WHERE UserId = @UserId AND IsDelete = 0)

		BEGIN
			
			UPDATE [dbo].[tblUsers]
			SET IsDelete = 1,
				DeletedOn = GETUTCDATE(),
				DeletedBy = @Deletedby

			WHERE UserId = @UserId

			SELECT 1 [Status], 'Data have been deleted successfully' [Message]
        END
    ELSE 
        BEGIN
        	SELECT 0 [Status], 'Given UserId value does not exists please check and try again.' [Message]
        END
END
