﻿CREATE PROC [dbo].[uspUserTempPasswordUpdate] --drop proc uspTempPasswordUpdate
(
	@Email VARCHAR(50),
	@PasswordHash VARBINARY(MAX),
	@PasswordSalt VARBINARY(MAX)
)
AS
BEGIN
	SET NOCOUNT ON

	BEGIN TRY

		IF EXISTS(SELECT 1 FROM [dbo].[tblUsers] WITH (NOLOCK) WHERE Email = Trim(@Email) AND IsTempPassword = 0 AND IsActive = 1 AND IsDelete = 0)
		BEGIN
			SELECT 0 [Status], 'Temporary Password is Not Valid' [Message]
			RETURN
		END

		IF EXISTS (SELECT 1 FROM [dbo].[tblUsers] WITH (NOLOCK) WHERE Email = Trim(@Email) AND IsActive = 1 AND IsTempPassword = 1) 
        BEGIN
            SELECT 1 [Status], 'Updated Successfully' [Message]  
			UPDATE [dbo].[tblUsers] SET PasswordHash = @PasswordHash , PasswordSalt = @PasswordSalt, IsTempPassword = 0 WHERE Email = @Email
			RETURN
		END
		ELSE
        BEGIN 
            SELECT 0 [Status], 'Updated UnSuccessfully' [Message]               
            RETURN 
		END
		
	END TRY

	BEGIN CATCH

		DECLARE @Msg VARCHAR(MAX) =Error_message()
        DECLARE @ErrorSeverity INT=Error_severity()
        DECLARE @ErrorState INT=Error_state()
        RAISERROR(@Msg, @ErrorSeverity, @ErrorState)
        SELECT 0 AS [Status], @Msg AS [Message]

	END CATCH

END

--SELECT * FROM TBLUSERS