﻿CREATE PROC [dbo].[uspResetPassword]
(
	@Email VARCHAR(50)
)
AS
BEGIN
	SET NOCOUNT ON

	BEGIN TRY

		IF NOT EXISTS(SELECT 1 FROM [dbo].[tblUsers] WITH (NOLOCK) WHERE Email = @Email AND IsDelete = 0)
		BEGIN
			SELECT 0 [Status], 'User Not Found' [Message]
			RETURN
		END

		IF EXISTS(SELECT 1 FROM [dbo].[tblUsers] WITH (NOLOCK) WHERE Email = @Email AND IsActive = 0 AND IsDelete = 0)
		BEGIN
			SELECT 0 [Status], 'User is Deactivated' [Message]
			RETURN
		END

		IF EXISTS (SELECT 1 FROM [dbo].[tblUsers] WITH (NOLOCK) WHERE Email = Trim(@Email) AND IsActive = 1) 
        BEGIN
            SELECT 1 [Status], 'Success' [Message]  
			SELECT UserId, Email, PasswordHash, PasswordSalt FROM tblUsers WHERE Email = @Email
			RETURN
		END
		ELSE
        BEGIN 
            SELECT 0 [Status], 'Failure' [Message]               
            RETURN 
		END
		
	END TRY

	BEGIN CATCH

		DECLARE @Msg VARCHAR(MAX) =Error_message()
        DECLARE @ErrorSeverity INT=Error_severity()
        DECLARE @ErrorState INT=Error_state()
        RAISERROR(@Msg, @ErrorSeverity, @ErrorState)
        SELECT 0 AS [Status], @Msg AS [Message]

	END CATCH

END


