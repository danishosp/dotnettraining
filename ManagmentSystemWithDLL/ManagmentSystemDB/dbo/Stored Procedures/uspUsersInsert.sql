﻿CREATE PROC [dbo].[uspUsersInsert]
(
	@FirstName	  VARCHAR(50), 
	@LastName	  VARCHAR(50), 
	@MobileNo	  VARCHAR(25), 
	@Address	  VARCHAR(MAX), 
	@Email		  VARCHAR(50),
	@PasswordHash VARBINARY(MAX),
	@PasswordSalt VARBINARY(MAX)
)
AS
BEGIN

	SET NOCOUNT ON;

	SET @FirstName = TRIM(@FirstName);
   	SET @LastName  = TRIM(@LastName);
   	SET @Address   = TRIM(@Address);
	SET @Email	   = TRIM(@Email);



		
		IF NOT EXISTS(SELECT 1 FROM [dbo].[tblUsers] WITH(NOLOCK) WHERE Email = @Email AND IsDelete = 0)
		BEGIN

			INSERT INTO tblUsers
			(
				FirstName,
				LastName,
				MobileNo,
				[Address],
				Email,
				PasswordHash,
				PasswordSalt,
				CreatedBy
			)
			VALUES
			(
				@FirstName,
				@LastName,
				@MobileNo,
				@Address,
				@Email,
				@PasswordHash,
				@PasswordSalt,
				NEWID()
			)

			SELECT 1 [Status], 'User is been inserted Successfully' [Message]
       END

   	ELSE

       BEGIN
           SELECT 0 [Status], 'User already exists.' [Message]
       END

END