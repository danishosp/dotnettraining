﻿CREATE TABLE [dbo].[tblUsers] (
    [UserId]         VARCHAR (50)    CONSTRAINT [DF_Users_UserId] DEFAULT (newid()) NOT NULL,
    [FirstName]      VARCHAR (50)    NOT NULL,
    [LastName]       VARCHAR (50)    NOT NULL,
    [MobileNo]       VARCHAR (25)    NOT NULL,
    [Address]        VARCHAR (MAX)   NULL,
    [Email]          VARCHAR (50)    NOT NULL,
    [PasswordHash]   VARBINARY (MAX) NOT NULL,
    [PasswordSalt]   VARBINARY (MAX) NOT NULL,
    [IsTempPassword] BIT             CONSTRAINT [DF_Users_IsTempPassword] DEFAULT ((1)) NOT NULL,
    [IsActive]       BIT             CONSTRAINT [DF_Users_IsActive] DEFAULT ((1)) NOT NULL,
    [IsDelete]       BIT             CONSTRAINT [DF_Users_IsDelete] DEFAULT ((0)) NULL,
    [CreatedOn]      DATETIME        CONSTRAINT [DF_Users_CreatedOn] DEFAULT (getutcdate()) NULL,
    [CreatedBy]      VARCHAR (50)    NULL,
    [UpdatedOn]      DATETIME        NULL,
    [UpdatedBy]      VARCHAR (50)    NULL,
    [DeletedOn]      DATETIME        NULL,
    [DeletedBy]      VARCHAR (50)    NULL,
    PRIMARY KEY CLUSTERED ([UserId] ASC)
);

