﻿using AuthLibrary;
using Core;
using Core.Models;
using CrudOperations;

namespace Infrastucture
{
    public class ManagementSystemRepository : IManagementSystemRepository
    {
        private readonly IAuthLib _authLib;
        private readonly ICrudOperationService _crudOperationService;

        public ManagementSystemRepository(ICrudOperationService crudOperationService, IAuthLib authLib)
        {
            _crudOperationService = crudOperationService;
            _authLib = authLib;
        }

        public async Task<Response> InsertUser(DtoUserInsert dtoUserInsert)
        {
            DataAccess.CreatePasswordHash(dtoUserInsert.Password, out byte[] passwordHash, out byte[] passwordSalt);
            dtoUserInsert.PasswordHash = passwordHash;
            dtoUserInsert.PasswordSalt = passwordSalt;


            return await _crudOperationService.InsertUpdateDelete<Response>("[dbo].[uspUsersInsert]", new
            {
                dtoUserInsert.FirstName, 
                dtoUserInsert.LastName,
                dtoUserInsert.Email,
                dtoUserInsert.Address,
                dtoUserInsert.MobileNo,
                passwordHash, 
                passwordSalt

            });
        }

        public async Task<ResponseList<UserDetails>> GetUserLists(DtoUsersListAll dtoUsersListAll)
        {
            return await _crudOperationService.GetPaginatedList<UserDetails>("[dbo].[uspUsersAllList]", dtoUsersListAll);
        }

        public async Task<Response<UserDetails>> GetUserById(DtoUserGetById dtoUserGetById)
        {
            return await _crudOperationService.GetSingleRecord<UserDetails>("[dbo].[uspUserGetById]", dtoUserGetById);
        }

        public async Task<Response> UpdateUser(DtoUserUpdate dtoUserUpdate)
        {
            return await _crudOperationService.InsertUpdateDelete<Response>("[dbo].[uspUsersUpdate]", dtoUserUpdate);
        }

        public async Task<Response> ChangeUserStatus(DtoUserActivedOrDeactivated DtoUserActivedOrDeactivated)
        {
            return await _crudOperationService.InsertUpdateDelete<Response>("[dbo].[uspUsersStatusChanges]", DtoUserActivedOrDeactivated);
        }

        public async Task<Response> DeleteUser(DtoUserDelete dtoUserDelete)
        {
            return await _crudOperationService.InsertUpdateDelete<Response>("[dbo].[uspUsersDelete]", dtoUserDelete);
        }

    }
}
