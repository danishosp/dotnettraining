﻿using System.ComponentModel.DataAnnotations;


namespace Core.Models
{
    public class DtoUserInsert
    {
        //[Required, StringLength(120, ErrorMessage = "{0} must be at least {2} characters long.", MinimumLength = 2)]
        //[RegularExpression(@"^[a-zA-Z\-\']+$", ErrorMessage = "First name should not contain numbers and special characters")]

        [Required, StringLength(50, ErrorMessage = "{0} must be at least {2} characters and maximum 50 characters long.", MinimumLength = 2)]
        [RegularExpression(@"^[a-zA-Z\-\']+$", ErrorMessage = "First name should not contain numbers and special characters")]
        public string? FirstName { get; set; }

        [Required, StringLength(120, ErrorMessage = "{0} must be at least {2} characters long.", MinimumLength = 2)]
        [RegularExpression(@"^[a-zA-Z\-\']+$", ErrorMessage = "Last name should not contain numbers and special characters")]
        public string? LastName { get; set; }

        [Required, DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\d{3}-\d{3}-\d{4}$", ErrorMessage = "Not a valid Mobile Number. Mobile Number must have a format as 000-000-0000.")]
        public string? MobileNo { get; set; }

        public string? Password { get; set; }

        public byte[]? PasswordHash { get; set; }

        public byte[]? PasswordSalt { get; set; }

        public string? Address { get; set; }

        [Required, DataType(DataType.EmailAddress)]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Not an valid Email Address.")]
        public string? Email { get; set; }

    }

    public class DtoUserUpdate
    {
        public Guid UserId { get; set; }

        [Required, StringLength(120, ErrorMessage = "{0} must be at least {2} characters long.", MinimumLength = 2)]
        [RegularExpression(@"^[a-zA-Z\-\' ]+$", ErrorMessage = "First name should not contain numbers and special characters")]
        public string? FirstName { get; set; }

        [Required, StringLength(120, ErrorMessage = "{0} must be at least {2} characters long.", MinimumLength = 2)]
        [RegularExpression(@"^[a-zA-Z\-\' ]+$", ErrorMessage = "Last name should not contain numbers and special characters")]
        public string? LastName { get; set; }

        [RegularExpression(@"^\d{3}-\d{3}-\d{4}$", ErrorMessage = "Not a valid Mobile Number. Mobile Number must have a format as 000-000-0000.")]
        public string? MobileNo { get; set; }

        public string? Address { get; set; }

        public string? UpdatedBy { get; set; }
    }

    public class DtoUserDelete
    {
        public Guid UserId { get; set; }

        public string? DeletedBy { get; set; }
    }

    public class DtoUserGetById
    {
        public string? UserId { get; set; }

    }

    public class DtoUsersListAll
    {
        public int Start { get; set; }
        public int PageSize { get; set; }
        public string? SortCol { get; set; }
        public string? SearchKey { get; set; }

    }

    public class DtoUserActivedOrDeactivated
    {
        public Guid UserId { get; set; }
        public bool IsActive { get; set; }
        public string? UpdatedBy { get; set; }
    }

    public class UserDetails
    {
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? MobileNo { get; set; }
        public string? Address { get; set; }
        public string? Email { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }

    }
    
}
