﻿using Core.Models;
using CrudOperations;

namespace Core
{
    public interface IManagementSystemRepository
    {
        Task<Response> InsertUser(DtoUserInsert dtoUserInsert);
        Task<ResponseList<UserDetails>> GetUserLists(DtoUsersListAll dtoUsersListAll);
        Task<Response<UserDetails>> GetUserById(DtoUserGetById dtoUserGetById);
        Task<Response> UpdateUser(DtoUserUpdate dtoUserUpdate);
        Task<Response> ChangeUserStatus(DtoUserActivedOrDeactivated DtoUserActivedOrDeactivated);
        Task<Response> DeleteUser(DtoUserDelete dtoUserDelete);
    }
}
