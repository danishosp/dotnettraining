﻿using Core;
using Core.Models;
using CrudOperations;
using Microsoft.AspNetCore.Mvc;
using StudentManagementERP.CommonException;
using StatusCodes = Microsoft.AspNetCore.Http.StatusCodes;

namespace ManagmentSystemWithDLL.Controller
{
    [Route("api/[controller]"), ServiceFilter(typeof(ModelValidatorAttribute))]
    public class UserController : ControllerBase
    {
        private readonly IManagementSystemRepository _managementSystemRepository;
        public UserController(IManagementSystemRepository managementSystemRepository)
        {
            _managementSystemRepository = managementSystemRepository;
        }

        [HttpPost]
        public async Task<IActionResult> InsertUser([FromBody]DtoUserInsert dtoUserInsert)
        {
            try
            {
                var response = await _managementSystemRepository.InsertUser(dtoUserInsert);
                return response.Status ? StatusCode(StatusCodes.Status201Created, response) : StatusCode(StatusCodes.Status400BadRequest, response);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpGet, Route("getUserLists")]
        public async Task<IActionResult> GetUserLists(DtoUsersListAll dtoUsersListAll)
        {
            var response = await _managementSystemRepository.GetUserLists(dtoUsersListAll);
            return response.Status ? StatusCode(StatusCodes.Status200OK, response) : StatusCode(StatusCodes.Status400BadRequest, response);
        }

        [HttpGet, Route("getSingleData")]
        public async Task<IActionResult> GetUserById(DtoUserGetById dtoUserGetById)
        {
            var response = await _managementSystemRepository.GetUserById(dtoUserGetById);
            return response.Status ? StatusCode(StatusCodes.Status200OK, response) : StatusCode(StatusCodes.Status400BadRequest, response);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateUser([FromBody]DtoUserUpdate dtoUserUpdate)
        {
            var response = await _managementSystemRepository.UpdateUser(dtoUserUpdate);
            return response.Status ? StatusCode(StatusCodes.Status200OK, response) : StatusCode(StatusCodes.Status400BadRequest, response);
        }

        [HttpPatch]
        public async Task<IActionResult> ChangeUserStatus([FromBody]DtoUserActivedOrDeactivated dtoUserActivedOrDeactivated)
        {
            try
            {
                var response = await _managementSystemRepository.ChangeUserStatus(dtoUserActivedOrDeactivated);
                return response.Status ? StatusCode(StatusCodes.Status200OK, response) : StatusCode(StatusCodes.Status400BadRequest, response);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteUser(DtoUserDelete dtoUserDelete)
        {
            try
            {
                var response = await _managementSystemRepository.DeleteUser(dtoUserDelete);
                return response.Status ? StatusCode(StatusCodes.Status200OK, response) : StatusCode(StatusCodes.Status400BadRequest, response);
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
