using AuthLibrary;
using Core;
using CrudOperations;
using Infrastucture;
using StudentManagementERP.CommonException;

var builder = WebApplication.CreateBuilder(args);
var connectionString = builder.Configuration.GetConnectionString("ManagementSystemDB") ?? throw new InvalidOperationException("Connection string 'ManagementSystemDB' not found");


// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddSingleton<IAuthLib>(x => new DataAccess(x.GetService<IConfiguration>(), connectionString));
builder.Services.AddScoped<ICrudOperationService>(x => new CrudOperationDataAccess(x.GetService<IConfiguration>(), connectionString));
builder.Services.AddScoped<ModelValidatorAttribute>();
builder.Services.AddScoped<IManagementSystemRepository, ManagementSystemRepository>();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
