using System.Globalization;
using AuthLibrary;
using CommonComponent.Email;
using Core.Account;
using Core.DataProtected;
using Core.User;
using CrudOperations;
using Infrastucture;
using Microsoft.AspNetCore.DataProtection;
using PMS.CommonException;

var builder = WebApplication.CreateBuilder(args);
var connectionString = builder.Configuration.GetConnectionString("PMS_DB") ?? throw new InvalidOperationException("ConnectionString 'PMS_DB' not found.");

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddScoped<IDataProtectionRepository, DataProtectionRepository>();
builder.Services.AddScoped<ICrudOperationService>(x => new CrudOperationDataAccess(x.GetService<IConfiguration>(), connectionString));
builder.Services.AddScoped<IAuthLib>(x => new DataAccess(x.GetService<IConfiguration>(), connectionString));
builder.Services.AddScoped<ModelValidatorAttribute>();
builder.Services.AddScoped<IManageStaffRepository, ManageStaffRepository>();
builder.Services.AddScoped<IEmailService>(x => new EmailService(new EmailConfig
{
   
    Username = (builder?.Configuration["AppSettings:Smtp:UserName"] ?? ""),
    Password = (builder?.Configuration["AppSettings:Smtp:Password"] ?? ""),
    EnableSSL = Convert.ToBoolean(builder?.Configuration["AppSettings:Smtp:EnableSSl"], CultureInfo.CurrentCulture),
    FromEmail = (builder?.Configuration["AppSettings:Smtp:From"] ?? ""),
    Host = (builder?.Configuration["AppSettings:Smtp:Host"] ?? ""),
    Port = Convert.ToInt32(builder?.Configuration["AppSettings:Smtp:Port"], CultureInfo.CurrentCulture)
}, connectionString));

builder.Services.AddScoped<IAccountRepository, AccountRepository>();
builder.Services.AddDataProtection();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
