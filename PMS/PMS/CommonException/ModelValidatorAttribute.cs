﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace PMS.CommonException
{
    public class ModelValidatorAttribute : ActionFilterAttribute
    {
        public override Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if(!context.ModelState.IsValid)
            {
                var message = string.Join(Environment.NewLine, context.ModelState.Values
                                                 .SelectMany(x => x.Errors)
                                                 .Select(x => x.ErrorMessage));
                context.Result = new BadRequestObjectResult(message);
            }
            return base.OnActionExecutionAsync(context, next);
        }
    }
}
