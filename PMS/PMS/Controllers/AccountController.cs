﻿using System.Globalization;
using System.Security.Principal;
using AuthLibrary;
using CommonComponent.Email;
using Core.Account;
using CrudOperations;
using Infrastucture;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PMS.CommonException;
using StatusCodes = Microsoft.AspNetCore.Http.StatusCodes;

namespace PMS.Controllers
{
    [Route("api/[controller]"), ServiceFilter(typeof(ModelValidatorAttribute))]
    public class AccountController : ControllerBase
    {
        private readonly IAccountRepository _accountRepository;
        private readonly ICrudOperationService _crudOperationService;
        private readonly IAuthLib _authLib;
        private readonly IConfiguration _configuration;
        private readonly IEmailService _emailService;
        public AccountController(IAccountRepository accountRepository, ICrudOperationService crudOperationService, IAuthLib authLib, IConfiguration configuration, IEmailService emailService)
        {
            _accountRepository = accountRepository;
            _crudOperationService = crudOperationService;
            _authLib = authLib;
            _configuration = configuration;
            _emailService = emailService;
        }

        [HttpPost]
        public async Task<IActionResult> AccountCreation([FromBody]DtoAccountCreation dtoAccountCreation)
        {
            try
            {
                var response = await _accountRepository.AccountCreation(dtoAccountCreation).ConfigureAwait(false);
                return response.Status ? StatusCode(StatusCodes.Status201Created, response) : StatusCode(StatusCodes.Status400BadRequest, response);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost, Route("Login")]
        public async Task<IActionResult> Login([FromBody] DtoLogin dtoLogin)
        {
            try
            {
                var response = await _accountRepository.Login(dtoLogin).ConfigureAwait(false);
                var userInfoRequest = response.Data;
                if(response.Status)
                {
                    if(userInfoRequest.IsTwoFactorAuthenticationRequired && !string.IsNullOrEmpty(userInfoRequest.Email))
                    {
                        string otp = await _authLib.GenerateOTP(6).ConfigureAwait(false);

                        var otpSaveResponse = await _authLib.SaveToken("Account.usp_SaveOtp", new
                        {
                            otp,
                            userInfoRequest.UserId
                        }).ConfigureAwait(false);

                        if (otpSaveResponse.Status)
                        {
                            string body;
                            using (StreamReader reader = new(Path.Combine("ContentFiles", "EmailTemplate", "LoginOtpMail.html")))
                            {
                                body = reader.ReadToEnd();
                            }

                            UserDetailsGetParam userDetailsGetParam = new()
                            {
                                Email = dtoLogin?.Email
                            };

                            Response<UserDetails> userDetailsResponse = await _accountRepository.GetUserBasicDetails(userDetailsGetParam).ConfigureAwait(false);

                            string? adminEmail = _configuration.GetValue<string>("AdminEmail");

                            body = body.Replace("{{otp}}", otp)
                                .Replace("{{username}}", userDetailsResponse.Data.FirstName)
                                .Replace("{{sendername}}", _configuration.GetValue<string>("CompanyNameMailSender"))
                                .Replace("{{adminemail}}", adminEmail);

                            string mailSubject = EmailSubjectHelper.LoginOTPSubject.Replace("{{company-name}}", _configuration.GetValue<string>("companyName"));

                            _emailService?.SendEmail(dtoLogin?.Email ?? "", mailSubject, body, true);

                            return Ok(new
                            {
                                userInfoRequest.UserId,
                                adminEmail,
                                userInfoRequest.IsTwoFactorAuthenticationRequired,
                                otpSaveResponse.Message,
                                LoginStatus = response.Message
                            });
                        }
                        else
                        {
                            return BadRequest(new
                            {
                                message = otpSaveResponse.Message
                            });
                        }
                    }
                    else
                    {
                        var token = await _authLib.GenerateJWTToken(userInfoRequest.UserId.ToString(), userInfoRequest.Email, userInfoRequest.RoleName, expiration: DateTime.UtcNow.AddDays(1)).ConfigureAwait(false);

                        await _authLib.SaveToken("[Account].[usp_TokenSave]", new
                        {
                            userInfoRequest.UserId,
                            token
                        }).ConfigureAwait(false);

                        if(!response.Status)
                        {
                            return StatusCode(StatusCodes.Status400BadRequest, "Something went worng, Please contact to administrator");
                        }

                        return Ok(new
                        {
                            message = "Successfully logged into system",
                            data = new
                            {
                                userInfoRequest.UserId,
                                userInfoRequest.Email,
                                userInfoRequest.FirstName,
                                userInfoRequest.RoleName,
                                LoginStatus = response.Message
                            },
                            token
                        });
                    }
                }
                else if (!response.Status && response.Message == "DAU" || response.Message == "DEU" || response.Message == "ACL")
                {

                    response.Message = Utility.GetDataFromXML("ErrorMessage.xml", "error", response.Message.Trim().ToLower(CultureInfo.CurrentCulture));
                    return StatusCode(StatusCodes.Status403Forbidden, response);
                }
                else if (!response.Status && response.Message == "UNF")
                {
                    response.Message = Utility.GetDataFromXML("ErrorMessage.xml", "error", response.Message.Trim().ToLower(CultureInfo.CurrentCulture));
                    return StatusCode(StatusCodes.Status404NotFound, response);
                }
                else if (!response.Status && response.Message.Trim() == "ALK")
                {
                    response.Message = Utility.GetDataFromXML("ErrorMessage.xml", "error", response.Message.Trim().ToLower(CultureInfo.CurrentCulture));
                    return StatusCode(StatusCodes.Status404NotFound, response);
                }
                else if (!response.Status && (response.Message.Trim() == "TPX"))
                {
                    response.Message = Utility.GetDataFromXML("ErrorMessage.xml", "error", response.Message.Trim().ToLower(CultureInfo.CurrentCulture));
                    return StatusCode(StatusCodes.Status404NotFound, response);
                }
                else if (!response.Status && response.Message.Trim() == "Invalid password")
                {
                    DtoInvalidAttemptChecker checker = new()
                    {
                        Email = dtoLogin?.Email
                    };

                    var responseInvalidAttempt = await _accountRepository.InvalidLoginAttemptChecker(checker).ConfigureAwait(false);

                    response.Message = Utility.GetDataFromXML("ErrorMessage.xml", "error", "invalid-credential");

                    if (responseInvalidAttempt.Message == "ACL")
                    {


                        var invalidResponseData = responseInvalidAttempt.Data;

                        string body;
                        using (StreamReader reader = new(Path.Combine("ContentFiles", "EmailTemplate", "AccountLockedMail.html")))
                        {
                            body = reader.ReadToEnd();
                        }

                        string? userFirstName = invalidResponseData.Name;
                        string? invalidAttemptCount = Convert.ToString(invalidResponseData.InvalidAttemptAllowed, CultureInfo.CurrentCulture);

                        body = body.Replace("{{FIRSTNAME}}", userFirstName)
                            .Replace("{{company-name}}", _configuration.GetValue<string>("CompanyName"))
                            .Replace("{{invalid_attempt}}}", invalidAttemptCount);

                        string mailSubject = EmailSubjectHelper.AccountLockSubject.Replace("{{company-name}}", _configuration.GetValue<string>("CompanyName"));

                        _emailService?.SendEmail(dtoLogin?.Email ?? "", mailSubject, body, true);

                    }

                    return StatusCode(StatusCodes.Status404NotFound, response);
                }
                else
                {
                    response.Message = "Something went wrong, Please contact system administrator";
                    return StatusCode(StatusCodes.Status400BadRequest, response);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        [AllowAnonymous, HttpPost, Route("otp/verify")]
        public async Task<IActionResult> VerifyOtp([FromBody]DtoVerifyOtp verifyOtp)
        {
            try
            {
                var verifyOtpResponse = await _accountRepository.VerifyOtp(verifyOtp).ConfigureAwait(false);

                return StatusCode(!verifyOtpResponse.Status ? StatusCodes.Status500InternalServerError : StatusCodes.Status200OK, new
                {
                    Response = verifyOtpResponse,
                    verifyOtpResponse.Data?.Token
                });
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        [AllowAnonymous, HttpPost, Route("otp/resend")]
        public async Task<IActionResult> ResendOtp([FromBody] DtoResendOtp resendOtp)
        {
            try
            {
                var otpSaveResponse = await _accountRepository.ResendOtp(resendOtp).ConfigureAwait(false);

                return StatusCode(!otpSaveResponse.Status ? StatusCodes.Status500InternalServerError : StatusCodes.Status200OK,
                    new
                    {
                        Response = otpSaveResponse

                    });
            }
            catch (Exception)
            {

                throw;
            }
            

        }

        [HttpGet, Route("logout")]
        public async Task<IActionResult> Logout()
        {
            try
            {
                var response = await _authLib.Logout(HttpContext.Request.Headers["Authorization"].FirstOrDefault(), "[Account].[usp_Logout]", new
                {
                    UserId = User?.Identity?.Name,
                }).ConfigureAwait(false);

                if (!response.Status)
                {
                    return BadRequest(response);
                }

                return Ok(response);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost, Route("password/forgot")]
        public async Task<IActionResult> ForgotPassword([FromBody] DtoForgotPasswordRequest forgot)
        {
            try
            {
                var forgotPasswordResponse = await _accountRepository.ForgotPasswordVerify(forgot).ConfigureAwait(false);

                return StatusCode(
                    (!forgotPasswordResponse.Status ? StatusCodes.Status500InternalServerError : StatusCodes.Status200OK),
                    new
                    {
                        Response = forgotPasswordResponse
                    });
            }
            catch (Exception)
            {

                throw;
            }

        }

        [HttpPost, Route("password/verifytoken")]
        public async Task<IActionResult> ForgotPasswordVerifyToken([FromBody] DtoPasswordVerifyTokenParam passwordVerifyTokenParam)
        {
            try
            {
                var forgotPasswordResponse = await _accountRepository.VerifyPasswordToken(passwordVerifyTokenParam).ConfigureAwait(false);

                return StatusCode(!forgotPasswordResponse.Status ? StatusCodes.Status409Conflict : StatusCodes.Status200OK,
                    new
                    {
                        Response = forgotPasswordResponse
                    });
            }
            catch (Exception)
            {

                throw;
            }

        }

        [HttpPost, Route("password/reset")]
        public async Task<IActionResult> ResetPasswordAsync([FromBody] DtoResetPasswordParam resetPasswordParam)
        {
            try
            {
                resetPasswordParam = resetPasswordParam ?? new();
                resetPasswordParam.IsEncrypted = true;
                resetPasswordParam.ScreenName = FlagHelper.ResetPasswordScreen;

                var resetPasswordResponse = await _accountRepository.ResetPassword(resetPasswordParam).ConfigureAwait(false);

                return StatusCode(!resetPasswordResponse.Status ? StatusCodes.Status409Conflict : StatusCodes.Status200OK,
                    new
                    {
                        Response = resetPasswordResponse
                    });
            }
            catch (Exception)
            {

                throw;
            }
           
        }
    }
}
