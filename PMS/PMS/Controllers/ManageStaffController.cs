﻿using Core.User;
using Microsoft.AspNetCore.Mvc;
using PMS.CommonException;
using StatusCodes = Microsoft.AspNetCore.Http.StatusCodes;

namespace PMS.Controllers
{
    [Route("api/[controller]"), ServiceFilter(typeof(ModelValidatorAttribute))]
    public class ManageStaffController : ControllerBase
    {
        private readonly IManageStaffRepository _manageStaffRepository;
        public ManageStaffController (IManageStaffRepository manageStaffRepository)
        {
            _manageStaffRepository = manageStaffRepository;
        }

        [HttpPost]
        public async Task<IActionResult> UserInsert([FromBody] DtoUserInsert dtoUserInsert)
        {
            try
            {
                var response = await _manageStaffRepository.UserInsert(dtoUserInsert);
                return response.Status ? StatusCode(StatusCodes.Status201Created, response) : StatusCode(StatusCodes.Status400BadRequest, response);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPut]
        public async Task<IActionResult> UserUpdate([FromBody] DtoUserUpdate dtoUserUpdate)
        {
            try
            {
                var response = await _manageStaffRepository.UserUpdate(dtoUserUpdate);
                return response.Status ? StatusCode(StatusCodes.Status201Created, response) : StatusCode(StatusCodes.Status400BadRequest, response);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpDelete]
        public async Task<IActionResult> UserDelete([FromBody] DtoUserDelete dtoUserDelete)
        {
            try
            {
                var response = await _manageStaffRepository.UserDelete(dtoUserDelete);
                return response.Status ? StatusCode(StatusCodes.Status201Created, response) : StatusCode(StatusCodes.Status400BadRequest, response);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPatch]
        public async Task<IActionResult> UserChangeStatus([FromBody] DtoUserChangeStatus dtoUserChangeStatus)
        {
            try
            {
                var response = await _manageStaffRepository.UserChangeStatus(dtoUserChangeStatus);
                return response.Status ? StatusCode(StatusCodes.Status201Created, response) : StatusCode(StatusCodes.Status400BadRequest, response);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpGet, Route("GetSingleData")]
        public async Task<IActionResult> GetUserById(DtoGetUserById dtoGetUserById)
        {
            try
            {
                var response = await _manageStaffRepository.GetUserById(dtoGetUserById);
                return response.Status ? StatusCode(StatusCodes.Status200OK, response) : StatusCode(StatusCodes.Status400BadRequest, response);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpGet]
        public async Task<IActionResult> UserLists(DtoUserLists dtoUserLists)
        {
            try
            {
                var response = await _manageStaffRepository.UserLists(dtoUserLists);
                return response.Status ? StatusCode(StatusCodes.Status200OK, response) : StatusCode(StatusCodes.Status400BadRequest, response);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
