﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Infrastucture
{
    public class Utility
    {
        static string? environment;
        private static Random random = new Random();

        public Utility()
        {
            environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "";
        }

        public static string ToTitleCase(string value)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(value);
        }

        public static string? GetDataFromXML(string fileName, string xmlHeaderTag, string key)
        {

            string cmlErrorMessageFilePath = Path.Combine("ContentFiles", "TextHelper", fileName);

            XDocument xdoc = XDocument.Load(cmlErrorMessageFilePath);

            string? value = (from node in xdoc?.Descendants(xmlHeaderTag)
                             select (string?)node?.Element(key))?
                            .First();

            string? message = string.IsNullOrWhiteSpace(value) ? MessageHelper.NoMessage : value;

            return message;

        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
