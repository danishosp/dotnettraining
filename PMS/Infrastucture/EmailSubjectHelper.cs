﻿
namespace Infrastucture
{
    public class EmailSubjectHelper
    {
        public static string LoginOTPSubject = "New User Login OTP | {{company-name}}";
        public static string UpdatePasswordOTPSubject = "Verify User OTP | {{company-name}}";
        public static string ResetPasswordSubject = "Reset Password request | {{company-name}}";
        public static string AccountLockSubject = "User Account Lock | {{company-name}}";
        public static string UserCreatedSubject = "New User Temporary Password | {{company-name}}";
    }
}
