﻿using System.Text;
using AuthLibrary;
using CommonComponent.Email;
using Core.Account;
using Core.DataProtected;
using CrudOperations;
using Microsoft.Extensions.Configuration;
using BCryptNet = BCrypt.Net.BCrypt;

namespace Infrastucture
{
    public class AccountRepository : IAccountRepository
    {
        private readonly ICrudOperationService _crudOperationService;
        private readonly IAuthLib _authLib;
        private readonly IConfiguration _configuration;
        private readonly IEmailService _emailService;
        private readonly IDataProtectionRepository _dataProtectionService;

        public AccountRepository(ICrudOperationService crudOperationService, IAuthLib authLib, IConfiguration configuration, IEmailService emailService, IDataProtectionRepository dataProtectionService)
        {
            _crudOperationService = crudOperationService;
            _authLib = authLib;
            _configuration = configuration;
            _emailService = emailService;
            _dataProtectionService = dataProtectionService;
        }

        public async Task<Response> AccountCreation(DtoAccountCreation accountCreation)
        {
            DataAccess.CreatePasswordHash(accountCreation.Password, out byte[] passwordHash, out byte[] Passwordsalt);
            accountCreation.PasswordHash = passwordHash;
            accountCreation.PasswordSalt = Passwordsalt;

            accountCreation.FirstName = Utility.ToTitleCase(accountCreation.FirstName ?? "");
            accountCreation.LastName = Utility.ToTitleCase(accountCreation.LastName ?? "");
            return await _authLib.SignUp<Response>("[Account].[usp_AccountCreation]", accountCreation);
        }

        public async Task<AuthResponse<UserDetails>> Login(DtoLogin dtoLogin)
        {
            var response = await _authLib.ValidateLoginCredentials<UserDetails>("[Account].[UspUserLoginGet]", new
            {
                dtoLogin.Email
            },dtoLogin.Password);

            return response;
        }

        public async Task<Response<UserDetails>> GetUserBasicDetails(UserDetailsGetParam userDetailsGetParam)
        {
            var response = await _crudOperationService.GetSingleRecord<UserDetails>("[Account].[usp_GetUserBasicDetails]", new
            {
                userDetailsGetParam.UserId,
                userDetailsGetParam.Email
            });
            return response;
        }

        public async Task<Response<DtoInvalidAttemptResponse>> InvalidLoginAttemptChecker(DtoInvalidAttemptChecker attemptChecker)
        {
            var response = await _crudOperationService.GetSingleRecord<DtoInvalidAttemptResponse>("[Account].[UspInvalidLoginAttemptChecker]", attemptChecker);
            
            return response;
        }

        public async Task<Response<UserDetails>> VerifyOtp(DtoVerifyOtp verifyOtp)
        {
            string? resultMessage;
            Response<UserDetails> userDetailsResponse = new();
            
            AuthResponse verifyOtpResponse = await _authLib.VerifyOneTimePassword("[Account].[usp_VerifyOtp]", new
            {
                verifyOtp.UserId,
                verifyOtp.Otp
            }).ConfigureAwait(false);

            if(verifyOtpResponse.Status)
            {
                UserDetailsGetParam userDetailsGetParam = new UserDetailsGetParam()
                {
                    UserId = verifyOtp?.UserId
                };
                userDetailsResponse = await GetUserBasicDetails(userDetailsGetParam).ConfigureAwait(false);

                if(userDetailsResponse.Status)
                {
                    UserDetails user = userDetailsResponse.Data;

                    var token = await _authLib.GenerateJWTToken(user.UserId.ToString(), user.Email,"Admin", expiration: DateTime.UtcNow.AddDays(1)).ConfigureAwait(false);

                    await _authLib.SaveToken("[Account].[usp_TokenSave]", new
                    {
                        verifyOtp?.UserId,
                        Token = token,
                        verifyOtp?.DeviceToken,
                        verifyOtp?.DevicePlatform,
                        verifyOtp?.DeviceId,
                    }).ConfigureAwait(false);

                    if(!string.IsNullOrWhiteSpace(verifyOtp?.DeviceToken))
                    {
                        var response = await _crudOperationService.InsertUpdateDelete<Response>("[Account].[usp_TokenSave]", new
                        {
                            user.UserId,
                            Token = verifyOtp?.DeviceToken,
                            verifyOtp?.DeviceId,
                            CreatedBy = "System"
                        }).ConfigureAwait(false);
                    }
                    userDetailsResponse.Data.Token = token;
                    resultMessage = Utility.GetDataFromXML("SuccessMessage.xml", "success", "user-success-login");
                    userDetailsResponse.Message = resultMessage;
                }
                else
                {
                    resultMessage = Utility.GetDataFromXML("ErrorMessage.xml", "error", userDetailsResponse.Message);
                    userDetailsResponse.Message = resultMessage;
                }
            }
            else
            {
                string? errorMessage = Utility.GetDataFromXML("ErrorMessage.xml", "error", verifyOtpResponse.Message);
                userDetailsResponse.Status = false;
                userDetailsResponse.Message = errorMessage;

            }

            return userDetailsResponse;
        }

        public async Task<Response<DtoOtpDetails>> SaveOtpAsync(DtoSaveOtpParam saveOtpParam)
        {
            Response<DtoOtpDetails> otpDetailsResponse = new();
            string? resultMessage;

            var otpSaveResponse = await _authLib.SaveToken("[Account].[usp_SaveOtp]", new
            {
                saveOtpParam.Otp,
                saveOtpParam.UserId
            }).ConfigureAwait(false);

            if(otpSaveResponse.Status)
            {
                UserDetailsGetParam userDetailsGetParam = new UserDetailsGetParam()
                {
                    UserId = saveOtpParam.UserId,
                };

                Response<UserDetails> userDetailsResponse = await GetUserBasicDetails(userDetailsGetParam);

                if(userDetailsResponse.Status)
                {
                    UserDetails user = userDetailsResponse.Data;

                    string body;
                    using(StreamReader reader = new StreamReader(Path.Combine("ContentFiles", "EmailTemplate", "LoginOtpMail.html")))
                    {
                        body = reader.ReadToEnd();
                    }

                    //string? companyName = _configuration.GetValue<string>("CompanyNameMailSender");
                    string? adminEmail = _configuration.GetValue<string>("AdminEmail");
                    string? mailSenderName = _configuration.GetValue<string>("CompanyNameMailSender");

                    body = body.Replace("{{username}}", user.FirstName)
                               .Replace("{{otp}}", saveOtpParam.Otp)
                               .Replace("{{sendername}}", mailSenderName)
                               .Replace("{{adminemail}}", adminEmail);

                    string subject = EmailSubjectHelper.LoginOTPSubject;

                    _emailService?.SendEmail(userDetailsResponse.Data.Email ?? "", subject, body, true);

                    DtoOtpDetails otpDetails = new DtoOtpDetails()
                    {
                        AdminEmail = adminEmail,
                        UserId = user.UserId,
                        IsTwoFactorAuthenticationRequired = user.IsTwoFactorAuthenticationRequired
                    };

                    resultMessage = Utility.GetDataFromXML("SuccessMessage.xml", "success", "otp-sent-success");

                    otpDetailsResponse.Status = true;
                    otpDetailsResponse.Data = otpDetails;
                    otpDetailsResponse.Message = resultMessage;
                }
                else
                {
                    resultMessage = Utility.GetDataFromXML("ErrorMessage.xml", "error", userDetailsResponse.Message);
                    otpDetailsResponse.Status = false;
                    otpDetailsResponse.Message = resultMessage;
                }
            }
            else
            {
                resultMessage = Utility.GetDataFromXML("ErrorMessage.xml", "error", "login-otp-not-generated");
                otpDetailsResponse.Message = resultMessage;
            }

            return otpDetailsResponse;
        }

        public async Task<Response<DtoOtpDetails>> ResendOtp(DtoResendOtp resendOtp)
        {

            string otp = await _authLib.GenerateOTP(6).ConfigureAwait(false);


            DtoSaveOtpParam SaveOtpParam = new()
            {
                Otp = otp,
                UserId = resendOtp.UserId
            };

            Response<DtoOtpDetails> otpDetailsResponse = await SaveOtpAsync(SaveOtpParam).ConfigureAwait(false);

            if (otpDetailsResponse.Status)
            {
                UserDetailsGetParam userDetailsGetParam = new UserDetailsGetParam()
                {
                    UserId = resendOtp.UserId
                };

                Response<UserDetails> userDetailsResponse = await GetUserBasicDetails(userDetailsGetParam).ConfigureAwait(false);

                string? adminEmail = _configuration.GetValue<string>("AdminEmail");

                string body;
                using (StreamReader reader = new(Path.Combine("ContentFiles", "EmailTemplate", "LoginOtpMail.html")))
                {
                    body = reader.ReadToEnd();
                }

                body = body.Replace("{{otp}}", otp)
                    .Replace("{{username}}", userDetailsResponse.Data.FirstName)
                    .Replace("{{sendername}}", _configuration.GetValue<string>("CompanyName"))
                    .Replace("{{adminemail}}", adminEmail);

                string mailSubject = EmailSubjectHelper.LoginOTPSubject.Replace("{{company-name}}", _configuration.GetValue<string>("CompanyName"));

                _emailService?.SendEmail(userDetailsResponse.Data.Email ?? "", mailSubject, body, true);
            }

            return otpDetailsResponse;

        }

        public async Task<Response> CheckValidEmail(DtoForgotPasswordRequest? forgotPasswordRequest)
        {
            var validEmailResponse = await _crudOperationService.InsertUpdateDelete<Response>("[Account].[uspCheckValidEmail]", new { forgotPasswordRequest?.Email }).ConfigureAwait(false);

            return validEmailResponse;
        }

        public async Task<Response> SavePasswordVerifyToken(DtoPasswordVerifyTokenParam passwordVerifyTokenParam)
        {


            var tokenSaveResponse = await _crudOperationService.InsertUpdateDelete<Response>("[Account].[usp_SavePasswordVerifyToken]", new
            {
                passwordVerifyTokenParam.EmailId,
                passwordVerifyTokenParam.Token

            }).ConfigureAwait(false);

            return tokenSaveResponse;


        }

        public async Task<Response<DtoForgotPassword>> ForgotPasswordVerify(DtoForgotPasswordRequest forgotPasswordRequest)
        {
            var validEmailResponse = await CheckValidEmail(forgotPasswordRequest).ConfigureAwait(false);
            string userFirstName = validEmailResponse.Data;

            string? resultMessage = "";

            Response<DtoForgotPassword> objResponseForgotPassword = new();
            DtoForgotPassword objForgotPassword = new();

            if (validEmailResponse.Status)
            {
                string emailToken = Utility.RandomString(10);

                DtoPasswordVerifyTokenParam passwordVerifyTokenParam = new()
                {
                    EmailId = forgotPasswordRequest?.Email,
                    Token = emailToken,
                };
                var PasswordValidatorResponse = await SavePasswordVerifyToken(passwordVerifyTokenParam).ConfigureAwait(false);

                UserDetailsGetParam userDetailsGetParam = new UserDetailsGetParam()
                {
                    Email = forgotPasswordRequest?.Email
                };

                Response<UserDetails> userDetailsResponse = await GetUserBasicDetails(userDetailsGetParam);

                string? link = "";
                if (userDetailsResponse.Status)
                {

                    UserDetails user = userDetailsResponse.Data;

                    link = _configuration?.GetValue<string>("VerifyPasswordUrl")?
                   .Replace("{{EMAIL}}", _dataProtectionService.Encrypt(forgotPasswordRequest!.Email!), StringComparison.InvariantCulture).
                   Replace("{{TOKEN}}", _dataProtectionService.Encrypt(emailToken), StringComparison.InvariantCulture);

                    string? mailSenderName = _configuration?.GetValue<string>("CompanyNameMailSender")!;

                    string body;
                    using (StreamReader reader = new StreamReader(Path.Combine("ContentFiles", "EmailTemplate", "ForgotPasswordVerifyMail.html")))
                    {
                        body = reader.ReadToEnd();
                    }


                    body = body.Replace("{{username}}", user.FirstName)
                        .Replace("{{LINK}}", link)
                        .Replace("{{sendername}}", mailSenderName);

                    string mailSubject = EmailSubjectHelper.ResetPasswordSubject.Replace("{{company-name}}", _configuration.GetValue<string>("CompanyName"));

                    var emailResult = _emailService?.SendEmail(forgotPasswordRequest?.Email ?? "", mailSubject, body, true);

                }

                resultMessage = Utility.GetDataFromXML("SuccessMessage.xml", "success", "password-reset-mail-success");
                objResponseForgotPassword.Message = resultMessage;
                objResponseForgotPassword.Status = true;

                objForgotPassword.link = link;
                objForgotPassword.Email = _dataProtectionService.Encrypt(forgotPasswordRequest?.Email ?? "");
                objForgotPassword.EmailToken = _dataProtectionService.Encrypt(emailToken);

                objResponseForgotPassword.Data = objForgotPassword;

            }
            else
            {

                objResponseForgotPassword.Status = false;
                objResponseForgotPassword.Message = validEmailResponse.Message;
            }

            return objResponseForgotPassword;

        }

        public async Task<Response<DtoUserVerifyDetails>> VerifyPasswordToken(DtoPasswordVerifyTokenParam passwordVerifyTokenParam)
        {
            DtoUserVerifyDetails objUserVerifyDetails = new DtoUserVerifyDetails();

            passwordVerifyTokenParam.EmailId = _dataProtectionService.Decrypt(passwordVerifyTokenParam.EmailId ?? "");
            passwordVerifyTokenParam.Token = !string.IsNullOrEmpty(passwordVerifyTokenParam?.Token) ? _dataProtectionService.Decrypt(passwordVerifyTokenParam.Token) : "";


            Response<DtoUserVerifyDetails> verifyTokenResponse = await _crudOperationService.InsertUpdateDelete<Response<DtoUserVerifyDetails>>("[Account].[usp_VerifyPasswordToken]", new
            {
                passwordVerifyTokenParam?.EmailId,
                passwordVerifyTokenParam?.Token
            }).ConfigureAwait(false);

            if (verifyTokenResponse.Status)
            {
                UserDetailsGetParam userDetailsGetParam = new UserDetailsGetParam()
                {
                    Email = passwordVerifyTokenParam?.EmailId
                };

                Response<UserDetails> userDetailsResponse = await GetUserBasicDetails(userDetailsGetParam).ConfigureAwait(false);

                var userDetails = userDetailsResponse.Data;
                objUserVerifyDetails.UserId = userDetails.UserId;
                objUserVerifyDetails.EmailId = userDetails.Email;


                string otp = await _authLib.GenerateOTP(6).ConfigureAwait(false);

                DtoSaveOtpParam SaveOtpParam = new()
                {
                    Otp = otp,
                    UserId = userDetails.UserId
                };
                Response<DtoOtpDetails> otpDetailsResponse = await SaveOtpAsync(SaveOtpParam).ConfigureAwait(false);

                string? adminEmail = _configuration.GetValue<string>("AdminEmail");

                string body;
                using (StreamReader reader = new(Path.Combine("ContentFiles", "EmailTemplate", "LoginOtpMail.html")))
                {
                    body = reader.ReadToEnd();
                }

                body = body.Replace("{{otp}}", otp)
                    .Replace("{{username}}", userDetailsResponse.Data.FirstName)
                    .Replace("{{sendername}}", _configuration.GetValue<string>("CompanyName"))
                    .Replace("{{adminemail}}", adminEmail);

                string mailSubject = EmailSubjectHelper.UpdatePasswordOTPSubject.Replace("{{company-name}}", _configuration.GetValue<string>("CompanyName"));

                _emailService?.SendEmail(userDetailsResponse.Data.Email ?? "", mailSubject, body, true);


                if (otpDetailsResponse.Status)
                {
                    verifyTokenResponse.Message = "OTP sent to your registered email id";
                    verifyTokenResponse.Data = objUserVerifyDetails;
                }


            }

            return verifyTokenResponse;

        }

        public bool VerifyOldPasswordHash(string password, List<DtoUserOldPasswords> userOldPasswords)
        {
            for (int counter = 0; counter < userOldPasswords.Count; counter++)
            {
                byte[]? storedSalt = userOldPasswords[counter].PasswordSalt;
                byte[]? storedHash = userOldPasswords[counter].PasswordHash;

                var computedHash = BCryptNet.HashPassword(password, Encoding.ASCII.GetString(storedSalt!));

                var storedHashSting = Encoding.ASCII.GetString(storedHash!);

                if (computedHash.Equals(storedHashSting))
                {
                    return true;
                }

            }
            return false;
        }

        public async Task<Response> ValidateNewPassWithOldPass(string password, string? userId, string? email)
        {
            ResponseList<DtoUserOldPasswords> response;

            response = await _crudOperationService.GetList<DtoUserOldPasswords>("[Account].[usp_GetOldPasswordDetails]", new
            {
                userId,
                email

            });

            if (response.Status)
            {

                List<DtoUserOldPasswords> userOldPasswordList = response.Data;

                if (VerifyOldPasswordHash(password, userOldPasswordList))
                {
                    return new Response
                    {
                        Status = true,
                        Message = "passwordavailableinlist",
                        Data = response.Message
                    };
                }
                else
                {
                    return new Response
                    {
                        Status = false,
                        Message = "passwordnotavailableinlist",
                        Data = response.Message
                    };
                }
            }
            else
            {
                return new Response
                {
                    Status = false,
                    Message = "Password does not exists.",
                    Data = response.Message
                };
            }
        }

        public async Task<Response> ResetPassword(DtoResetPasswordParam resetPasswordParam)
        {
            Response? response = new();

            if (resetPasswordParam != null)
            {
                string email = resetPasswordParam!.IsEncrypted! ? _dataProtectionService.Decrypt(resetPasswordParam.Email ?? "") : resetPasswordParam.Email!;
                string emailToken = !string.IsNullOrEmpty(resetPasswordParam?.EmailToken) ? (resetPasswordParam.IsEncrypted ? _dataProtectionService.Decrypt(resetPasswordParam.EmailToken) : resetPasswordParam.EmailToken) : "";

                resetPasswordParam!.Email = email ?? "";
                resetPasswordParam!.EmailToken = emailToken;

                resetPasswordParam!.Opr = _dataProtectionService.Encrypt(resetPasswordParam.Password!);

                DataAccess.CreatePasswordHash(resetPasswordParam.Password, out byte[] passwordHash, out byte[] passwordSalt);
                Response ValidateOldPasswordResponse = await ValidateNewPassWithOldPass(resetPasswordParam.Password!, null, resetPasswordParam.Email).ConfigureAwait(false);

                if (ValidateOldPasswordResponse.Status)
                {
                    string resultMessage = Utility.GetDataFromXML("ErrorMessage.xml", "error", "password-already-exists")!;
                    response.Status = false;
                    response.Message = resultMessage.Replace("{prev-password-limit}", ValidateOldPasswordResponse.Data);
                    response.Data = "duplicate";
                    return response;

                }
                else
                {
                    resetPasswordParam.PasswordHash = passwordHash;
                    resetPasswordParam.PasswordSalt = passwordSalt;


                    response = await _crudOperationService.InsertUpdateDelete<Response>("[Account].[usp_UserResetPassword]", new
                    {
                        resetPasswordParam.Email,
                        resetPasswordParam.PasswordHash,
                        resetPasswordParam.PasswordSalt,
                        resetPasswordParam.UpdatedBy,
                        resetPasswordParam.Opr
                    }).ConfigureAwait(false);


                    UserDetailsGetParam userDetailsGetParam = new() { Email = resetPasswordParam.Email };

                    Response<UserDetails> userDetailsResponse = await GetUserBasicDetails(userDetailsGetParam);

                    if (userDetailsResponse.Status)
                    {
                        UserDetails user = userDetailsResponse.Data;
                        string body = "", Subject = "";

                        string? adminEmail = _configuration.GetValue<string>("AdminEmail");

                        string? mailSenderName = _configuration?.GetValue<string>("CompanyNameMailSender");


                        if (resetPasswordParam.ScreenName == FlagHelper.ResetPasswordScreen)
                        {
                            using (StreamReader reader = new StreamReader(Path.Combine("ContentFiles", "EmailTemplate", "ResetPasswordUpdated.html")))
                            {
                                body = reader.ReadToEnd();
                            }

                            body = body
                            .Replace("{{username}}", user.FirstName)
                            .Replace("{{useraccountname}}", user.Email)
                            .Replace("{{sendername}}", mailSenderName)
                            .Replace("{{adminemail}}", adminEmail);


                            Subject = EmailSubjectHelper.LoginOTPSubject;


                        }
                        else if (resetPasswordParam.ScreenName == FlagHelper.ChangePasswordScreen)
                        {
                            using (StreamReader reader = new StreamReader(Path.Combine("ContentFiles", "EmailTemplate", "PasswordChange.html")))
                            {
                                body = reader.ReadToEnd();
                            }

                            body = body
                            .Replace("{{username}}", user.FirstName)
                            .Replace("{{sendername}}", mailSenderName)
                            .Replace("{{adminemail}}", adminEmail);


                            Subject = EmailSubjectHelper.LoginOTPSubject;
                        }
                        _emailService?.SendEmail(userDetailsResponse?.Data.Email ?? "", Subject, body, true);

                    }

                }

            }

            return response;
        }

    }
}
