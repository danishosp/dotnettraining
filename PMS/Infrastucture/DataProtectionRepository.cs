﻿using Core.DataProtected;
using Microsoft.AspNetCore.DataProtection;

namespace Infrastucture
{
    public class DataProtectionRepository : IDataProtectionRepository
    {
        private readonly IDataProtectionProvider _dataProtectionProvider;
        private const string Key = "bxl7NMlvyp7xORE45DqqvHXaB2sgI5lH";

        public DataProtectionRepository(IDataProtectionProvider dataProtectionProvider)
        {
            _dataProtectionProvider = dataProtectionProvider;
        }

        public string Encrypt(string input)
        {
            var protector = _dataProtectionProvider.CreateProtector(Key);
            return protector.Protect(input);
        }

        public string Decrypt(string cipherText)
        {
            var protector = _dataProtectionProvider.CreateProtector(Key);
            return protector.Unprotect(cipherText);
        }
    }
}
