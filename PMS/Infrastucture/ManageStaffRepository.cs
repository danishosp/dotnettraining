﻿using Core.User;
using CrudOperations;

namespace Infrastucture
{
    public class ManageStaffRepository : IManageStaffRepository
    {
        private readonly ICrudOperationService _curdOperationService;
        public ManageStaffRepository(ICrudOperationService crudOperationService)
        {
            _curdOperationService = crudOperationService;
        }

        public async Task<Response<DtoUserDetails>> GetUserById(DtoGetUserById dtoGetUserById)
        {
            return await _curdOperationService.GetSingleRecord<DtoUserDetails>("[dbo].[uspUserGetById]", dtoGetUserById);
        }

        public async Task<Response> UserChangeStatus(DtoUserChangeStatus dtoUserChangeStatus)
        {
            return await _curdOperationService.InsertUpdateDelete<Response>("[dbo].[uspUserChangeStatus]", dtoUserChangeStatus);
        }

        public async Task<Response> UserDelete(DtoUserDelete dtoUserDelete)
        {
            return await _curdOperationService.InsertUpdateDelete<Response>("[dbo].[usp_UserDelete]", dtoUserDelete);
        }

        public async Task<Response> UserInsert(DtoUserInsert dtoUserInsert)
        {
            return await _curdOperationService.InsertUpdateDelete<Response>("[dbo].[usp_UserInsert]", dtoUserInsert);
        }

        public async Task<ResponseList<DtoUserDetails>> UserLists(DtoUserLists dtoUserLists)
        {
            return await _curdOperationService.GetPaginatedList<DtoUserDetails>("[dbo].[uspUsersLists]", dtoUserLists);
        }

        public async Task<Response> UserUpdate(DtoUserUpdate dtoUserUpdate)
        {
            return await _curdOperationService.InsertUpdateDelete<Response>("[dbo].[usp_UserUpdate]", dtoUserUpdate);
        }
    }
}
