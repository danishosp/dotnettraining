﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastucture
{
    public static class MessageHelper
    {
        public static string NoMessage = "Message is not assigned";
    }

    public static class FlagHelper
    {
        public static string ResetPasswordScreen = "reset-password";
        public static string ChangePasswordScreen = "change-password";

    }
}
