﻿using System.ComponentModel.DataAnnotations;
using AuthLibrary;

namespace Core.Account
{
    public class DtoAccountCreation
    {
        [Required, StringLength(50, ErrorMessage = "{0} must be at least {2} characters and maximum 50 characters long.", MinimumLength = 2)]
        [RegularExpression(@"^[a-zA-Z\-\']+$", ErrorMessage = "First name should not contain numbers and special characters")]
        public string? FirstName { get; set; }

        [Required, StringLength(50, ErrorMessage = "{0} must be at least {2} characters and maximum 50 characters long.", MinimumLength = 2)]
        [RegularExpression(@"^[a-zA-Z\-\']+$", ErrorMessage = "Last name should not contain numbers and special characters")]
        public string? LastName { get; set; }

        [Required, EmailAddress]
        public string? Email { get; set; }

        [Ignore, Required, DataType(DataType.Password)]
        [RegularExpression(@"^(?!\S*\s)(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{6,15}$",
        ErrorMessage = "Password must be 6 to 15 Characters long without spaces and must contain at least one upper case character, one lowercase character, one number and one special character.")]
        public string? Password { get; set; }
        public byte[]? PasswordHash { get; set; }
        public byte[]? PasswordSalt { get; set; }

        [Required, StringLength(10, ErrorMessage = "The Mobile value cannot exceed 10 characters. ")]
        public string? Mobile { get; set; }
        public Guid RoleId { get; set; } 
        public Guid CreatedBy { get; set; }
    }

    public class DtoLogin
    {   
        [Required, RegularExpression(@"\A(?i)(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?-i)[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z",
         ErrorMessage = "The Email field is not a valid e-mail address.")]
        public string? Email { get; set;}

        [Required, DataType(DataType.Password)]
        public string? Password { get; set; }
    }

    
    public class UserDetails
    {

        public Guid UserId { get; set;}

        [Required, EmailAddress]
        public string? Email {get; set;}

        public string? FirstName {get; set;}


        [Required, DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\d{3}-\d{3}-\d{4}$", ErrorMessage = "Not a valid phone number. Phone Number must have a format as 000-000-0000.")]
        public string? Phone { get; set;}

        [DataType(DataType.Date)]
        [Required, DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public string? DateOfBirth { get; set; }

        [Required, StringLength(12, ErrorMessage = "{0} must be at least {2} characters long.", MinimumLength = 1)]
        public string? Gender { get; set; }

        public byte[]? PasswordHash { get; set; }
        public byte[]? PasswordSalt { get; set; }
        public string? RoleId { get; set; }
        public string? RoleName { get; set; }
        public string? Address { get; set;}
        public int IsActive { get; set;}
       
        public bool IsTwoFactorAuthenticationRequired { get; set;}

        public bool IsFirstLogin { get; set;}

        public string? Token { get; set; }
    }

    public class UserDetailsGetParam
    {
        public Guid? UserId { get; set; }
        public string? Email { get; set; }
        public bool IsUserActive { get; set; }
    }

    public class DtoInvalidAttemptChecker
    {
        [Required, EmailAddress, StringLength(150, ErrorMessage = "email should be less than or equal to 150 characters.")]
        public string? Email { get; set;}
    }

    public class DtoVerifyOtp
    {
        [Required(AllowEmptyStrings = false), StringLength(6)]
        [RegularExpression("^[0-9]{6}$", ErrorMessage = "Otp should be numerical only")]
        public string? Otp { get; set; }

        [Required]
        public Guid UserId { get; set; }
        public string? DeviceToken { get; set; }
        public string? DevicePlatform { get; set; }
        public string? DeviceId { get; set; }
    }

    public class DtoInvalidAttemptResponse
    {
        public string? Name { get; set; }
        public int? InvalidAttemptAllowed { get; set; }
    }

    public class DtoResendOtp
    {
        public Guid? UserId { get; set;}
        public string? AdminEmail { get; set; }
        public bool? IsTwoFactorAuthenticationRequired { get; set; }

    }

    public class DtoOtpDetails
    {
        public Guid UserId { get; set; }
        public string? AdminEmail { get; set; }
        public bool IsTwoFactorAuthenticationRequired { get; set; }

    }

    public class DtoSaveOtpParam
    {
        public string? Otp { get; set; }
        public Guid? UserId { get; set; }
    }

    public class DtoForgotPasswordRequest
    {
        [Required]
        public string? Email { get; set; }
    }

    public class DtoPasswordVerifyTokenParam
    {
        public string? EmailId { get; set; }
        public string? Token { get; set; }

    }

    public class DtoForgotPassword
    {
        public string? link { get; set; }
        public string? Email { get; set; }
        public string? EmailToken { get; set; }

    }

    public class DtoUserVerifyDetails
    {
        public Guid? UserId { get; set; }
        public string? EmailId { get; set; }

    }

    public class DtoResetPasswordParam
    {
        public string? Email { get; set; }
        public string? EmailToken { get; set; }
        public Guid? UserId { get; set; }
        public string? Password { get; set; }
        public byte[]? PasswordHash { get; set; }
        public byte[]? PasswordSalt { get; set; }
        public Guid? UpdatedBy { get; set; }
        public string? Opr { get; set; }
        public bool IsEncrypted { get; set; }
        public string? ScreenName { get; set; }

    }

    public class DtoUserOldPasswords
    {
        public byte[]? PasswordHash { get; set; }
        public byte[]? PasswordSalt { get; set; }

    }

}
