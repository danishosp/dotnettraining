﻿using AuthLibrary;
using Core.User;
using CrudOperations;

namespace Core.Account
{
    public interface IAccountRepository
    {
        Task<Response> AccountCreation(DtoAccountCreation accountCreation);
        Task<AuthResponse<UserDetails>> Login(DtoLogin dtoLogin);
        Task<Response<UserDetails>> GetUserBasicDetails(UserDetailsGetParam userDetailsGetParam);
        Task<Response<DtoInvalidAttemptResponse>> InvalidLoginAttemptChecker(DtoInvalidAttemptChecker attemptChecker);
        Task<Response<UserDetails>> VerifyOtp(DtoVerifyOtp verifyOtp);
        Task<Response<DtoOtpDetails>> ResendOtp(DtoResendOtp resendOtp);
        Task<Response> CheckValidEmail(DtoForgotPasswordRequest? forgotPasswordRequest);
        Task<Response> SavePasswordVerifyToken(DtoPasswordVerifyTokenParam passwordVerifyTokenParam);
        Task<Response<DtoForgotPassword>> ForgotPasswordVerify(DtoForgotPasswordRequest forgotPasswordRequest);
        Task<Response<DtoUserVerifyDetails>> VerifyPasswordToken(DtoPasswordVerifyTokenParam passwordVerifyTokenParam);
        Task<Response> ResetPassword(DtoResetPasswordParam resetPasswordParam);
    }
}
