﻿
namespace Core.DataProtected
{
    public interface IDataProtectionRepository
    {
        string Encrypt(string input);

        string Decrypt(string cipherText);
    }
}
