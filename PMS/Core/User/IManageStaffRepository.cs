﻿using CrudOperations;

namespace Core.User
{
    public interface IManageStaffRepository
    {
        Task<Response> UserInsert(DtoUserInsert dtoUserInsert);
        Task<Response> UserUpdate(DtoUserUpdate dtoUserUpdate);
        Task<Response> UserDelete(DtoUserDelete dtoUserDelete);
        Task<Response> UserChangeStatus(DtoUserChangeStatus dtoUserChangeStatus);
        Task<Response<DtoUserDetails>> GetUserById(DtoGetUserById dtoGetUserById);
        Task<ResponseList<DtoUserDetails>> UserLists(DtoUserLists dtoUserLists);
    }
}
