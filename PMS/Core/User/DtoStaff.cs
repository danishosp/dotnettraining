﻿using System.ComponentModel.DataAnnotations;


namespace Core.User
{
    public class DtoUserInsert
    {
        [Required, StringLength(50, ErrorMessage = "{0} must be at least {2} characters and maximum 50 characters long.", MinimumLength = 2)]
        [RegularExpression(@"^[a-zA-Z\-\']+$", ErrorMessage = "First name should not contain numbers and special characters")]
        public string? FirstName { get; set; }

        [Required, StringLength(50, ErrorMessage = "{0} must be at least {2} characters and maximum 50 characters long.", MinimumLength = 2)]
        [RegularExpression(@"^[a-zA-Z\-\']+$", ErrorMessage = "Middle name should not contain numbers and special characters")]
        public string? MiddleName { get; set; }

        [Required, StringLength(50, ErrorMessage = "{0} must be at least {2} characters and maximum 50 characters long.", MinimumLength = 2)]
        [RegularExpression(@"^[a-zA-Z\-\']+$", ErrorMessage = "Last name should not contain numbers and special characters")]
        public string? LastName { get; set; }

        [Required, DataType(DataType.EmailAddress)]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Not an valid Email Address.")]
        public string? Email { get; set; }

        [Required, DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\d{3}-\d{3}-\d{4}$", ErrorMessage = "Not a valid Mobile Number. Mobile Number must have a format as 000-000-0000.")]
        public string? Phone { get; set; }
        public string? Address { get; set; }
        public Guid CreatedBy { get; set; }
    }

    public class DtoUserUpdate
    {
        public Guid UserId { get; set; }

        [Required, StringLength(50, ErrorMessage = "{0} must be at least {2} characters and maximum 50 characters long.", MinimumLength = 2)]
        [RegularExpression(@"^[a-zA-Z\-\']+$", ErrorMessage = "First name should not contain numbers and special characters")]
        public string? FirstName { get; set; }

        [Required, StringLength(50, ErrorMessage = "{0} must be at least {2} characters and maximum 50 characters long.", MinimumLength = 2)]
        [RegularExpression(@"^[a-zA-Z\-\']+$", ErrorMessage = "Middle name should not contain numbers and special characters")]
        public string? MiddleName { get; set; }

        [Required, StringLength(50, ErrorMessage = "{0} must be at least {2} characters and maximum 50 characters long.", MinimumLength = 2)]
        [RegularExpression(@"^[a-zA-Z\-\']+$", ErrorMessage = "Last name should not contain numbers and special characters")]
        public string? LastName { get; set; }
        public string? Address { get; set; }
        public Guid UpdatedBy { get; set; }

    }

    public class DtoUserDelete
    {
        public string? Email { get; set; }
        public Guid DeletedBy { get; set; }
    }

    public class DtoUserChangeStatus
    {
        public Guid UserId { get; set; }
        public bool IsActive { get; set; }
        public Guid UpdatedBy { get; set; }

    }

    public class DtoGetUserById
    {
        public Guid UserId { get; set; }
    }

    public class DtoUserLists
    {
        public int Start { get; set; }
        public int PageSize { get; set; }
        public string? SortCol { get; set; }
        public string? SearchKey { get; set; }
    }

    public class DtoUserDetails
    {
        public string? Name { get; set; }
        public string? Email { get; set; }
        public string? Phone { get; set; }
    }
}
