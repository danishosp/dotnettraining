﻿ALTER ROLE [db_owner] ADD MEMBER [danish];


GO
ALTER ROLE [db_accessadmin] ADD MEMBER [danish];


GO
ALTER ROLE [db_securityadmin] ADD MEMBER [danish];


GO
ALTER ROLE [db_ddladmin] ADD MEMBER [danish];


GO
ALTER ROLE [db_backupoperator] ADD MEMBER [danish];


GO
ALTER ROLE [db_datareader] ADD MEMBER [danish];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [danish];


GO
ALTER ROLE [db_denydatareader] ADD MEMBER [danish];


GO
ALTER ROLE [db_denydatawriter] ADD MEMBER [danish];

