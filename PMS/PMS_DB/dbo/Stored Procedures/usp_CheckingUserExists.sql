﻿CREATE PROC usp_CheckingUserExists
(
	@Email VARCHAR(50)
)
AS
BEGIN
	IF NOT EXISTS(SELECT 1 FROM tblUsers WITH (NOLOCK) WHERE Email = Trim(@Email) AND IsDeleted = 0)
		BEGIN
			SELECT 0 [Status], 'User does not Exist' [Message]
			RETURN;
		END

	IF EXISTS(SELECT 1 FROM tblUsers WITH (NOLOCK) WHERE Email = Trim(@Email) AND IsActive = 0)
		BEGIN
			SELECT 0 [Status],'User is Deactivated' [Message]
			RETURN;
		END
END
