﻿CREATE PROC usp_UserUpdate
(
	@UserId		UNIQUEIDENTIFIER,
	@FirstName	VARCHAR(50),
	@MiddleName	VARCHAR(50),
	@Lastname	VARCHAR(50),
	@Address	VARCHAR(250),
	@UpdatedBy	UNIQUEIDENTIFIER
)
AS
BEGIN
	BEGIN TRY

		SET NOCOUNT ON

		IF EXISTS(SELECT 1 FROM tblUsers WITH (NOLOCK) WHERE UserId = @UserId AND IsDeleted = 0 AND IsActive = 1)
		BEGIN
			UPDATE tblUsers
			SET FirstName	= TRIM(@FirstName),
				MiddleName	= TRIM(@MiddleName),
				Lastname	= TRIM(@Lastname),
				[Address]	= TRIM(@Address),
				UpdatedOn	= GETUTCDATE(),
				UpdatedBy	= @UpdatedBy
			WHERE UserId = @UserId

			SELECT 1 [Status], 'Updated Successfully' [Message]
		END
		ELSE
		BEGIN
			SELECT 0 [Status], 'UserId does not exists please check and try again.' [Message]
		END

	END TRY

	BEGIN CATCH
		DECLARE @Message VARCHAR(MAX) =Error_message()
        DECLARE @ErrorSeverity INT=Error_severity()
        DECLARE @ErrorState INT=Error_state()
        RAISERROR(@Message, @ErrorSeverity, @ErrorState)
        SELECT 0 [Status], @Message  [Message]
	END CATCH

END
