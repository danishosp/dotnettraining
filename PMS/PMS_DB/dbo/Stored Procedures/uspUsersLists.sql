﻿CREATE PROC [dbo].[uspUsersLists] 
(
	@Start     INT = 0,
    @PageSize  INT = - 1,
    @SortCol   VARCHAR(100) = NULL,
    @SearchKey VARCHAR(100) = ''
)
AS
BEGIN
	 BEGIN TRY
        SET NOCOUNT ON;

        SET @SortCol = TRIM(ISNULL(@SortCol, ''));
        SET @SearchKey = TRIM(ISNULL(@SearchKey, ''));

        IF ISNULL(@Start, 0) = 0 SET @Start = 0
        IF ISNULL(@PageSize, 0) <= 0 SET @PageSize = - 1

		SELECT 1 AS [Status], 'Success' AS [Message]

		SELECT FirstName +' '+ MiddleName +' '+ LastName AS [Name],
			   Email,
			   Phone
		FROM tblUsers
		WHERE IsDeleted = 0 
		AND
   		(      
       		FirstName LIKE CONCAT('%',@SearchKey,'%') OR
			MiddleName LIKE CONCAT('%',@SearchKey,'%') OR
       		LastName LIKE CONCAT('%',@SearchKey,'%') OR
       		Email LIKE CONCAT('%', @SearchKey, '%') OR
			Phone LIKE CONCAT('%',@SearchKey,'%') OR
       		FirstName + ' ' + MiddleName +' '+ LastName  LIKE CONCAT('%',@SearchKey,'%')
   		)
		ORDER BY                    
   		CASE WHEN @SortCol = 'firstName_asc' THEN FirstName END ASC,      
   		CASE WHEN @SortCol = 'firstName_desc' THEN FirstName END DESC,
		CASE WHEN @SortCol = 'middleName_asc' THEN LastName END ASC,      
   		CASE WHEN @SortCol = 'middleName_desc' THEN LastName END DESC,
   		CASE WHEN @SortCol = 'lastName_asc' THEN LastName END ASC,      
   		CASE WHEN @SortCol = 'lastName_desc' THEN LastName END DESC,
		CASE WHEN @SortCol = 'email_asc' THEN Email END ASC,
   		CASE WHEN @SortCol = 'email_desc' THEN Email END DESC,
   		CASE WHEN @SortCol = 'phone_asc' THEN Phone END ASC,
   		CASE WHEN @SortCol = 'phone_desc' THEN Phone END DESC,
   		CASE WHEN @SortCol NOT IN ('firstName_asc','firstName_desc','middleName_asc', 'middleName_desc', 'lastName_asc','lastName_desc','phone_asc', 'phone_desc','email_asc','email_desc') THEN CreatedOn END DESC
		
		OFFSET @Start ROWS                      
   		FETCH NEXT (CASE WHEN @PageSize = -1 THEN (SELECT COUNT(1) FROM [dbo].[tblUsers]) ELSE @PageSize END) ROWS ONLY

		DECLARE @recordsFiltered INT = 
   		(
       		SELECT COUNT(*)
       		FROM [dbo].[tblUsers] WITH(NOLOCK)
       		WHERE IsDeleted = 0 
       		AND
       		(      
           		FirstName LIKE CONCAT('%',@SearchKey,'%') OR
           		LastName LIKE CONCAT('%',@SearchKey,'%') OR
           		Email LIKE CONCAT('%', @SearchKey, '%') OR
				Phone LIKE CONCAT('%',@SearchKey,'%') OR
           		FirstName + ' ' + LastName  LIKE CONCAT('%',@SearchKey,'%')
       		)
   		)
   		SELECT @recordsFiltered 'recordsFiltered'
		
		DECLARE @totalRecords INT = (SELECT COUNT(UserId) FROM tblUsers WITH(NOLOCK) WHERE IsDeleted = 0)

		SELECT @totalRecords 'Total Records'
	END TRY

    BEGIN CATCH

        DECLARE @Msg VARCHAR(MAX) =Error_message()
        DECLARE @ErrorSeverity INT=Error_severity()
        DECLARE @ErrorState INT=Error_state()
        RAISERROR(@Msg, @ErrorSeverity, @ErrorState)
        SELECT 0 AS [Status], @Msg AS [Message]

    END CATCH

END