﻿CREATE PROC usp_UserInsert
(
	@FirstName	VARCHAR(50),
	@MiddleName	VARCHAR(50),
	@Lastname	VARCHAR(50),
	@Email		VARCHAR(50),
	@Phone		VARCHAR(15),
	@Address	VARCHAR(250),
	@CreatedBy	UNIQUEIDENTIFIER = NULL
)
AS
BEGIN
	BEGIN TRY

		SET NOCOUNT ON

		IF NOT EXISTS(SELECT 1 FROM tblUsers WITH(NOLOCK) WHERE Email = TRIM(@Email) AND IsDeleted = 0)
		BEGIN
			DECLARE @Role int = (SELECT RoleId FROM tblRole WITH(NOLOCK) WHERE RoleName = 'Staff')
			INSERT INTO tblUsers 
			(
				FirstName, 
				MiddleName, 
				LastName, 
				Email, 
				Phone, 
				RoleId,
				CreatedBy,
				CreatedOn
			)		  
			VALUES 
			(
				TRIM(@FirstName), 
				TRIM(@MiddleName), 
				TRIM(@Lastname), 
				TRIM(@Email), 
				TRIM(@Phone),
				@Role,
				@CreatedBy,
				GETUTCDATE()
			)

			SELECT 1 [Status], 'User Inserted Successfully' [Message]
		END
		ELSE
		BEGIN
			SELECT 0 [Status], 'User Already Exist' [Message]
		END

	END TRY

	BEGIN CATCH
        DECLARE @Message VARCHAR(MAX) =Error_message()
        DECLARE @ErrorSeverity INT=Error_severity()
        DECLARE @ErrorState INT=Error_state()
        RAISERROR(@Message, @ErrorSeverity, @ErrorState)
        SELECT 0 [Status], @Message  [Message]
    END CATCH

END
