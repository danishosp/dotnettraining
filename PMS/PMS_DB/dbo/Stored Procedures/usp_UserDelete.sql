﻿CREATE PROC [dbo].[usp_UserDelete]
(
	@Email		varchar(50),
	@DeletedBy UNIQUEIDENTIFIER = null
)
AS
BEGIN
	BEGIN TRY
		SET NOCOUNT ON

		BEGIN
			EXEC usp_CheckingUserExists @Email = @Email
		END

		IF EXISTS(SELECT 1 FROM tblUsers WITH (NOLOCK) WHERE Email = @Email AND IsDeleted = 0 AND IsActive = 1)
		BEGIN
			UPDATE tblUsers SET DeletedBy = @DeletedBy, DeletedOn = GETUTCDATE(), IsDeleted = 1 WHERE Email = @Email

			SELECT 1 [Status], 'Deleted Successfully' [Message]
			RETURN
		END

	END TRY
	BEGIN CATCH
		DECLARE @Message VARCHAR(MAX) =Error_message()
        DECLARE @ErrorSeverity INT=Error_severity()
        DECLARE @ErrorState INT=Error_state()
        RAISERROR(@Message, @ErrorSeverity, @ErrorState)
        SELECT 0 [Status], @Message  [Message]
	END CATCH

END