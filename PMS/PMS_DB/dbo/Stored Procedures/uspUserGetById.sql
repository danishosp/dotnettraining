﻿CREATE PROC [dbo].[uspUserGetById]
(
	@UserId UNIQUEIDENTIFIER
)
AS
BEGIN
	BEGIN TRY
		
		SET NOCOUNT ON;

		IF EXISTS(SELECT 1 FROM [dbo].[tblUsers] WITH(NOLOCK) WHERE UserId = @UserId AND IsDeleted =0)
		BEGIN
			SELECT 1  [Status], 'UserId Data is been successfully fetched.' [Message]

			SELECT FirstName +' '+ MiddleName +' '+ LastName AS [Name], Email, Phone
			FROM tblUsers
			WHERE UserId = @UserId
		END
		ELSE
		BEGIN
			SELECT 0 [Status],'Given UserId value does not exists please check and try again.' [Message]
		END
	END TRY

	BEGIN CATCH
		DECLARE @Msg VARCHAR(MAX) =Error_message()
        DECLARE @ErrorSeverity INT=Error_severity()
        DECLARE @ErrorState INT=Error_state()
        RAISERROR(@Msg, @ErrorSeverity, @ErrorState)
        SELECT 0 [Status], @Msg [Message]
	END CATCH

END