﻿CREATE PROC usp_GetErrorInfo
as
begin
	    DECLARE @Message VARCHAR(MAX) =Error_message()
        DECLARE @ErrorSeverity INT=Error_severity()
        DECLARE @ErrorState INT=Error_state()
        RAISERROR(@Message, @ErrorSeverity, @ErrorState)
        SELECT 0 [Status], @Message  [Message]
end