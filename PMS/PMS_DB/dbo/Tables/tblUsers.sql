﻿CREATE TABLE [dbo].[tblUsers] (
    [UserId]     UNIQUEIDENTIFIER CONSTRAINT [DF_tblUsers_UserId] DEFAULT (newid()) NOT NULL,
    [Email]      VARCHAR (200)    NOT NULL,
    [FirstName]  VARCHAR (150)    NULL,
    [MiddleName] VARCHAR (150)    NULL,
    [LastName]   VARCHAR (150)    NULL,
    [Phone]      VARCHAR (50)     NULL,
    [DOB]        DATE             NULL,
    [Gender]     VARCHAR (50)     NULL,
    [RoleId]     INT              NOT NULL,
    [Address]    VARCHAR (250)    NULL,
    [IsActive]   BIT              CONSTRAINT [DF_tblUsers_Active] DEFAULT ((1)) NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_tblUsers_Deleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tblUsers_CreatedOn] DEFAULT (getutcdate()) NULL,
    [CreatedBy]  UNIQUEIDENTIFIER NULL,
    [UpdatedOn]  DATETIME         NULL,
    [UpdatedBy]  UNIQUEIDENTIFIER NULL,
    [DeletedOn]  DATETIME         NULL,
    [DeletedBy]  UNIQUEIDENTIFIER NULL,
    FOREIGN KEY ([RoleId]) REFERENCES [dbo].[tblRole] ([RoleId])
);

