﻿CREATE TABLE [dbo].[tblRole] (
    [RoleId]    INT          IDENTITY (1, 1) NOT NULL,
    [RoleName]  VARCHAR (50) NOT NULL,
    [CreatedOn] DATETIME     DEFAULT (getutcdate()) NULL,
    PRIMARY KEY CLUSTERED ([RoleId] ASC)
);

