﻿CREATE TABLE [dbo].[tblRoleMaster] (
    [RoleId]    UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [RoleName]  VARCHAR (50)     NOT NULL,
    [CreatedOn] DATETIME         DEFAULT (getutcdate()) NULL
);

