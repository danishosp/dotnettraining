﻿CREATE PROCEDURE [Account].[UspUserLoginGet] 
    @Email VARCHAR(150)

 

AS
BEGIN
    
    SET NOCOUNT ON;
    BEGIN TRY                    
        IF @Email IS NULL                      
            BEGIN                      
                SELECT 0 'Status','Email Required' 'Message'                      
            END                      
        ELSE                       
            BEGIN 
                IF EXISTS(SELECT 1 FROM [Account].[UserAccount] WITH(NOLOCK) WHERE LoginId = TRIM(@Email) AND IsActive = 1)                    
                    BEGIN
                        DECLARE @StatusMessage VARCHAR(20) = 'Success'
                        IF EXISTS (SELECT 1 from [Account].UserAccount Where LoginId = @Email 
                                    AND IsActive = 1
                                    AND IsPasswordReset = 1 
                                    AND (ResetPasswordExpiryDate IS NOT NULL 
                                    AND ResetPasswordExpiryDate < GETUTCDATE()))
                            BEGIN
                                SET @StatusMessage = 'RPX';
                            END

                        SELECT 1 [Status], @StatusMessage AS [Message]

 

                        SELECT  U.[UserId],                    
                                [Email],                    
                                U.[PasswordHash],                    
                                U.[PasswordSalt],                    
                                UI.FirstName,
								UI.LastName,
                                UI.Mobile,
                                R.RoleName,
                                IsTwoFactorAuthenticationRequired,
                                U.IsActive 'Active'
                        FROM [Account].[UserAccount] U WITH(NOLOCK)
                        LEFT JOIN [Account].[UserInfo] UI WITH(NOLOCK) ON U.UserId = UI.UserId
                        LEFT JOIN [dbo].[tblRoleMaster] R WITH(NOLOCK) ON U.[RoleId] = R.[RoleId] 
                        WHERE U.LoginId = @Email AND U.IsActive = 1

 

                    END
                ELSE
                    BEGIN
                        IF NOT EXISTS (SELECT 1 FROM [Account].[UserAccount] WITH(NOLOCK) WHERE LoginId = TRIM(@Email))                    
                            BEGIN
                                SELECT 0 'Status','UNF' 'Message'
                            END
                        ELSE IF EXISTS (SELECT 1 FROM [Account].[UserAccount] WITH(NOLOCK) WHERE (LoginId = TRIM(@Email)) AND IsActive = 0 )
                            BEGIN
                                SELECT 0 'Status','DAU' 'Message'  
                            END

                        ELSE IF EXISTS (SELECT 1 FROM [Account].[UserAccount] WITH(NOLOCK) WHERE (LoginId = TRIM(@Email)) AND IsActive = 2 )                    
                            BEGIN    
                                SELECT 0 'Status','DEU' 'Message'
                            END

 

                        ELSE IF EXISTS (SELECT 1 FROM [Account].[UserAccount] WITH(NOLOCK) WHERE (LoginId = TRIM(@Email)) AND IsActive = 3 )                    
                            BEGIN    
                                SELECT 0 'Status','ALK' 'Message'
                            END
                    END
            END
    END TRY
    BEGIN CATCH                                     
        DECLARE @Message INT = Error_message();     
        DECLARE @ErrorSeverity INT = Error_severity();                           
        DECLARE @ErrorState INT = Error_state();
        RAISERROR(@Message,@ErrorSeverity,@ErrorState);
        SELECT 0 'Status', Error_message() [Message]                    
    END CATCH
END