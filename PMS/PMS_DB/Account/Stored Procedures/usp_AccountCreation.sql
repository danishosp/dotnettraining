﻿CREATE PROC [Account].[usp_AccountCreation]
(
	@FirstName    VARCHAR(50),     
	@LastName     VARCHAR(50),     
	@Email        VARCHAR(50),    
	@PasswordHash VARBINARY(MAX) = null,
	@PasswordSalt VARBINARY(MAX) = null,   
	@Mobile       VARCHAR(15),
	@RoleId		  UNIQUEIDENTIFIER,
	@CreatedBy	  UNIQUEIDENTIFIER
)
AS
BEGIN
	BEGIN TRY
		SET NOCOUNT ON

		DECLARE @UserId UNIQUEIDENTIFIER = NEWID()
		IF NOT EXISTS(SELECT 1 FROM Account.UserAccount WITH (NOLOCK) WHERE LoginId = @Email AND IsDelete = 0)
		BEGIN
			INSERT INTO Account.UserInfo
			(
				UserId,
				FirstName,
				LastName,
				Email,
				Mobile
			)
			VALUES
			(
				@UserId,
				@FirstName,
				@LastName,
				@Email,
				@Mobile
			)
			
			INSERT INTO Account.UserAccount
			(
				UserId,
				LoginId,
				PasswordHash,
				PasswordSalt,
				RoleId,
				CreatedBy
			)
			VALUES
			(
				@UserId,
				@Email,
				@PasswordHash,
				@PasswordSalt,
				@RoleId,
				@CreatedBy
			)

			SELECT 1 [Status], 'Inserted Successfully' [Message]
			
		END

		ELSE
		BEGIN
			SELECT 0 [Status], 'User Already Exist' [Message]
		END

	END TRY

	BEGIN CATCH
		DECLARE @Msg VARCHAR(MAX) =Error_message()
        DECLARE @ErrorSeverity INT=Error_severity()
        DECLARE @ErrorState INT=Error_state()
        RAISERROR(@Msg, @ErrorSeverity, @ErrorState)
        SELECT 0 [Status], @Msg [Message]
	END CATCH

END