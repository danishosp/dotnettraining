﻿CREATE TABLE [Account].[UserInfo] (
    [UserInfoId]  UNIQUEIDENTIFIER CONSTRAINT [DF_UserInfo_UserInfoId] DEFAULT (newid()) NOT NULL,
    [FirstName]   VARCHAR (50)     NULL,
    [MiddleName]  VARCHAR (50)     NULL,
    [LastName]    VARCHAR (50)     NULL,
    [Email]       VARCHAR (50)     NULL,
    [Mobile]      VARCHAR (15)     NULL,
    [Gender]      VARCHAR (10)     NULL,
    [DateOfBirth] DATE             NULL,
    [Address]     NVARCHAR (500)   NULL,
    [UserId]      UNIQUEIDENTIFIER NOT NULL
);

