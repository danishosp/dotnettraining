﻿using System.Data;
using System.Data.SqlClient;
using Core;
using Core.Models;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace Infrastructure.Repository
{
    public class RecordRepository : IRecordRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;

        public RecordRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("Demo");
        }

        public async Task InsertData(Records records)
        {
            using IDbConnection dbConnection = new SqlConnection(_connectionString);

            await dbConnection.ExecuteAsync("[uspToInsertData]", new
            {
                records.Id
            }, commandType: CommandType.StoredProcedure);
        }
    }
}