﻿using Core;
using Core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace OOPS_Problems.Controllers
{
    [Route("api/[controller]")]
    public class DemoController : ControllerBase
    {
        private readonly IRecordRepository _recordRepository;

        public DemoController(IRecordRepository recordRepository)
        {
            _recordRepository = recordRepository;
        }

        [HttpPost, Route("insertData")]

        public async Task<IActionResult> Insert(Records records)
        {
            await _recordRepository.InsertData(records);
            return Ok("Data has been added.");
        } 
    }
}
