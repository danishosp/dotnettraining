﻿namespace OOPS_Problems.Models.Inheritance.BookingConcertTickets
{
    public interface IBookMyShow
    {
        int[] Gather(int k, int maxRow);

        bool Scatter(int k, int maxRow);
    }
}
