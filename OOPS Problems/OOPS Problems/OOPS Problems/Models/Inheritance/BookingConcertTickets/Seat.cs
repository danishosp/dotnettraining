﻿namespace OOPS_Problems.Models.Inheritance.BookingConcertTickets
{
    public class Seat
    {
        public int Row { get; set; }
        public int Number { get; set; }
        public bool IsReserved { get; set; }
    }
}
