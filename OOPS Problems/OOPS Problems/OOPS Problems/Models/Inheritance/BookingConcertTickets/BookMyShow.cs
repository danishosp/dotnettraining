﻿namespace OOPS_Problems.Models.Inheritance.BookingConcertTickets
{
    public class BookMyShow : IBookMyShow
    {
        private readonly int _rows;
        private readonly int _seatsPerRow;
        private readonly List<List<Seat>> _seats;

        public BookMyShow(int rows, int seatsPerRow)
        {
            _rows = rows;
            _seatsPerRow = seatsPerRow;
            _seats = new List<List<Seat>>();

            for (int i = 0; i < _rows; i++)
            {
                var rowSeats = new List<Seat>();
                for (int j = 0; j < _seatsPerRow; j++)
                {
                    rowSeats.Add(new Seat { Row = i, Number = j });
                }
                _seats.Add(rowSeats);
            }
        }

        public int[] Gather(int k, int maxRow)
        {
            for (int i = 0; i <= maxRow; i++)
            {
                var rowSeats = _seats[i];
                for (int j = 0; j <= _seatsPerRow - k; j++)
                {
                    bool found = true;
                    for (int l = j; l < j + k; l++)
                    {
                        if (rowSeats[l].IsReserved)
                        {
                            found = false;
                            break;
                        }
                    }

                    if (found)
                    {
                        for (int l = j; l < j + k; l++)
                        {
                            rowSeats[l].IsReserved = true;
                        }

                        return new int[] { i, j };
                    }
                }
            }

            return Array.Empty<int>();
        }

        public bool Scatter(int k, int maxRow)
        {
            int seatsNeeded = k;
            for (int i = 0; i <= maxRow; i++)
            {
                var rowSeats = _seats[i];
                for (int j = 0; j < _seatsPerRow; j++)
                {
                    if (rowSeats[j].IsReserved == false)
                    {
                        rowSeats[j].IsReserved = true;
                        seatsNeeded--;
                    }

                    if (seatsNeeded == 0)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
