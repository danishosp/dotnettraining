﻿namespace OOPS_Problems.Models.Inheritance.BookingConcertTickets
{
    public class BookMyShowModel
    {
        public int KthSeat { get; set; }

        public int MaxRow { get; set; }
    }
}
