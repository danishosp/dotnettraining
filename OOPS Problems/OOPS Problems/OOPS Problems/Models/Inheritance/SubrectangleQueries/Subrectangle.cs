﻿namespace OOPS_Problems.Models.Inheritance.SubrectangleQueries
{
    public class Subrectangle : Rectangle
    {
        public int Row1 { get; set; }
        public int Column1 { get; set; }
        public int Row2 { get; set; }
        public int Column2 { get; set; }
        public int NewValue { get; set; }

        public Subrectangle(int[,] matrix, int row1, int column1, int row2, int column2, int newValue) : base(matrix)
        {
            this.Row1 = row1;
            this.Column1 = column1;
            this.Row2 = row2;
            this.Column2 = column2;
            this.NewValue = newValue;
        }
    }
}
