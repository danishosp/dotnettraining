﻿namespace OOPS_Problems.Models.Inheritance.SubrectangleQueries
{
    public class Rectangle
    {
        public int[,] Matrix { get; set; }

        public Rectangle(int[,] matrix)
        {
            this.Matrix = matrix;
        }
    }
}
