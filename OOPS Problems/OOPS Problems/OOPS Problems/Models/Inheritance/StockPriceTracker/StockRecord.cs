﻿namespace OOPS_Problems.Models.Inheritance.StockPriceTracker
{
    public class StockRecord
    {
        public int Timestamp { get; set; }
        public int Price { get; set; }
    }
}
