﻿namespace OOPS_Problems.Models.Inheritance.StockPriceTracker
{
    public class StockPrice : List<StockRecord>
    {

        public void Update(int timestamp, int price)
        {
            // Check if there is already a record with the same timestamp
            var existingRecord = this.FirstOrDefault(r => r.Timestamp == timestamp);

            if (existingRecord != null)
            {
                // If there is, update its price
                existingRecord.Price = price;
            }
            else
            {
                // Otherwise, add a new record
                this.Add(new StockRecord { Timestamp = timestamp, Price = price });
            }
        }

        public int Current()
        {
            // Find the latest record and return its price
            var latestRecord = this.OrderByDescending(r => r.Timestamp).FirstOrDefault();
            return latestRecord != null ? latestRecord.Price : 0;
        }

        public int Maximum()
        {
            // Find the highest price among all records
            return this.Max(r => r.Price);
        }

        public int Minimum()
        {
            // Find the lowest price among all records
            return this.Min(r => r.Price);
        }
    }
}
