﻿namespace OOPS_Problems.Models.Inheritance.TweetCountsPerFrequency
{
    public class TweetCountsByMinute : TweetCountsBase
    {
        public override void RecordTweet(string tweetName, int time)
        {
            base.RecordTweet(tweetName, time);
        }

        public List<int> GetTweetCounts(string tweetName, int startTime, int endTime)
        {
            return base.GetTweetCounts(tweetName, startTime, endTime, 60);
        }
    }
}
