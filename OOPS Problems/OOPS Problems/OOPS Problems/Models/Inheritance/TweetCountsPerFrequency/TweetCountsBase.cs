﻿namespace OOPS_Problems.Models.Inheritance.TweetCountsPerFrequency
{
    public class TweetCountsBase
    {
        protected Dictionary<string, List<Tweet>> _tweetCounts;

        public TweetCountsBase()
        {
            _tweetCounts = new Dictionary<string, List<Tweet>>();
        }

        public virtual void RecordTweet(string tweetName, int time)
        {
            if (!_tweetCounts.ContainsKey(tweetName))
            {
                _tweetCounts[tweetName] = new List<Tweet>();
            }

            _tweetCounts[tweetName].Add(new Tweet { Name = tweetName, Time = time });
        }

        public virtual List<int> GetTweetCounts(string tweetName, int startTime, int endTime, int chunkSize)
        {
            List<int> counts = new();

            for (int i = startTime; i <= endTime; i += chunkSize)
            {
                int chunkEnd = Math.Min(i + chunkSize - 1, endTime);

                int count = _tweetCounts.ContainsKey(tweetName) ? _tweetCounts[tweetName].Count(t => t.Time >= i && t.Time <= chunkEnd) : 0;

                counts.Add(count);
            }

            return counts;
        }
    }
}
