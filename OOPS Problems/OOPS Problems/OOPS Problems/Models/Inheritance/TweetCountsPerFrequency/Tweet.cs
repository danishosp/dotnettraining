﻿namespace OOPS_Problems.Models.Inheritance.TweetCountsPerFrequency
{
    public class Tweet
    {
        public string? Name { get; set; }
        public int Time { get; set; }
    }

    public class TweetCount
    {
        public string? TweetName { get; set; } 
        public int StartTime { get; set; }
        public int EndTime { get; set; } 
        public int ChunkSize { get; set; }
    }
}
