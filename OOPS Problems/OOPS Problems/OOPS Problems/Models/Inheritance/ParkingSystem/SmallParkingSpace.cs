﻿namespace OOPS_Problems.Models.Inheritance.HierarchicalInheritance
{
    public class SmallParkingSpace : ParkingSpace 
    {
        public SmallParkingSpace(int capacity) : base(capacity) { }
    }
}
