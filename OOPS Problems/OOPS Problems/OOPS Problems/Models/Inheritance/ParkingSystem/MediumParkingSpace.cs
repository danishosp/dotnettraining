﻿namespace OOPS_Problems.Models.Inheritance.HierarchicalInheritance
{
    public class MediumParkingSpace : ParkingSpace
    {
        public MediumParkingSpace(int capacity) : base(capacity) { }
    }
}
