﻿namespace OOPS_Problems.Models.Inheritance.HierarchicalInheritance
{
    public abstract class ParkingSpace
    {
        protected int _capacity;
        protected int _numOccupied;

        public ParkingSpace(int capacity)
        {
            _capacity = capacity;
            _numOccupied = 0;
        }

        public bool AddCar()
        {
            if (_numOccupied < _capacity)
            {
                _numOccupied++;
                return true;
            }
            return false;
        }
    }
}
