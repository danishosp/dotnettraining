﻿namespace OOPS_Problems.Models.Inheritance.HierarchicalInheritance
{
    public class BigParkingSpace : ParkingSpace 
    {
        public BigParkingSpace(int capacity) : base(capacity) { }
    }
}
