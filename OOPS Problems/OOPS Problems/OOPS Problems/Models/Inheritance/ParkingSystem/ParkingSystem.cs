﻿namespace OOPS_Problems.Models.Inheritance.HierarchicalInheritance
{
    public class ParkingSystem
    {
        private BigParkingSpace _bigSpaces;
        private MediumParkingSpace _mediumSpaces;
        private SmallParkingSpace _smallSpaces;

        public ParkingSystem(int big, int medium, int small)
        {
            _bigSpaces = new BigParkingSpace(big);
            _mediumSpaces = new MediumParkingSpace(medium);
            _smallSpaces = new SmallParkingSpace(small);
        }

        public bool AddCar(int carType)
        {
            switch (carType)
            {
                case 1:
                    return _bigSpaces.AddCar();
                case 2:
                    return _mediumSpaces.AddCar();
                case 3:
                    return _smallSpaces.AddCar();
                default:
                    return false;
            }
        }
    }
}
