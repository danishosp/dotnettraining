﻿namespace OOPS_Problems.Models.RegularCLass.MyStack
{
    public class MyStack
    {
        private Queue<int> _queue1;
        private Queue<int> _queue2;
        private int topElement;

        public MyStack()
        {
            _queue1 = new Queue<int>();
            _queue2 = new Queue<int>();
            topElement = 0;
        }

        public void Push(int x)
        {
            _queue1.Enqueue(x);
            topElement = x;
        }

        public int Pop()
        {
            while (_queue1.Count > 1)
            {
                topElement = _queue1.Dequeue();
                _queue2.Enqueue(topElement);
            }

            int popElement = _queue1.Dequeue();
            (_queue2, _queue1) = (_queue1, _queue2);
            return popElement;
        }

        public int Top()
        {
            return topElement;
        }

        public bool Empty()
        {
            return _queue1.Count == 0;
        }
    }
}
