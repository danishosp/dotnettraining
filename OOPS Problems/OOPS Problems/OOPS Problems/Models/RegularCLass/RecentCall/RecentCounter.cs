﻿namespace OOPS_Problems.Models.RegularCLass.RecentCall
{
    public class RecentCounter
    {
        private readonly Queue<int> _requests;

        public RecentCounter()
        {
            _requests = new Queue<int>();
        }

        public int Ping(int total)
        {
            _requests.Enqueue(total);
            while (_requests.Peek() < total - 3000)
            {
                _requests.Dequeue();
            }
            return _requests.Count;
        }
    }
}
