﻿namespace OOPS_Problems.Models.RegularCLass.LibrarySystem
{
    public class BookModels
    {
        public int Id { get; set; }
        public string? Title { get; set; }
        public string? Author { get; set; }
        public int YearPublished { get; set; }
    }
}
