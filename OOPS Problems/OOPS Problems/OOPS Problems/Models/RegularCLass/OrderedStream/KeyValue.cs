﻿namespace OOPS_Problems.Models.RegularCLass.OrderedStream
{
    public class KeyValue
    {
        public int IdKey { get; set; }
        public string? Value { get; set; }
    }
}
