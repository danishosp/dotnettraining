﻿namespace OOPS_Problems.Models.RegularCLass.OrderedStream
{
    public class OrderedStream
    {
        private readonly List<KeyValue> _buffer;
        private readonly int _number;
        private int _nextIdKey;

        public OrderedStream(int number)
        {
            _buffer = new List<KeyValue>(number);
            _number = number;
            _nextIdKey = 1;
        }

        public string[] Insert(int idKey, string value)
        {
            _buffer.Add(new KeyValue { IdKey = idKey, Value = value });

            if (idKey != _nextIdKey)
            {
                return Array.Empty<string>();
            }

            var chunk = new List<string>();
            while (_nextIdKey <= _number && _buffer.Any(kv => kv.IdKey == _nextIdKey))
            {
                var kv = _buffer.First(x => x.IdKey == _nextIdKey);
                chunk.Add(kv.Value!);
                _buffer.Remove(kv);
                _nextIdKey++;
            }

            return chunk.ToArray();
        }
    }
}
