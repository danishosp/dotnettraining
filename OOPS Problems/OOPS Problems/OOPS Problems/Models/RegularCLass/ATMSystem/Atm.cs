﻿using Microsoft.AspNetCore.Mvc;

namespace OOPS_Problems.Models.RegularCLass.ATMSystem
{
    public class Atm
    {
        private readonly int[]? _availableDenominations;
        private int _balance;


        public Atm(int[] denomination, int balance)
        {
            _availableDenominations = denomination;
            _balance = balance;
        }

        public int GetBalance()
        {
            return _balance;
        }

        public IEnumerable<int> Withdraw(int amount)
        {
            if (amount <= 0 || amount > _balance)
            {
                yield return 0;
            }


            if (amount > _availableDenominations!.Sum())
            {
                yield return 1;  //"Insufficient balance in the ATM";
            }

            List<int> result = new();
            foreach (int denomination in _availableDenominations!)
            {
                int count = amount / denomination;
                if (count > 0)
                {
                    result.Add(count);
                    amount -= count * denomination;
                }
            }

            _balance -= result.Sum(denomination => denomination * _availableDenominations[result.IndexOf(denomination)]);
            yield return _balance;
        }
    }
}
