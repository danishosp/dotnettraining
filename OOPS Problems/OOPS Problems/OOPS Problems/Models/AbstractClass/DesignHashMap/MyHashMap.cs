﻿namespace OOPS_Problems.Models.AbstractClass.DesignHashMap
{
    public abstract class MyHashMap
    {
        public abstract void Put(int key, int value);
        public abstract int Get(int key);
        public abstract void Remove(int key);
    }
}
