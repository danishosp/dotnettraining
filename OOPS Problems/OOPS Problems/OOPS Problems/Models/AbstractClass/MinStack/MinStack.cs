﻿namespace OOPS_Problems.Models.AbstractClass.MinStack
{
    public class MinStack : Stack
    {
        private readonly Stack<int> _stack;
        private readonly Stack<int> _minStack;

        public MinStack()
        {
            _stack = new Stack<int>();
            _minStack = new Stack<int>();
        }

        public override void Push(int val)
        {
            _stack.Push(val);

            if (_minStack.Count == 0 || val <= _minStack.Peek())
            {
                _minStack.Push(val);
            }
        }

        public override void Pop()
        {
            if (_stack.Count == 0)
            {
                return;
            }

            if (_stack.Peek() == _minStack.Peek())
            {
                _minStack.Pop();
            }

            _stack.Pop();
        }

        public override int Top()
        {
            return _stack.Peek();
        }

        public override bool IsEmpty()
        {
            return _stack.Count == 0;
        }

        public int GetMin()
        {
            return _minStack.Peek();
        }
    }
}
