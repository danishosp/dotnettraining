﻿namespace OOPS_Problems.Models.AbstractClass.MinStack
{
    public abstract class Stack
    {
        public abstract void Push(int val);
        public abstract void Pop();
        public abstract int Top();
        public abstract bool IsEmpty();
    }
}
