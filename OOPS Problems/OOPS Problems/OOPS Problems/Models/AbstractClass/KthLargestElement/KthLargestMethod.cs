﻿using System.IO;

namespace OOPS_Problems.Models.AbstractClass.KthLargestElement
{
    public class KthLargestMethod : KthLargest
    {
        private readonly List<int> _stream;
        public KthLargestMethod(int[] numbers)
        {
            _stream = new List<int>(numbers);
            _stream.Sort((a, b) => b.CompareTo(a));
            
        }
        public override int Add(int value, int key)
        {
            int index = _stream.BinarySearch(value);

            if (index < 0)
            {
                index = ~index;
            }
            _stream.Insert(index, value);
            return (_stream[key - 1]);
        }
    }
}
