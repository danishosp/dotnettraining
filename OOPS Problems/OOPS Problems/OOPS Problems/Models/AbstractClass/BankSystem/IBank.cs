﻿namespace OOPS_Problems.Controllers.Models
{
    public abstract class IBank
    {
        public abstract string ValidateCard();
        public abstract string WithdrawMoney();
        public abstract string CheckBalanace();
        public abstract string BankTransfer();
        public abstract string MiniStatement();
    }
}
