﻿namespace OOPS_Problems.Controllers.Models
{
    public class AXIX : IBank
    {
        public override string BankTransfer()
        {
            return "AXIX Bank Bank Transfer";
        }

        public override string CheckBalanace()
        {
            return "AXIX Bank Check Balanace";
        }

        public override string MiniStatement()
        {
            return "AXIX Bank Mini Statement";
        }

        public override string ValidateCard()
        {
            return "AXIX Bank Validate Card";
        }

        public override string WithdrawMoney()
        {
            return "AXIX Bank Withdraw Money";
        }
    }
}
