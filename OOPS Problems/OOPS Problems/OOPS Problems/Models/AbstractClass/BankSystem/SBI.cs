﻿namespace OOPS_Problems.Controllers.Models
{
    public class SBI : IBank
    {
        public override string BankTransfer()
        {
            return "SBI Bank Bank Transfer";
        }

        public override string CheckBalanace()
        {
            return "SBI Bank Check Balanace";
        }

        public override string MiniStatement()
        {
            return "SBI Bank Mini Statement";
        }

        public override string ValidateCard()
        {
            return "SBI Bank Validate Card";
        }

        public override string WithdrawMoney()
        {
            return "SBI Bank Withdraw Money";
        }
    }
}
