﻿namespace OOPS_Problems.Models.SealedClass.SeatReversationManager
{
    public sealed class Seat
    {
        public int SeatNumber { get; set; }
        public bool IsReserved { get; set; }
    }
}
