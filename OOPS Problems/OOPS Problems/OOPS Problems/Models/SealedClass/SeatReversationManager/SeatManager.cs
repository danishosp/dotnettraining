﻿namespace OOPS_Problems.Models.SealedClass.SeatReversationManager
{
    public sealed class SeatManager
    {
        private readonly List<Seat> _seats;

        public SeatManager(int n)
        {
            _seats = Enumerable.Range(1, n).Select(i => new Seat { SeatNumber = i, IsReserved = false }).ToList();
        }

        public int Reserve()
        {
            var seat = _seats.FirstOrDefault(s => !s.IsReserved);
            if (seat != null)
            {
                seat.IsReserved = true;
                return seat.SeatNumber;
            }
            return -1;
        }

        public void Unreserve(int seatNumber)
        {
            var seat = _seats.FirstOrDefault(s => s.SeatNumber == seatNumber);
            if (seat != null)
            {
                seat.IsReserved = false;
            }
        }
    }
}
