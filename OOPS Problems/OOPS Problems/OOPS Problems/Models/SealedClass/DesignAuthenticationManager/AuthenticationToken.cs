﻿namespace OOPS_Problems.Models.SealedClass.DesignAuthenticationManager
{
    public sealed class AuthenticationToken
    {
        public string? TokenId { get; set; }
        public int ExpirationTime { get; set; }
    }
}
