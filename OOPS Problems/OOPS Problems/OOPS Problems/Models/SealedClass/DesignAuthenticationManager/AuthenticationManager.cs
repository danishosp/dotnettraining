﻿namespace OOPS_Problems.Models.SealedClass.DesignAuthenticationManager
{
    public sealed class AuthenticationManager
    {
        private readonly int _timeToLive;
        private readonly List<AuthenticationToken> _tokens;

        public AuthenticationManager(int timeToLive)
        {
            _timeToLive = timeToLive;
            _tokens = new List<AuthenticationToken>();
        }

        public string Generate(string tokenId, int currentTime)
        {
            var token = new AuthenticationToken { TokenId = tokenId, ExpirationTime = currentTime + _timeToLive };
            _tokens.Add(token);
            return token.TokenId;
        }

        public void Renew(string tokenId, int currentTime)
        {
            var token = _tokens.FirstOrDefault(t => t.TokenId == tokenId && t.ExpirationTime > currentTime);
            if (token != null)
            {
                token.ExpirationTime = currentTime + _timeToLive;
            }
        }

        public int CountUnexpiredTokens(int currentTime)
        {
            _tokens.RemoveAll(t => t.ExpirationTime <= currentTime);
            return _tokens.Count;
        }

    }
}
