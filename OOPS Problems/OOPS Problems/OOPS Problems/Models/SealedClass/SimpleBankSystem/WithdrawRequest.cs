﻿namespace OOPS_Problems.Models.SealedClass.SimpleBankSystem
{
    public sealed class WithdrawRequest
    {
        public int Account { get; set; }
        public long Money { get; set; }
    }
}
