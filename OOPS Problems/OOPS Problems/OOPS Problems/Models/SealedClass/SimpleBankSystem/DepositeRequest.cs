﻿namespace OOPS_Problems.Models.SealedClass.SimpleBankSystem
{
    public sealed class DepositeRequest
    {
        public int Account { get; set; }
        public long Money { get; set; }
    }
}
