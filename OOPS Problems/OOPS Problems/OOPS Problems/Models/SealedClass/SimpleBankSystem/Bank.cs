﻿namespace OOPS_Problems.Models.SealedClass.SimpleBankSystem
{
    public sealed class Bank
    {
        private readonly long[] _balances;

        public Bank(long[] balance)
        {
            _balances = balance;
        }

        public bool Transfer(int account1, int account2, long money)
        {
            if (account1 < 1 || account1 > _balances.Length || account2 < 1 || account2 > _balances.Length)
            {
                return false;
            }

            if (_balances[account1 - 1] >= money)
            {
                _balances[account1 - 1] -= money;
                _balances[account2 - 1] += money;
                return true;
            }

            return false;
        }

        public bool Deposit(int account, long money)
        {
            if (account < 1 || account > _balances.Length)
            {
                return false;
            }

            _balances[account - 1] += money;
            return true;
        }

        public bool Withdraw(int account, long money)
        {
            if (account < 1 || account > _balances.Length || _balances[account - 1] < money)
            {
                return false;
            }

            _balances[account - 1] -= money;
            return true;
        }
    }
}
