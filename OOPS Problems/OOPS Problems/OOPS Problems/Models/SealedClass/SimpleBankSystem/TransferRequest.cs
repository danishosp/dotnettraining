﻿namespace OOPS_Problems.Models.SealedClass.SimpleBankSystem
{
    public sealed class TransferRequest
    {
        public int Account1 { get; set; }
        public int Account2 { get; set; }
        public long Money { get; set; }
    }
}
