﻿namespace OOPS_Problems.Models.SealedClass.DesignUndergroundSystem
{
    public sealed class CheckOutRecord
    {
        public int Id { get; set; }
        public string? StationName { get; set; }
        public int Time { get; set; }
    }
}
