﻿namespace OOPS_Problems.Models.SealedClass.DesignUndergroundSystem
{
    public sealed class StationRouteRecord
    {
        public string? StartStation { get; set; }
        public string? EndStation { get; set; }
        public double TotalTime { get; set; }
        public int Count { get; set; }
    }
}
