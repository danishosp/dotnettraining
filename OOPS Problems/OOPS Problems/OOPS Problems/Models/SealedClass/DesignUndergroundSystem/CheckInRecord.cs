﻿namespace OOPS_Problems.Models.SealedClass.DesignUndergroundSystem
{
    public sealed class CheckInRecord
    {
        public int Id { get; set; }
        public string? StationName { get; set; }
        public int Time { get; set; }
    }
}
