﻿namespace OOPS_Problems.Models.SealedClass.ProductLastNumber
{
    public sealed class ProductOfNumbers
    {
        private readonly List<int> _stream;

        public ProductOfNumbers()
        {
            _stream = new List<int>();
        }

        public void Add(int num)
        {
            _stream.Add(num);
        }

        public int GetProduct(int key)
        {
            int product = 1;
            int startIndex = _stream.Count - key;
            for (int i = startIndex; i < _stream.Count; i++)
            {
                product *= _stream[i];
            }
            return product;
        }
    }
}
