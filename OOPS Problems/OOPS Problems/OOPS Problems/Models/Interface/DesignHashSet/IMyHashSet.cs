﻿namespace OOPS_Problems.Models.Interface.DesignHashSet
{
    public interface IMyHashSet
    {
        public int Add(int key);
        public bool Contains(int key);
        public int Remove(int key);
    }
}
