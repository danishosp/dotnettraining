﻿using Microsoft.AspNetCore.DataProtection.KeyManagement;

namespace OOPS_Problems.Models.Interface.DesignHashSet
{
    public class MyHashSet : IMyHashSet
    {
        private readonly List<int>[] _hashSet;
        public MyHashSet() 
        {
            _hashSet = new List<int>[1000];
        }

        public int Add(int key)
        {
            int index = key % 1000;
            if (_hashSet[index] == null)
            {
                _hashSet[index] = new List<int>();
            }
            if (!_hashSet[index].Contains(key))
            {
                _hashSet[index].Add(key);
            }
            return index;
        }

        public bool Contains(int key)
        {
            int index = key % 1000;
            if (_hashSet[index] == null)
            {
                return false;
            }
            return _hashSet[index].Contains(key);
        }

        public int Remove(int key)
        {
            int index = key % 1000;
            if (_hashSet[index] != null)
            {
                _hashSet[index].Remove(key);
            }
            return index;
        }
    }
}
