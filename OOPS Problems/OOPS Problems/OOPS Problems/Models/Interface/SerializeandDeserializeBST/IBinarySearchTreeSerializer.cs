﻿namespace OOPS_Problems.Models.Interface.SerializeandDeserializeBST
{
    public interface IBinarySearchTreeSerializer
    {
        string Serialize(BinaryTreeNode root);
        BinaryTreeNode Deserialize(string treeString);
    }
}
