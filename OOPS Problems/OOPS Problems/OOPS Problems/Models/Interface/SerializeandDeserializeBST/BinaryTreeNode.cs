﻿namespace OOPS_Problems.Models.Interface.SerializeandDeserializeBST
{
    public class BinaryTreeNode
    {
        public int Value { get; set; }
        public BinaryTreeNode? Left { get; set; }
        public BinaryTreeNode? Right { get; set; }
    }
}
