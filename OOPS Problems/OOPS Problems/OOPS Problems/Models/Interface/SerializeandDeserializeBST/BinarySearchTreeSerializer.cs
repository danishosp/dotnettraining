﻿namespace OOPS_Problems.Models.Interface.SerializeandDeserializeBST
{
    public class BinarySearchTreeSerializer : IBinarySearchTreeSerializer
    {
        public string Serialize(BinaryTreeNode root)
        {
            if (root == null)
            {
                return "";
            }

            string leftString = Serialize(root.Left);
            string rightString = Serialize(root.Right);

            return root.Value + "," + leftString + rightString;
        }

        public BinaryTreeNode Deserialize(string treeString)
        {
            if (string.IsNullOrEmpty(treeString))
            {
                return null!;
            }

            string[] nodeStrings = treeString.Split(',');
            Queue<string> nodeQueue = new Queue<string>(nodeStrings);
            return DeserializeHelper(nodeQueue);
        }

        private BinaryTreeNode DeserializeHelper(Queue<string> nodeQueue)
        {
            if (nodeQueue.Count == 0)
            {
                return null!;
            }

            string nodeString = nodeQueue.Dequeue();
            if (string.IsNullOrEmpty(nodeString))
            {
                return null!;
            }

            int nodeValue = int.Parse(nodeString);
            BinaryTreeNode node = new()
            {
                Value = nodeValue,
                Left = DeserializeHelper(nodeQueue),
                Right = DeserializeHelper(nodeQueue)
            };

            return node;
        }

    }
}
