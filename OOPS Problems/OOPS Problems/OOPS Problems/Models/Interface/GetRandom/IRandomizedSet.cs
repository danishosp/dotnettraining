﻿namespace OOPS_Problems.Models.Interface.GetRandom
{
    public interface IRandomizedSet
    {
        bool Insert(int val);
        bool Remove(int val);
        int GetRandom();
    }

}
