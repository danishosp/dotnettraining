﻿using System.Collections.ObjectModel;

namespace OOPS_Problems.Models.Interface.GetRandom
{
    public class RandomizedSet : IRandomizedSet
    {
        private readonly List<int> _list;
        private readonly Dictionary<int, int> _dict;
        private readonly Random _random;

        public RandomizedSet()
        {
            _list = new List<int>();
            _dict = new Dictionary<int, int>();
            _random = new Random();
        }

        public bool Insert(int value)
        {
            if (_dict.ContainsKey(value))
            {
                return false;
            }
            _dict[value] = _list.Count;
            _list.Add(value);
            return true;
        }

        public bool Remove(int value)
        {
            if (!_dict.ContainsKey(value))
            {
                return false;
            }
            int index = _dict[value];
            int last = _list[_list.Count - 1];
            _list[index] = last;
            _dict[last] = index;
            _list.RemoveAt(_list.Count - 1);
            _dict.Remove(value);
            return true;
        }

        public int GetRandom()
        {
            int index = _random.Next(_list.Count);
            return _list[index];
        }
    }
}
