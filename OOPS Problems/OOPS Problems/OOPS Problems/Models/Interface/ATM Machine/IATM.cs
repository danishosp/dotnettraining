﻿namespace OOPS_Problems.Models.Interface.ATM_Machine
{
    public interface IATM
    {
        void Deposit(int[] banknotesCount);
        int[] Withdraw(int amount);
    }
}
