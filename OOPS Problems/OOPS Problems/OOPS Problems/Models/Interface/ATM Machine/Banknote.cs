﻿namespace OOPS_Problems.Models.Interface.ATM_Machine
{
    public class Banknote
    {
        public int Denomination { get; set; }
        public int Count { get; set; }
    }
}
