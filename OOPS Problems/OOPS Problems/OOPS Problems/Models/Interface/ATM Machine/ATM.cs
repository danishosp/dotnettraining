﻿namespace OOPS_Problems.Models.Interface.ATM_Machine
{
    public class ATM : IATM
    {
        private List<Banknote> _banknotes;
        public ATM()
        {
            _banknotes = new List<Banknote>();
            _banknotes.Add(new Banknote { Denomination = 20, Count = 0 });
            _banknotes.Add(new Banknote { Denomination = 50, Count = 0 });
            _banknotes.Add(new Banknote { Denomination = 100, Count = 0 });
            _banknotes.Add(new Banknote { Denomination = 200, Count = 0 });
            _banknotes.Add(new Banknote { Denomination = 500, Count = 0 });
        }



        public void Deposit(int[] banknotesCount)
        {
            for (int i = 0; i < _banknotes.Count; i++)
            {
                _banknotes[i].Count += banknotesCount[i];
            }
        }



        public int[] Withdraw(int amount)
        {
            int[] result = new int[5];



            // Copy the banknote list so we can manipulate it
            List<Banknote> banknotesCopy = new List<Banknote>(_banknotes);



            // Calculate the number of banknotes to withdraw for each denomination
            for (int i = banknotesCopy.Count - 1; i >= 0; i--)
            {
                int banknoteCount = Math.Min(amount / banknotesCopy[i].Denomination, banknotesCopy[i].Count);
                amount -= banknoteCount * banknotesCopy[i].Denomination;
                banknotesCopy[i].Count -= banknoteCount;
                result[i] = banknoteCount;
            }



            // If the entire amount has been withdrawn, update the banknote list
            if (amount == 0)
            {
                _banknotes = banknotesCopy;
            }
            else
            {
                result = new int[] { -1 };
            }



            return result;
        }
    }
}
