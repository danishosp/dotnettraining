﻿namespace OOPS_Problems.Models.StaticClass.DesignFoodRatingSystem
{
    public static class FoodRatings
    {
        private static readonly List<FoodItem> _foods = new();

        public static void Initialize(string[] foods, string[] cuisines, int[] ratings)
        {
            for (int i = 0; i < foods.Length; i++)
            {
                _foods.Add(new FoodItem { Name = foods[i], Cuisine = cuisines[i], Rating = ratings[i] });
            }
        }

        public static int ChangeRating(string food, int newRating)
        {
            var foodItem = _foods.FirstOrDefault(f => f.Name == food);
            if (foodItem != null)
            {
                foodItem.Rating = newRating;
            }
            return _foods.Count;
        }

        public static string HighestRated(string cuisine)
        {
            var highestRatedFood = _foods.Where(f => f.Cuisine == cuisine)
                                          .OrderByDescending(f => f.Rating)
                                          .ThenBy(f => f.Name)
                                          .FirstOrDefault();

            return highestRatedFood?.Name ?? "";
        }
    }
}
