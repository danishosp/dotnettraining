﻿namespace OOPS_Problems.Models.StaticClass.DesignFoodRatingSystem
{
    public class FoodItem
    {
        public string Name { get; set; }
        public string Cuisine { get; set; }
        public int Rating { get; set; }
    }
}
