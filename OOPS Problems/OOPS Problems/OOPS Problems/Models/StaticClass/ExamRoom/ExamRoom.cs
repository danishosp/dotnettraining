﻿namespace OOPS_Problems.Models.StaticClass.ExamRoom
{
    public static class ExamRoom
    {
        private static int _n;
        private static SortedSet<int>? _seats;

        public static void Initialize(int n)
        {
            _n = n;
            _seats = new SortedSet<int>();
        }

        public static int Seat()
        {
            if (_seats!.Count == 0)
            {
                _seats.Add(0);
                return 0;
            }

            int maxDistance = _seats.First();
            int seat = 0;

            foreach (int s in _seats)
            {
                int dist = (s - maxDistance) / 2;
                if (dist > maxDistance)
                {
                    maxDistance = dist;
                    seat = s - dist;
                }
            }

            if (_n - 1 - _seats.Last() > maxDistance)
            {
                seat = _n - 1;
            }

            _seats.Add(seat);
            return seat;
        }

        public static void Leave(int p)
        {
            _seats!.Remove(p);
        }
    }
}
