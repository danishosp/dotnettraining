﻿namespace OOPS_Problems.Models.StaticClass.OnlineStockSpan
{
    public class StockPrices
    {
        private readonly static List<int> prices = new();

        public static int GetSpan(int price)
        {
            int span = 1;

            // Check if there are any previous prices
            if (prices.Count > 0)
            {
                // Loop through the previous prices
                for (int i = prices.Count - 1; i >= 0; i--)
                {
                    // If the previous price is less than or equal to the current price
                    if (prices[i] <= price)
                    {
                        // Add to the span and continue checking
                        span++;
                    }
                    else
                    {
                        // If the previous price is greater than the current price, break the loop
                        break;
                    }
                }
            }

            // Add the current price to the list
            prices.Add(price);

            return span;
        }
    }
}
