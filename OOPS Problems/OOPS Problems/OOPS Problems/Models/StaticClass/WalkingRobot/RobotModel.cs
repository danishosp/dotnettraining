﻿namespace OOPS_Problems.Models.StaticClass.WalkingRobot
{
    public class RobotModel
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public int CurrentX { get; set; }
        public int CurrentY { get; set; }
        public string Direction { get; set; }

        public RobotModel(int width, int height)
        {
            Width = width;
            Height = height;
            CurrentX = 0;
            CurrentY = 0;
            Direction = "East";
        }

        public void Step(int num)
        {
            for (int i = 0; i < num; i++)
            {
                switch (Direction)
                {
                    case "North":
                        if (CurrentY + 1 < Height)
                        {
                            CurrentY++;
                        }
                        else
                        {
                            TurnLeft();
                        }
                        break;
                    case "East":
                        if (CurrentX + 1 < Width)
                        {
                            CurrentX++;
                        }
                        else
                        {
                            TurnLeft();
                        }
                        break;
                    case "South":
                        if (CurrentY - 1 >= 0)
                        {
                            CurrentY--;
                        }
                        else
                        {
                            TurnLeft();
                        }
                        break;
                    case "West":
                        if (CurrentX - 1 >= 0)
                        {
                            CurrentX--;
                        }
                        else
                        {
                            TurnLeft();
                        }
                        break;
                }
            }
        }

        private void TurnLeft()
        {
            switch (Direction)
            {
                case "North":
                    Direction = "West";
                    break;
                case "East":
                    Direction = "North";
                    break;
                case "South":
                    Direction = "East";
                    break;
                case "West":
                    Direction = "South";
                    break;
            }
        }

        public int[] GetPos()
        {
            return new int[] { CurrentX, CurrentY };
        }

        public string GetDir()
        {
            return Direction;
        }

    }
}
