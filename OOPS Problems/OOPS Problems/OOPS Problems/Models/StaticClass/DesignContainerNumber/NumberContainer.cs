﻿namespace OOPS_Problems.Models.StaticClass.DesignContainerNumber
{
    public class NumberContainer
    {
        public int Index { get; set; }
        public int Number { get; set; }
    }
}
