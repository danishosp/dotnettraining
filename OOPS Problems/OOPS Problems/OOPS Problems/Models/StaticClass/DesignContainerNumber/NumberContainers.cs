﻿namespace OOPS_Problems.Models.StaticClass.DesignContainerNumber
{
    public static class NumberContainers
    {
        private static List<NumberContainer> _containers = new List<NumberContainer>();

        public static int Change(int index, int number)
        {
            var container = _containers.FirstOrDefault(c => c.Index == index);
            if (container != null)
            {
                container.Number = number;
            }
            else
            {
                _containers.Add(new NumberContainer { Index = index, Number = number });
            }
            return index;
        }

        public static int Find(int number)
        {
            var container = _containers.FirstOrDefault(c => c.Number == number);
            return container?.Index ?? -1;
        }
    }
}
