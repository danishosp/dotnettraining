﻿using static System.Runtime.InteropServices.JavaScript.JSType;

namespace OOPS_Problems.Models.Polymorphism.DinnerPlateModel
{
    public class NormalStack : Stack
    {
        public NormalStack(int capacity) : base(capacity) { }
        public override void Push(int val)
        {
            if (!IsFull())
            {
                data.Add(val);
            }
        }
        public override int Pop()
        {
            if (IsEmpty())
            {
                return -1;
            }
            else
            {
                int result = data[data.Count - 1];
                data.RemoveAt(data.Count - 1);
                return result;
            }
        }
    }
}
