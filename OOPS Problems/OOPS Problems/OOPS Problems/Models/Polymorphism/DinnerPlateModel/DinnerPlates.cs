﻿namespace OOPS_Problems.Models.Polymorphism.DinnerPlateModel
{
    public class DinnerPlates
    {
        private readonly int _capacity;
        private readonly List<Stack> _stacks;
        public DinnerPlates(int capacity)
        {
            _capacity = capacity;
            _stacks = new List<Stack>();
        }
        public void Push(int val)
        {
            if (_stacks.Count == 0 || _stacks[_stacks.Count - 1].IsFull())
            {
                Stack newStack = new NormalStack(_capacity);
                newStack.Push(val);
                _stacks.Add(newStack);
            }
            else
            {
                _stacks[_stacks.Count - 1].Push(val);
            }
        }
        public int Pop()
        {
            if (_stacks.Count == 0 || _stacks[_stacks.Count - 1].IsEmpty())
            {
                return -1;
            }
            else
            {
                int result = _stacks[_stacks.Count - 1].Pop();
                if (_stacks[_stacks.Count - 1].IsEmpty())
                {
                    _stacks.RemoveAt(_stacks.Count - 1);
                }
                return result;
            }
        }
        public int PopAtStack(int index)
        {
            if (index >= _stacks.Count || _stacks[index].IsEmpty())
            {
                return -1;
            }
            else
            {
                int result = _stacks[index].Pop();
                if (_stacks[index].IsEmpty())
                {
                    _stacks.RemoveAt(index);
                }
                return result;
            }
        }
    }
}
