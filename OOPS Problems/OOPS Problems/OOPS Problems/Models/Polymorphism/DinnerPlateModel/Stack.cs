﻿namespace OOPS_Problems.Models.Polymorphism.DinnerPlateModel
{
    public abstract class Stack
    {
        protected int capacity;
        protected List<int> data;
        public Stack(int capacity)
        {
            this.capacity = capacity;
            data = new List<int>();
        }
        public bool IsFull()
        {
            return data.Count == capacity;
        }
        public bool IsEmpty()
        {
            return data.Count == 0;
        }
        public abstract void Push(int val);
        public abstract int Pop();
    }
}
