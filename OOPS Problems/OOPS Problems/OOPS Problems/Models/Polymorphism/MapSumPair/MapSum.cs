﻿namespace OOPS_Problems.Models.Polymorphism.MapSumPair
{
    public class MapSum : Map
    {
        private readonly List<KeyValue> _keyValues;

        public MapSum()
        {
            _keyValues = new List<KeyValue>();
        }

        public override void Insert(string key, int value)
        {
            var existingKeyValue = _keyValues.FirstOrDefault(k => k.Key == key);
            if (existingKeyValue != null)
            {
                existingKeyValue.Value = value;
            }
            else
            {
                _keyValues.Add(new KeyValue { Key = key, Value = value });
            }
        }

        public override int Sum(string prefix)
        {
            var sum = 0;
            foreach (var keyValue in _keyValues)
            {
                if (keyValue.Key!.StartsWith(prefix))
                {
                    sum += keyValue.Value;
                }
            }
            return sum;
        }
    }
}
