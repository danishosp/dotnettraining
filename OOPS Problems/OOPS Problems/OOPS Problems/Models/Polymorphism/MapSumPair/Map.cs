﻿namespace OOPS_Problems.Models.Polymorphism.MapSumPair
{
    public abstract class Map
    {
        public abstract void Insert(string key, int value);
        public abstract int Sum(string prefix);
    }
}
