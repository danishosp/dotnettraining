﻿namespace OOPS_Problems.Models.Polymorphism.MapSumPair
{
    public class KeyValue
    {
        public string? Key { get; set; }
        public int Value { get; set; }
    }
}
