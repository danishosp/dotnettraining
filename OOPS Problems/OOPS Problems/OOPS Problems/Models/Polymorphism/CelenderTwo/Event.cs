﻿namespace OOPS_Problems.Models.Polymorphism.CelenderTwo
{
    public class Event
    {
        public int Start { get; set; }
        public int End { get; set; }
    }
}
