﻿namespace OOPS_Problems.Models.Polymorphism.CelenderTwo
{
    public abstract class MyCalendar
    {
        public abstract bool Book(Event newEvent);
    }
}
