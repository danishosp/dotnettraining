﻿namespace OOPS_Problems.Models.Polymorphism.CelenderTwo
{
    public class MyCalendarTwo : MyCalendar
    {
        private readonly List<Event> _bookings;
        private readonly List<Event> _overlaps;

        public MyCalendarTwo()
        {
            _bookings = new List<Event>();
            _overlaps = new List<Event>();
        }

        public override bool Book(Event newEvent)
        {
            foreach (var overlap in _overlaps)
            {
                if (newEvent.Start < overlap.End && overlap.Start < newEvent.End)
                {
                    return false; // triple booking
                }
            }

            foreach (var booking in _bookings)
            {
                if (newEvent.Start < booking.End && booking.Start < newEvent.End)
                {
                    // create a new overlap event
                    var overlapStart = Math.Max(newEvent.Start, booking.Start);
                    var overlapEnd = Math.Min(newEvent.End, booking.End);
                    var overlap = new Event { Start = overlapStart, End = overlapEnd };
                    _overlaps.Add(overlap);
                }
            }

            // add the new event to the bookings list
            _bookings.Add(newEvent);
            return true;
        }
    }
}
