﻿namespace OOPS_Problems.Models.Polymorphism.FindMedainFormDataStream
{
    public abstract class MedianFinder
    {
        protected List<int> _data;

        public MedianFinder()
        {
            _data = new List<int>();
        }

        public abstract void AddNum(int num);
        public abstract double FindMedian();
    }
}
