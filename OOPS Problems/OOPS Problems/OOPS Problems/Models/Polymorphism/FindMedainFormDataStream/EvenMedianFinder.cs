﻿using static System.Runtime.InteropServices.JavaScript.JSType;

namespace OOPS_Problems.Models.Polymorphism.FindMedainFormDataStream
{
    public class EvenMedianFinder : MedianFinder
    {
        public override void AddNum(int num)
        {
            _data.Add(num);
        }

        public override double FindMedian()
        {
            int middleIndex = _data.Count / 2;
            _data.Sort();
            double median = (_data[middleIndex - 1] + _data[middleIndex]) / 2.0;
            return median;
        }
    }
}
