﻿using static System.Runtime.InteropServices.JavaScript.JSType;

namespace OOPS_Problems.Models.Polymorphism.FindMedainFormDataStream
{
    public class OddMedianFinder : MedianFinder
    {
        public override void AddNum(int num)
        {
            _data.Add(num);
        }

        public override double FindMedian()
        {
            int middleIndex = _data.Count / 2;
            _data.Sort();
            return _data[middleIndex];
        }
    }
}
