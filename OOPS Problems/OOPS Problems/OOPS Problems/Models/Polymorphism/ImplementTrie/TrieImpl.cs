﻿namespace OOPS_Problems.Models.Polymorphism.ImplementTrie
{
    public class TrieImpl : Trie 
    {
        private readonly TrieNode _root;

        public TrieImpl()
        {
            _root = new TrieNode();
        }

        public override void Insert(string word)
        {
            var currentNode = _root;
            foreach (var c in word)
            {
                var index = c - 'a';
                if (currentNode.Children[index] == null)
                {
                    currentNode.Children[index] = new TrieNode();
                }
                currentNode = currentNode.Children[index];
            }
            currentNode.IsEndOfWord = true;
        }

        public override bool Search(string word)
        {
            var currentNode = _root;
            foreach (var c in word)
            {
                var index = c - 'a';
                if (currentNode.Children[index] == null)
                {
                    return false;
                }
                currentNode = currentNode.Children[index];
            }
            return currentNode.IsEndOfWord;
        }

        public override bool StartsWith(string prefix)
        {
            var currentNode = _root;
            foreach (var c in prefix)
            {
                var index = c - 'a';
                if (currentNode.Children[index] == null)
                {
                    return false;
                }
                currentNode = currentNode.Children[index];
            }
            return true;
        }
    }
}
