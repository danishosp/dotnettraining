﻿namespace OOPS_Problems.Models.Polymorphism.ImplementTrie
{
    public abstract class Trie
    {
        public abstract void Insert(string word);
        public abstract bool Search(string word);
        public abstract bool StartsWith(string prefix);
    }
}
