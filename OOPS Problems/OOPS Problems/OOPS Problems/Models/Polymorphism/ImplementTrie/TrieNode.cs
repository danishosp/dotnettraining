﻿namespace OOPS_Problems.Models.Polymorphism.ImplementTrie
{
    public class TrieNode
    {
        public bool IsEndOfWord { get; set; }
        public TrieNode[] Children { get; set; }

        public TrieNode()
        {
            IsEndOfWord = false;
            Children = new TrieNode[26];
        }
    }
}
