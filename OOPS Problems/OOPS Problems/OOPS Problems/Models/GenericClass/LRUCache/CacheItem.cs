﻿namespace OOPS_Problems.Models.GenericClass.LRUCache
{
    public class CacheItem<T>
    {
        public int Key { get; set; }
        public T? Value { get; set; }
        public CacheItem<T>? Next { get; set; }
        public CacheItem<T>? Prev { get; set; }
    }
}
