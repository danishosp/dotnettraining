﻿namespace OOPS_Problems.Models.GenericClass.LRUCache
{
    public class LRUCache<T>
    {
        private readonly Dictionary<int, CacheItem<T>> _dict;
        private readonly CacheItem<T> _head;
        private readonly CacheItem<T> _tail;
        private readonly int _capacity;

        public LRUCache(int capacity)
        {
            _dict = new Dictionary<int, CacheItem<T>>();
            _head = new CacheItem<T>();
            _tail = new CacheItem<T>();
            _head.Next = _tail;
            _tail.Prev = _head;
            _capacity = capacity;
        }

        public T Get(int key)
        {
            if (_dict.TryGetValue(key, out CacheItem<T>? item))
            {
                Remove(item);
                AddToFront(item);
                return item.Value!;
            }
            return default!;
        }

        public void Put(int key, T value)
        {
            if (_dict.TryGetValue(key, out CacheItem<T>? item))
            {
                item.Value = value;
                Remove(item);
                AddToFront(item);
            }
            else
            {
                if (_dict.Count == _capacity)
                {
                    _dict.Remove(_tail.Prev!.Key);
                    Remove(_tail.Prev);
                }
                var newItem = new CacheItem<T> { Key = key, Value = value };
                _dict.Add(key, newItem);
                AddToFront(newItem);
            }
        }

        private void Remove(CacheItem<T> item)
        {
            item.Prev!.Next = item.Next;
            item.Next!.Prev = item.Prev;
        }

        private void AddToFront(CacheItem<T> item)
        {
            item.Prev = _head;
            item.Next = _head.Next;
            _head.Next!.Prev = item;
            _head.Next = item;
        }
    }
}
