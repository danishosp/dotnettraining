﻿namespace OOPS_Problems.Models.GenericClass.DiscountNthOrder
{
    public class Cashier<TProduct, TBill> where TProduct : Product where TBill : Bill
    {
        private readonly int _n;
        private readonly decimal _discount;
        private readonly Dictionary<int, TProduct> _products;

        private int _customerCount;

        public Cashier(int n, decimal discount, IEnumerable<TProduct> products)
        {
            _n = n;
            _discount = discount;
            _products = products.ToDictionary(p => p.Id);
            _customerCount = 0;
        }

        public decimal GetBill(IEnumerable<TBill> bill)
        {
            decimal total = 0;

            foreach (var item in bill)
            {
                if (!_products.TryGetValue(item.ProductId, out var product))
                {
                    throw new ArgumentException($"Product with id {item.ProductId} not found.");
                }

                total += item.Amount * product.Price;
            }

            _customerCount++;

            if (_customerCount % _n == 0)
            {
                total *= (100 - _discount) / 100;
            }

            return total;
        }
    }
    
}
