﻿namespace OOPS_Problems.Models.GenericClass.DiscountNthOrder
{
    public class Bill
    {
        public int ProductId { get; set; }
        public int Amount { get; set; }
    }
}
