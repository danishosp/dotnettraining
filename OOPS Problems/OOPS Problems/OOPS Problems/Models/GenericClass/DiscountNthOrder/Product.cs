﻿namespace OOPS_Problems.Models.GenericClass.DiscountNthOrder
{
    public class Product
    {
        public int Id { get; set; }
        public decimal Price { get; set; }
    }
}
