﻿namespace OOPS_Problems.Models.GenericClass.FindPairWithSum
{
    public class FindSumPairModels
    {
       public int Total { get; set; } 
       public int Index { get; set; }
       public int Value { get; set; }
    }
}
