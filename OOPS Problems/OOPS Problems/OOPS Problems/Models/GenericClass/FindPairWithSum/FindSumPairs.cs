﻿namespace OOPS_Problems.Models.GenericClass.FindPairWithSum
{
    public class FindSumPairs<T>
    {
        private readonly T[] numbers1;
        private readonly T[] numsber2;
        private readonly Dictionary<T, int> _countDict;

        public FindSumPairs(T[] numbers1, T[] numbers2)
        {
            this.numbers1 = numbers1;
            this.numsber2 = numbers2;
            _countDict = new Dictionary<T, int>();
            foreach (T number in numbers2)
            {
                if (_countDict.TryGetValue(number, out int value))
                    value++;
                else
                    _countDict[number] = 1;
            }
        }

        public void Add(int index, T val)
        {
            T oldValue = numsber2[index];
            numsber2[index] = FindSumPairs<T>.AddValues(oldValue, val);
            _countDict[oldValue]--;
            if (_countDict.TryGetValue(numsber2[index], out int value))
                value++;
            else
                _countDict[numsber2[index]] = 1;
        }

        public int Count(T total)
        {
            int count = 0;
            foreach (T number in numbers1)
            {
                if (_countDict.ContainsKey(FindSumPairs<T>.SubtractValues(total, number)))
                    count += _countDict[FindSumPairs<T>.SubtractValues(total, number)];
            }
            return count;
        }

        private static T AddValues(T value1, T value2)
        {
            dynamic? a = value1;
            dynamic? b = value2;
            return a + b;
        }

        private static T SubtractValues(T value1, T value2)
        {
            dynamic? a = value1;
            dynamic? b = value2;
            return a - b;
        }
    }
}
