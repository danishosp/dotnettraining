﻿namespace OOPS_Problems.Models.GenericClass.SnapShotArray
{
    public class GetElementRequest
    {
        public int Index { get; set; }
        public int SnapId { get; set; }
    }
}
