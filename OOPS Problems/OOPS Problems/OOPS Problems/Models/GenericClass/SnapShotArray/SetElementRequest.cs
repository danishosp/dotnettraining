﻿namespace OOPS_Problems.Models.GenericClass.SnapShotArray
{
    public class SetElementRequest
    {
        public int Index { get; set; }
        public int Value { get; set; }
    }
}
