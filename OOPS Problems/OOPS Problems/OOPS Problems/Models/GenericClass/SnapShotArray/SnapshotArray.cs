﻿namespace OOPS_Problems.Models.GenericClass.SnapShotArray
{
    public class SnapshotArray<T>
    {
        private readonly List<List<(int, T)>> _snapshots;
        private readonly List<(int, T)> _currentSnapshot;

        public SnapshotArray(int length)
        {
            _snapshots = new List<List<(int, T)>> { _currentSnapshot! };
            _currentSnapshot = new List<(int, T)>();
            for (int i = 0; i < length; i++)
            {
                _currentSnapshot.Add((i, default(T)!));
            }

            
        }

        public void Set(int index, T val)
        {
            _currentSnapshot[index] = (index, val);
        }

        public int Snap()
        {
            List<(int, T)> newSnapshot = new(_currentSnapshot);
            _snapshots.Add(newSnapshot);
            return _snapshots.Count - 1;
        }

        public T Get(int index, int snapId)
        {
            List<(int, T)> snapshot = _snapshots[snapId];
            foreach ((int i, T val) in snapshot)
            {
                if (i == index)
                {
                    return val;
                }
            }
            return default!;
        }
    }
}
