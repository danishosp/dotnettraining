﻿namespace OOPS_Problems.Models.GenericClass.MyCalender
{
    public class MyCalendar<T> where T : Event
    {
        private readonly List<T> _events;

        public MyCalendar()
        {
            _events = new List<T>();
        }

        public bool Book(T newEvent)
        {
            foreach (T existingEvent in _events)
            {
                if (newEvent.Start < existingEvent.End && existingEvent.Start < newEvent.End)
                {
                    return false;
                }
            }

            _events.Add(newEvent);
            return true;
        }
    }
}