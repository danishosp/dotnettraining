﻿namespace OOPS_Problems.Models.GenericClass.MyCalender
{
    public class Event
    {
        public int Start { get; set; }
        public int End { get; set; }
    }
}
