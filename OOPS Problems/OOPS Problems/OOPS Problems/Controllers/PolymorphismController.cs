﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OOPS_Problems.Models.Polymorphism.CelenderTwo;
using OOPS_Problems.Models.Polymorphism.DinnerPlateModel;
using OOPS_Problems.Models.Polymorphism.FindMedainFormDataStream;
using OOPS_Problems.Models.Polymorphism.ImplementTrie;
using OOPS_Problems.Models.Polymorphism.MapSumPair;


namespace OOPS_Problems.Controllers
{
    [Route("api/polymorphism")]
    public class PolymorphismController : ControllerBase
    {
        private readonly MyCalendar _calendar;
        private readonly Map _map;
        private readonly Trie _trie;
        private readonly DinnerPlates _dinnerPlates;
        private readonly MedianFinder _medianFinder;

        public PolymorphismController()
        {
            _calendar = new MyCalendarTwo();
            _map = new MapSum();
            _trie = new TrieImpl();
            _dinnerPlates = new DinnerPlates(2);
            _medianFinder = new OddMedianFinder();
        }

        #region Calender Two
        [HttpPost, Route("calenderTwo/addEvent")]
        public ActionResult<bool> BookEvent(Event newEvent)
        {
            var success = _calendar.Book(newEvent);
            return Ok(success);
        }
        #endregion



        #region Map Two Sum
        [HttpPost, Route("mapTwoSum/add")]
        public ActionResult Insert(KeyValue keyValue)
        {
            _map.Insert(keyValue.Key, keyValue.Value);
            return Ok($"{keyValue.Value} is inserted to {keyValue.Key} successfully");
        }

        [HttpGet, Route("mapTwoSum/{prefix}")]
        public ActionResult<int> Sum(string prefix)
        {
            var sum = _map.Sum(prefix);
            return Ok(sum);
        }
        #endregion



        #region Implement Trie
        [HttpPost, Route("implementTrie/adds")]
        public ActionResult Insert(string word)
        {
            _trie.Insert(word);
            return Ok($"Data Inserted Successfully");
        }

        [HttpGet, Route("implementTrie/{word}")]
        public ActionResult<bool> Search(string word)
        {
            var found = _trie.Search(word);
            return Ok(found);
        }

        [HttpGet, Route("implementTrie/startsWith/{prefix}")]
        public ActionResult<bool> StartsWith(string prefix)
        {
            var found = _trie.StartsWith(prefix);
            return Ok(found);
        }
        #endregion



        #region DinnerPlate Model
        [HttpPost, Route("dinnerPlateModel/push")]
        public IActionResult Push(int val)
        {
            _dinnerPlates.Push(val);
            return Ok($"Push Successfully");
        }

        [HttpGet, Route("dinnerPlateModel/pop")]
        public IActionResult Pop()
        {
            int result = _dinnerPlates.Pop();
            if (result == -1)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpGet, Route("dinnerPlateModel/popAtStack/{index}")]
        public IActionResult PopAtStack(int index)
        {
            int result = _dinnerPlates.PopAtStack(index);
            if (result == -1)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        #endregion



        #region Find Median From Data Stream
        [HttpPost, Route("findMedianFromDataStream/addNumber")]
        public IActionResult AddNum(int num)
        {
            _medianFinder.AddNum(num);
            return Ok();
        }

        [HttpGet, Route("findMedianFromDataStream/getMedian")]
        public IActionResult FindMedian()
        {
            return Ok(_medianFinder.FindMedian());
        }
        #endregion
    }
}
