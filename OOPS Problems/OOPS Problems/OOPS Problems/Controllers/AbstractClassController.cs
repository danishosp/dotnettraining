﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using OOPS_Problems.Controllers.Models;
using OOPS_Problems.Models.AbstractClass.MinStack;
using OOPS_Problems.Models.AbstractClass.QueueUsingStacks;

namespace OOPS_Problems.Controllers
{
    [Route("api/abstractClass")]
    public class AbstractClassController : ControllerBase
    {
        private readonly List<KeyValuePair<int, int>>[] _hashMap;
        private readonly int _key;
        private readonly MyQueue _myQueue;
        private readonly MinStack _minStack;

        public AbstractClassController(int Key, MyQueue myQueue)
        {
            _hashMap = new List<KeyValuePair<int, int>>[1000];
            _key = Key;
            
            _myQueue = myQueue;
            _minStack = new MinStack();
        }

        #region Bank System
        [HttpGet(), Route("bankSystem")]
        public IActionResult FetchBankServices(string bankType)
        {
            IBank bankName = BankFactory.GetBankObject(bankType);

            if (bankName == null)
            {
                return Ok("Invalid bank type");
            }

            return Ok(new object[] { bankName.ValidateCard(),
                                     bankName.WithdrawMoney(),
                                     bankName.CheckBalanace(),
                                     bankName.BankTransfer(),
                                     bankName.MiniStatement(),
            });
        }
        #endregion 



        #region Design HashMap
        [HttpPost, Route("designHashMap/Put")]
        public IActionResult Put(int key, int value)
        {
            int index = key % 1000;
            if (_hashMap[index] == null)
            {
                _hashMap[index] = new List<KeyValuePair<int, int>>();
            }
            for (int i = 0; i < _hashMap[index].Count; i++)
            {
                if (_hashMap[index][i].Key == key)
                {
                    _hashMap[index][i] = new KeyValuePair<int, int>(key, value);
                    return Ok();
                }
            }
            _hashMap[index].Add(new KeyValuePair<int, int>(key, value));
            return Ok(index);
        }

        [HttpGet, Route("designHashMap/Get")]
        public IActionResult Get(int key)
        {
            int index = key % 1000;
            if (_hashMap[index] == null)
            {
                return Ok(-1);
            }
            for (int i = 0; i < _hashMap[index].Count; i++)
            {
                if (_hashMap[index][i].Key == key)
                {
                    return Ok(_hashMap[index][i].Value);
                }
            }
            return Ok(-1);
        }

        [HttpDelete, Route("designHashMap/Remove")]
        public IActionResult Remove(int key)
        {
            int index = key % 1000;
            if (_hashMap[index] == null)
            {
                return Ok();
            }
            for (int i = 0; i < _hashMap[index].Count; i++)
            {
                if (_hashMap[index][i].Key == key)
                {
                    _hashMap[index].RemoveAt(i);
                    return Ok();
                }
            }
            return Ok(index);
        }
        #endregion



        #region Kth Largest Element
        [HttpPost, Route("kthLargestElement/Add")]
        public IActionResult Add()
        {
            return Ok();
        }
        #endregion



        #region Queue Using Stack
        [HttpPost, Route("queueUsingStack")]
        public IActionResult Push(int x)
        {
           _myQueue.Push(x);
            return Ok($"{x} has been pushed.");
        }

        [HttpGet, Route("queueUsingStack/Pop")]
        public IActionResult Pop()
        {
            return Ok(_myQueue.Pop());
        }

        [HttpGet, Route("queueUsingStack/Peek")]
        public IActionResult Peek()
        {
            return Ok(_myQueue.Peek());
        }

        [HttpGet, Route("queueUsingStack/Empty")]
        public new ActionResult<bool> Empty()
        {
            return Ok(_myQueue.Empty());
        }
        #endregion



        #region Min Stack
        [HttpPost, Route("minStackPush/Val")]
        public IActionResult Pushs(int val)
        {
            _minStack.Push(val);
            return Ok();
        }

        [HttpPost, Route("minStack/Pop")]
        public IActionResult Pops()
        {
            _minStack.Pop();
            return Ok();
        }

        [HttpGet, Route("minStack/Top")]
        public IActionResult Top()
        {
            var top = _minStack.Top();
            return Ok(top);
        }

        [HttpGet, Route("minStack/GetMin")]
        public IActionResult GetMin()
        {
            var min = _minStack.GetMin();
            return Ok(min);
        }
        #endregion
    }


}

