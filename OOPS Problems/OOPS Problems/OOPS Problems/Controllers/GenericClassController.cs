﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OOPS_Problems.Models.GenericClass.DiscountNthOrder;
using OOPS_Problems.Models.GenericClass.FindPairWithSum;
using OOPS_Problems.Models.GenericClass.LRUCache;
using OOPS_Problems.Models.GenericClass.MyCalender;
using OOPS_Problems.Models.GenericClass.SnapShotArray;

namespace OOPS_Problems.Controllers
{
    [Route("api/genericClass")]
    public class GenericClassController : ControllerBase
    {
        private readonly MyCalendar<Event> _calendar;
        private readonly SnapshotArray<int> _array;
        private readonly Cashier<Product, Bill> _cashier;
        private readonly LRUCache<int> _cache;
        private readonly FindSumPairs<int> _findSumPairs;


        public GenericClassController()
        {
            _calendar = new MyCalendar<Event>();
            _array = new SnapshotArray<int>(5);
            var products = new[]
            {
            new Product { Id = 1, Price = 2.50m },
            new Product { Id = 2, Price = 1.50m },
            new Product { Id = 3, Price = 3.00m },
            };

            _cashier = new Cashier<Product, Bill>(3, 10, products);
            _cache = new LRUCache<int>(3);
            int[] numsber1 = { 1, 1, 2, 2, 2, 3 };
            int[] numsber2 = { 1, 4, 5, 2, 5, 4 };
            _findSumPairs = new FindSumPairs<int>(numsber1, numsber2);
        }

        #region MyCalender
        [HttpPost, Route("myCalender")]
        public ActionResult BookEvent(Event newEvent)
        {
           return Ok(_calendar.Book(newEvent));
           
        }
        #endregion 



        #region Snap Shot Array
        [HttpPost, Route("snapShotArray/set")]
        public IActionResult SetElement([FromBody] SetElementRequest request)
        {
            _array.Set(request.Index, request.Value);
            return Ok($"{request.Value} has been set to {request.Index}");
        }

        [HttpPost, Route("snapShotArray/snap")]
        public IActionResult TakeSnapshot()
        {
            int snapId = _array.Snap();
            return Ok(new { snap_id = snapId });
        }

        [HttpGet, Route("snapShotArray/get")]
        public IActionResult GetElement([FromQuery] GetElementRequest request)
        {
            int value = _array.Get(request.Index, request.SnapId);
            return Ok(new { value });
        }
        #endregion



        #region Discount Nth Order
        [HttpPost, Route("discountNthOrder")]
        public ActionResult<decimal> GetBill(IEnumerable<Bill> bill)
        {
            try
            {
                var total = _cashier.GetBill(bill);
                return Ok(total);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion



        #region Find Pair With Sum
        [HttpPost, Route("findPairWithSum")]
        public IActionResult CountPairs(FindSumPairModels findSumPairModels)
        {
           _findSumPairs.Add(findSumPairModels.Index, findSumPairModels.Value);
            return Ok(_findSumPairs.Count(findSumPairModels.Total));
        }
        #endregion




        #region LRU Cache
        [HttpGet("lurCache/key")]
        public ActionResult<int> Get(int key)
        {
            var value = _cache.Get(key);
            
            return Ok(value);
        }

        [HttpPost("lurCache/key/value")]
        public IActionResult Put(int key, int value)
        {
            _cache.Put(key, value);
            return Ok($"{value} has been added at {key}");
        }
        #endregion 
    }
}
