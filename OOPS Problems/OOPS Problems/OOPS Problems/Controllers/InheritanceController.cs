﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OOPS_Problems.Models.Inheritance.BookingConcertTickets;
using OOPS_Problems.Models.Inheritance.HierarchicalInheritance;
using OOPS_Problems.Models.Inheritance.StockPriceTracker;
using OOPS_Problems.Models.Inheritance.SubrectangleQueries;
using OOPS_Problems.Models.Inheritance.TweetCountsPerFrequency;

namespace OOPS_Problems.Controllers
{
    [Route("api/inheritance")]
    public class InheritanceController : ControllerBase
    {
        private readonly ParkingSystem _parkingSystem;
        private readonly TweetCountsBase _tweetCountsBase;
        private readonly TweetCountsBase _tweetCountsByMinute;
        private readonly TweetCountsBase _tweetCountsByHour;
        private readonly TweetCountsBase _tweetCountsByDay;
        private readonly StockPrice _stockPrice;
        private readonly Rectangle _rectangle;
        private readonly IBookMyShow _bookMyShow;
        public InheritanceController()
        {
            _parkingSystem = new ParkingSystem(5, 10, 15);
            _tweetCountsByMinute = new TweetCountsByMinute();
            _tweetCountsByHour = new TweetCountsByHour();
            _tweetCountsByDay = new TweetCountsByDay();
            _tweetCountsBase = new TweetCountsBase();
            _stockPrice = new StockPrice();
            _rectangle = new Rectangle(new int[,] { {2,3,5},{5,4,3},{2,6,4} });
            _bookMyShow = new BookMyShow(10, 10);
        }

        #region Parking System
        [HttpPost , Route("parkingSystem/addCars")]
        public IActionResult AddCar(int carType)
        {
            bool success = _parkingSystem.AddCar(carType);
            if (success)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }

        }
        #endregion



        #region Tweet Counts Per Frequency
        [HttpPost("tweetCountsPerFrequency/recordTweet")]
        public IActionResult RecordTweet(Tweet tweet)
        {
            _tweetCountsByMinute.RecordTweet(tweet.Name!, tweet.Time);
            _tweetCountsByHour.RecordTweet(tweet.Name!, tweet.Time);
            _tweetCountsByDay.RecordTweet(tweet.Name!, tweet.Time);

            return Ok();
        }
        [HttpGet, Route("tweetCountsPerFrequency/GetTweetCounts")]
        public IActionResult GetTweetCounts(TweetCount tweetCount)
        { 
           return Ok(_tweetCountsBase.GetTweetCounts(tweetCount.TweetName!, tweetCount.StartTime, tweetCount.EndTime, tweetCount.ChunkSize));
        }
        #endregion



        #region Stock Price Tracker
        [HttpPost, Route("stockPriceTracker/modifyStockPrice")]
        public IActionResult UpdateStockPrice(int timestamp, int price)
        {
            _stockPrice.Update(timestamp, price);
            return Ok($"{timestamp} and {price} has been updated.");           
        }

        [HttpGet, Route("stockPriceTracker/current")]
        public IActionResult GetCurrentStockPrice()
        {
            var currentPrice = _stockPrice.Current();
            return Ok(currentPrice);
        }

        [HttpGet, Route("stockPriceTracker/maximum")]
        public IActionResult GetMaximumStockPrice()
        {
            var maximumPrice = _stockPrice.Maximum();
            return Ok(maximumPrice);
        }

        [HttpGet, Route("stockPriceTracker/minimum")]
        public IActionResult GetMinimumStockPrice()
        {
            var minimumPrice = _stockPrice.Minimum();
            return Ok(minimumPrice);
        }
        #endregion



        #region Subrectangle Queries
        [HttpPost, Route("subrectangleQueries/update")]
        public IActionResult UpdateSubrectangle(Subrectangle subrectangle)
        {
            for (int i = subrectangle.Row1; i <= subrectangle.Row2; i++)
            {
                for (int j = subrectangle.Column1; j <= subrectangle.Column2; j++)
                {
                    _rectangle.Matrix[i,j] = subrectangle.NewValue;
                }
            }
            return Ok(subrectangle);
        }

        [HttpGet, Route("subrectangleQueries/getvalue/{row}/{col}")]
        public IActionResult GetValue(int row, int column)
        {
            return Ok(_rectangle.Matrix[row, column]);
        }
        #endregion



        #region Booking Concert Tickets
        [HttpPost, Route("gather")]
        public IActionResult GatherSeat(BookMyShowModel bookMyShowModel)
        {
            return Ok(_bookMyShow.Gather(bookMyShowModel.KthSeat, bookMyShowModel.MaxRow));

        }

        [HttpPost, Route("scatter")]
        public IActionResult ScatterSeat(BookMyShowModel bookMyShowModel)
        {
            return Ok(_bookMyShow.Scatter(bookMyShowModel.KthSeat, bookMyShowModel.MaxRow));

        }
        #endregion

    }
}
