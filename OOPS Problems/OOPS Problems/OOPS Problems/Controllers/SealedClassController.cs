﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OOPS_Problems.Models.SealedClass.DesignAuthenticationManager;
using OOPS_Problems.Models.SealedClass.DesignUndergroundSystem;
using OOPS_Problems.Models.SealedClass.ProductLastNumber;
using OOPS_Problems.Models.SealedClass.SeatReversationManager;
using OOPS_Problems.Models.SealedClass.SimpleBankSystem;
using System.Collections.Generic;
using System.IO;

namespace OOPS_Problems.Controllers
{
    [Route("api/sealedClass")]
    public class SealedClassController : ControllerBase
    {
        private readonly Dictionary<int, CheckInRecord> _checkIns = new();
        private readonly List<StationRouteRecord> _stationRoutes = new();
        private readonly ProductOfNumbers _productOfNumbers;
        private readonly AuthenticationManager _authenticationManager;
        private readonly SeatManager _seatManager;
        private readonly Bank _bank;

        public SealedClassController()
        {
            _productOfNumbers = new ProductOfNumbers();
            _authenticationManager = new AuthenticationManager(100);
            _seatManager = new SeatManager(200);
            _bank = new Bank(new long[] { 1200000,1800000,150000 });
        }

        #region Design Underground System
        [HttpPost, Route("undergroundSystem/Checkin")]
        public IActionResult CheckIn(CheckInRecord checkInRecord)
        {
            _checkIns[checkInRecord.Id] = new CheckInRecord { Id = checkInRecord.Id, StationName = checkInRecord.StationName, Time = checkInRecord.Time };
            return Ok(_checkIns);
        }

        [HttpPost, Route("undergroundSystem/Checkout")]
        public IActionResult CheckOut(CheckOutRecord checkOutRecord)
        {
            var checkIn = _checkIns[checkOutRecord.Id];
            _checkIns.Remove(checkOutRecord.Id);

            var route = _stationRoutes.FirstOrDefault(r =>
                r.StartStation == checkIn.StationName &&
                r.EndStation == checkOutRecord.StationName);

            if (route == null)
            {
                _stationRoutes.Add(new StationRouteRecord
                {
                    StartStation = checkIn.StationName,
                    EndStation = checkOutRecord.StationName,
                    TotalTime = checkOutRecord.Time - checkIn.Time,
                    Count = 1
                });
            }
            else
            {
                route.TotalTime += checkOutRecord.Time - checkIn.Time;
                route.Count++;
            }

            return Ok(route);
        }

        [HttpGet, Route("undergroundSystem/Average")]
        public IActionResult GetAverageTime(StationRouteRecord stationRouteRecord)
        {
            var route = _stationRoutes.FirstOrDefault(r =>
                r.StartStation == stationRouteRecord.StartStation &&
                r.EndStation == stationRouteRecord.EndStation);

            if (route == null)
            {
                return NotFound();
            }

            return Ok(route.TotalTime / route.Count);
        }
        #endregion



        #region Product LastNumber        
        [HttpPost, Route("productLastNumber/add")]
        public IActionResult AddNumber(int num)
        {
            _productOfNumbers.Add(num);
            return Ok(_productOfNumbers);
        }

        [HttpGet, Route("productLastNumber/{key}")]
        public IActionResult GetProduct(int key)
        {
            int product = _productOfNumbers.GetProduct(key);
            return Ok(product);
        }
        #endregion



        #region Design Authentication Manager
        [HttpPost, Route("designAuthenticationManager/generate")]
        public IActionResult GenerateToken([FromBody] string tokenId, [FromBody] int currentTime)
        {
            var token = _authenticationManager.Generate(tokenId, currentTime);
            return Ok(token);
        }

        [HttpPost, Route("designAuthenticationManager/renew")]
        public IActionResult RenewToken([FromBody] string tokenId, [FromBody] int currentTime)
        {
            _authenticationManager.Renew(tokenId, currentTime);
            return Ok();
        }

        [HttpGet, Route("designAuthenticationManager/count")]
        public IActionResult CountUnexpiredTokens([FromQuery] int currentTime)
        {
            var count = _authenticationManager.CountUnexpiredTokens(currentTime);
            return Ok(count);
        }
        #endregion



        #region Seat Reversation System
        [HttpGet, Route("seatReversationSystem/reserve")]
        public IActionResult ReserveSeat()
        {
            var seatNumber = _seatManager.Reserve();
            if (seatNumber != -1)
            {
                return Ok(seatNumber);
            }
            return NotFound();
        }

        [HttpPut, Route("seatReversationSystem/unreserve/{seatNumber}")]
        public IActionResult UnreserveSeat(int seatNumber)
        {
            _seatManager.Unreserve(seatNumber);
            return Ok();
        }
        #endregion



        #region Simple Bank System
        [HttpPost, Route("simpleBankSystem/transfer")]
        public IActionResult TransferMoney(TransferRequest request)
        {
            var success = _bank.Transfer(request.Account1, request.Account2, request.Money);
            if (success)
            {
                return Ok();
            }
            return BadRequest();
        }

        [HttpPost, Route("simpleBankSystem/deposit")]
        public IActionResult DepositMoney(DepositeRequest request)
        {
            var success = _bank.Deposit(request.Account, request.Money);
            if (success)
            {
                return Ok();
            }
            return BadRequest();
        }

        [HttpPost, Route("simpleBankSystem/withdraw")]
        public IActionResult WithdrawMoney(WithdrawRequest request)
        {
            var success = _bank.Withdraw(request.Account, request.Money);
            if (success)
            {
                return Ok();
            }
            return BadRequest();
        }
        #endregion 

    }
}
