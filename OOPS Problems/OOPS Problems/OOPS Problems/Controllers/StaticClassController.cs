﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OOPS_Problems.Models.StaticClass.DesignContainerNumber;
using OOPS_Problems.Models.StaticClass.DesignFoodRatingSystem;
using OOPS_Problems.Models.StaticClass.ExamRoom;
using OOPS_Problems.Models.StaticClass.OnlineStockSpan;
using OOPS_Problems.Models.StaticClass.WalkingRobot;

namespace OOPS_Problems.Controllers
{
    [Route("api/staticClass")]
    public class StaticClassController : ControllerBase
    {
        private static RobotModel? robot;

        #region Exam Room
        [HttpPost, Route("examRoom/Initialize")]
        public IActionResult Initialize(ExamRoomViewModel model)
        {
            ExamRoom.Initialize(model.Number);
            return Ok($"{model.Number} has been added.");
        }

        [HttpGet, Route("examRoom/Seat")]
        public IActionResult Seat()
        {
            return Ok(ExamRoom.Seat());
        }

        [HttpPost, Route("examRoom/Leave")]
        public IActionResult Leave(int p)
        {
            ExamRoom.Leave(p);
            return Ok($"{p} has been removed.");
        }
        #endregion



        #region Online Stock Span
        [HttpGet, Route("onlineStockSpan/GetSpan")]
        public IActionResult GetSpan(int price)
        {
            return Ok(StockPrices.GetSpan(price));
        }
        #endregion



        #region Walking Robot Simulation
        [HttpPost, Route("walkingRobotSimulation/Initializes")]
        public IActionResult Initializes(int width, int height)
        {
            robot = new RobotModel(width, height);
            return Ok();
        }

        [HttpPost, Route("walkingRobotSimulation/Step")]
        public IActionResult Step(int num)
        {
            robot?.Step(num);
            return Ok();
        }

        [HttpGet, Route("walkingRobotSimulation/Position")]
        public IActionResult GetPosition()
        {
            return Ok(robot?.GetPos());
        }

        [HttpGet, Route("walkingRobotSimulation/Direction")]
        public IActionResult GetDirection()
        {
            return Ok(robot?.GetDir());
        }
        #endregion



        #region Design Number Contrainers
        [HttpPost, Route("designNumberContrainers/{index}/{number}")]
        public IActionResult Change(int index, int number)
        {
            return Ok(NumberContainers.Change(index, number));
        }

        [HttpGet, Route("designNumberContrainers/{number}")]
        public IActionResult Find(int number)
        {
            return Ok(NumberContainers.Find(number));
        }
        #endregion



        #region Design FoodRating System
        [HttpPut, Route("designFoodRatingSystem{food}/{newRating}")]
        public IActionResult ChangeRating(string food, int newRating)
        {
            return Ok(FoodRatings.ChangeRating(food, newRating));
        }

        [HttpGet, Route("designFoodRatingSystem/{cuisine}")]
        public IActionResult HighestRated(string cuisine)
        {
            return Ok(FoodRatings.HighestRated(cuisine));
        }
        #endregion

    }
}
