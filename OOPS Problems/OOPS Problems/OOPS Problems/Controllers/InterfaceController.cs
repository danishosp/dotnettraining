﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OOPS_Problems.Controllers.Models;
using OOPS_Problems.Models.Interface.ATM_Machine;
using OOPS_Problems.Models.Interface.DesignHashSet;
using OOPS_Problems.Models.Interface.GetRandom;
using OOPS_Problems.Models.Interface.SerializeandDeserializeBST;
using OOPS_Problems.Models.RegularCLass.ATMSystem;
using OOPS_Problems.Models.StaticClass.ExamRoom;

namespace OOPS_Problems.Controllers
{
    [Route("api/Interface")]
    public class InterfaceController : ControllerBase
    {
        private readonly IRandomizedSet _randomizedSet;
        private readonly IATM _atm;
        private readonly IBinarySearchTreeSerializer _serializer;
        private readonly IMyHashSet _myHashSet;

        public InterfaceController(IATM atm, IBinarySearchTreeSerializer serializer)
        {            
            _randomizedSet = new RandomizedSet();
            _atm = atm;
            _serializer = serializer;
            _myHashSet = new MyHashSet();
        }

        #region Design HashSet
        [HttpPost, Route("designHashSet/Add")]
        public IActionResult Add(int key)
        {
            return Ok(_myHashSet.Add(key));
        }

        [HttpGet, Route("designHashSet/Contains")]
        public ActionResult<bool> Contains(int key)
        {            
            return _myHashSet.Contains(key);
        }

        [HttpDelete, Route("designHashSet/Remove")]
        public IActionResult Removes(int key)
        {            
            return Ok(_myHashSet.Remove(key));
        }
        #endregion



        #region Bank System
        [HttpGet, Route("bankSystem")]
        public IActionResult FetchBankServices(string bankType)
        {
            IBank bankName = BankFactory.GetBankObject(bankType);

            if (bankName == null)
            {
                return Ok("Invalid bank type");
            }

            return Ok(new object[] { bankName.ValidateCard(),
                                     bankName.WithdrawMoney(),
                                     bankName.CheckBalanace(),
                                     bankName.BankTransfer(),
                                     bankName.MiniStatement(),
            });
        }
        #endregion 



        #region Get Random

        [HttpPost, Route("getRandom/Insert")]
        public ActionResult<bool> Insert(int val)
        {
            return Ok(_randomizedSet.Insert(val));
        }

        [HttpPost, Route("getRandom/Remove")]
        public ActionResult<bool> Remove(int val)
        {
            return Ok(_randomizedSet.Remove(val));
        }

        [HttpGet, Route("getrandom")]
        public IActionResult GetRandom()
        {
            return Ok(_randomizedSet.GetRandom());
        }
        #endregion



        #region ATM Machine        
        [HttpPost, Route("atmMachine/Deposit")]
        public IActionResult Deposit([FromBody] int[] banknotesCount)
        {
            _atm.Deposit(banknotesCount);
            return Ok();
        }



        [HttpGet, Route("atmMachine/Withdraw")]
        public IActionResult Withdraw(int amount)
        {
            int[] result = _atm.Withdraw(amount);
            if (result[0] == -1)
            {
                return BadRequest("Unable to withdraw the requested amount.");
            }
            return Ok(result);
        }
        #endregion



        #region Serialize and Deserialize BST        
        [HttpGet("serializeAndDeserializeBST{treeString}")]
        public IActionResult Deserialize(string treeString)
        {
            BinaryTreeNode root = _serializer.Deserialize(treeString);
            return Ok(root);
        }

        [HttpPost, Route("serializeAndDeserializeBST")]
        public IActionResult Serialize(BinaryTreeNode root)
        {
            string treeString = _serializer.Serialize(root);
            return Ok(treeString);
        }
        #endregion
    }


}
