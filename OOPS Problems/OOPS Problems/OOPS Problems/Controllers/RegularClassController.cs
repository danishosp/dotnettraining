﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OOPS_Problems.Models.RegularCLass.ATMSystem;
using OOPS_Problems.Models.RegularCLass.LibrarySystem;
using OOPS_Problems.Models.RegularCLass.MyStack;
using OOPS_Problems.Models.RegularCLass.OrderedStream;
using OOPS_Problems.Models.RegularCLass.RecentCall;

namespace OOPS_Problems.Controllers
{
    [Route("api/regularClass")]
    public class RegularClassController : ControllerBase
    {
        private readonly MyStack _myStack;
        private readonly Atm _atm;
        private readonly List<BookModels> _books;
        private readonly RecentCounter _recentCounter;
        private readonly OrderedStream _orderedStream;

        public RegularClassController()
        {
            _myStack = new MyStack();
            _atm = new Atm(new int[] { 2000, 500, 200, 100, 50, 20, 10, 5 }, 10000);
            _books = new List<BookModels>();
            _recentCounter = new RecentCounter();
            _orderedStream = new OrderedStream(number: 10);
        }

        #region StackQueue
        [HttpPost, Route("stackQueue/push")]
        public IActionResult PushInStack(int number)
        {
            _myStack.Push(number);
            return Ok($"{number} has been pushed into stack.");
        }

        [HttpGet, Route("tackQueue/pop")]
        public IActionResult PopFromStack()
        {
            return Ok(_myStack.Pop());
        }

        [HttpGet, Route("stackQueue/TopElement")]
        public IActionResult TopFromStack()
        {
            return Ok(_myStack.Top());
        }

        [HttpGet, Route("stackQueue/IsEmptyOrNotStack")]
        public IActionResult IsEmptyOrNot()
        {
            return Ok(_myStack.Empty());
        }
        #endregion



        #region ATM System
        [HttpGet, Route("atmSystem/GetBalance")]
        public ActionResult<IEnumerable<int>> Balance()
        {
            return Ok(_atm.GetBalance());
        }

        [HttpPost, Route("atmSystem/WithdrawAmount")]
        public IActionResult WithdrawAmount(int amount)
        {
            return Ok(_atm.Withdraw(amount));
        }
        #endregion



        #region Library Management System        
        [HttpGet, Route("libraryManagementSystem/allBooks")]
        public ActionResult<BookModels> GetAllBooks()
        {
            return Ok(_books);
        }

        [HttpGet, Route("libraryManagementSystem/bookById")]
        public ActionResult<BookModels> GetBook(int id)
        {
            var book = _books.FirstOrDefault(b => b.Id == id);
            if (book == null)
            {
                return NotFound();
            }
            return book;
        }

        [HttpPost, Route("libraryManagementSystem/addBook")]
        public IActionResult AddBook(BookModels bookModels)
        {
            bookModels.Id = _books.Count + 1;
            _books.Add(bookModels);
            return CreatedAtAction(nameof(GetBook), new { id = bookModels.Id }, bookModels);
        }

        [HttpPut, Route("libraryManagementSystem/byId")]
        public IActionResult UpdateBook(int id, BookModels book)
        {
            var existingBook = _books.FirstOrDefault(b => b.Id == id);
            if (existingBook == null)
            {
                return NotFound();
            }
            existingBook.Title = book.Title;
            existingBook.Author = book.Author;
            existingBook.YearPublished = book.YearPublished;
            return NoContent();
        }

        [HttpDelete, Route("libraryManagementSystem/{id}")]
        public IActionResult DeleteBook(int id)
        {
            var bookToRemove = _books.FirstOrDefault(b => b.Id == id);
            if (bookToRemove == null)
            {
                return NotFound();
            }
            _books.Remove(bookToRemove);
            return NoContent();
        }
        #endregion



        #region Recent Call
        [HttpPost, Route("recentCall")]
        public IActionResult Pings(PingRequest request)
        {
            int numRequests = _recentCounter.Ping(request.Total);
            return Ok(new PingResponse { NumRequests = numRequests });
        }
        #endregion



        #region Ordered Stream
        [HttpPost, Route("orderedStream/{idKey}")]
        public IActionResult Insert(int idKey,string value)
        {
            var chunk = _orderedStream.Insert(idKey, value);
            return Ok(chunk);
        }
        #endregion
    }
}
